package edu.cvut.fit.wudipetr.particledetection.evaluation;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class DummyFilter {
    private final Collection<ImagePoint> remainingA = new ArrayList<>();
    private final Collection<ImagePoint> remainingB = new ArrayList<>();
    private final List<ImagePoint> filteredOutA = new ArrayList<>();
    private final List<ImagePoint> filteredOutB = new ArrayList<>();
    private final Map<ImagePoint, ImagePoint> mapping = new HashMap<>();

    public DummyFilter(Collection<ImagePoint> inputPointsA, Collection<ImagePoint> inputPointsB, int errorCost) {
        Collection<ImagePoint> pointsA = inputPointsA;
        Collection<ImagePoint> pointsB = inputPointsB;
        boolean shouldContinue = true;
        while (shouldContinue) {
            shouldContinue = iteration(pointsA, pointsB, errorCost);
            pointsA = new ArrayList<>(remainingA);
            pointsB = new ArrayList<>(remainingB);
        }
    }

    private boolean iteration(Collection<ImagePoint> inputPointsA, Collection<ImagePoint> inputPointsB, int errorCost) {
        int filteredOutASize = filteredOutA.size();
        int filteredOutBSize = filteredOutB.size();
        int mappedSize = mapping.size();

        remainingA.clear();
        remainingB.clear();
        filterTooFarAway(inputPointsA, inputPointsB, errorCost, remainingA, filteredOutA); // todo better code style - new return type
        filterTooFarAway(inputPointsB, inputPointsA, errorCost, remainingB, filteredOutB);
        Map<ImagePoint, ImagePoint> mappingLocal;
        do {
            mappingLocal = getOnlyMatches(remainingA, remainingB, errorCost);
            mapping.putAll(mappingLocal);
        } while (!mappingLocal.isEmpty());
        return filteredOutA.size() > filteredOutASize
                || filteredOutB.size() > filteredOutBSize
                || mapping.size() > mappedSize;
    }

    private Map<ImagePoint, ImagePoint> getOnlyMatches(Collection<ImagePoint> pointsA, Collection<ImagePoint> pointsB, int errorCost) {
        Map<ImagePoint, ImagePoint> onlyMatches = new HashMap<>();
        List<ImagePoint> aRemove = new ArrayList<>();
        List<ImagePoint> bRemove = new ArrayList<>();
        for (ImagePoint pointA : pointsA) {
            ImagePoint onlyMatch = getOnlyMatch(pointA, pointsB, errorCost);
            if (onlyMatch != null && !hasCloser(onlyMatch, ImagePoint.sqrDistance(pointA, onlyMatch), pointsA)) {
                onlyMatches.put(pointA, onlyMatch);
                aRemove.add(pointA);
                bRemove.add(onlyMatch);
            }
        }
        aRemove.forEach(pointsA::remove);
        bRemove.forEach(pointsB::remove);
        aRemove.clear();
        bRemove.clear();
        for (ImagePoint pointB : pointsB) {
            ImagePoint onlyMatch = getOnlyMatch(pointB, pointsA, errorCost);
            if (onlyMatch != null && !hasCloser(onlyMatch, ImagePoint.sqrDistance(pointB, onlyMatch), pointsB)) {
                onlyMatches.put(onlyMatch, pointB);
                aRemove.add(onlyMatch);
                bRemove.add(pointB);
            }
        }
        aRemove.forEach(pointsA::remove);
        bRemove.forEach(pointsB::remove);
        return onlyMatches;
    }

    private boolean hasCloser(ImagePoint refPoint, long refDistance, Collection<ImagePoint> points) {
        return points.stream()
                .map(point -> ImagePoint.sqrDistance(point, refPoint))
                .anyMatch(distance -> distance < refDistance);
    }

    private ImagePoint getOnlyMatch(ImagePoint pointA, Collection<ImagePoint> pointsB, int errorCost) {
        ImagePoint onlyMatch = null;
        for (ImagePoint pointB : pointsB) {
            if (ImagePoint.sqrDistance(pointA, pointB) <= errorCost) {
                if (onlyMatch == null) {
                    onlyMatch = pointB;
                } else {
                    return null;
                }
            }
        }
        return onlyMatch;
    }

    private void mapObvious(Map<ImagePoint, ImagePoint> bestA, Map<ImagePoint, ImagePoint> bestB) {
        for (Map.Entry<ImagePoint, ImagePoint> entry : bestA.entrySet()) {
            ImagePoint a = entry.getKey();
            ImagePoint b = entry.getValue();
            ImagePoint bestMappingForB = bestB.get(b);
            if (a != null && a == bestMappingForB) {
                remainingA.add(a);
                remainingB.add(b);
            }
        }
    }

    public List<ImagePoint> getFirstPointsNotBest() {
        return new ArrayList<>(remainingA);
        /*
        List<ImagePoint> collect = pointsA.stream()
                .filter(pointA -> {
                    ImagePoint bestBForPointA = bestA.get(pointA);
                    if (bestBForPointA == null) {
                        return true;
                    }
                    ImagePoint bestForBestBForPointA = bestB.get(bestBForPointA);
                    return pointA != bestForBestBForPointA;
                })
                .collect(Collectors.toList());
        mappedASize = pointsA.size() - collect.size();
         */
//        return collect;
    }

    public List<ImagePoint> getSecondPointsNotBest() {
        return new ArrayList<>(remainingB);
        /*
        return pointsB.stream()
                .filter(pointB -> {
                    ImagePoint bestAForPointB = bestB.get(pointB);
                    if (bestAForPointB == null) {
                        return true;
                    }
                    ImagePoint bestForBestAForPointB = bestA.get(bestAForPointB);
                    return pointB != bestForBestAForPointB;
                })
                .collect(Collectors.toList());
         */
    }

    private static Map<ImagePoint, ImagePoint> createBest(Collection<ImagePoint> pointsA, Collection<ImagePoint> pointsB, int errorCost) {
        return pointsA.stream()
                .collect(Collectors.toMap(pointA -> pointA, pointA -> getClosest(pointA, pointsB, errorCost)));
    }

    private static ImagePoint getClosest(ImagePoint point, Collection<ImagePoint> pointCollection, int errorCost) {
        return pointCollection.stream()
                .map(pointB -> new PointWithDistance(pointB, ImagePoint.sqrDistance(point, pointB)))
                .min(Comparator.comparingLong(PointWithDistance::getDistance))
                .filter(pointB -> pointB.getDistance() <= errorCost)
                .map(PointWithDistance::getPoint)
                .orElse(null);
    }

    private static void filterTooFarAway(Collection<ImagePoint> points,
                                                     Collection<ImagePoint> anotherPoints,
                                                     int errorCost,
                                                     Collection<ImagePoint> outputMapped,
                                                     Collection<ImagePoint> outputUnmapped) {
        for (ImagePoint point : points) {
            boolean hasPossibleMapping = anotherPoints.stream()
                    .map(anotherPoint -> ImagePoint.sqrDistance(point, anotherPoint))
                    .anyMatch(dist -> dist <= errorCost);
            if (hasPossibleMapping) {
                outputMapped.add(point);
            } else {
                outputUnmapped.add(point);
            }
        }
    }

    private static List<ImagePoint> filterTooFarAway(Collection<ImagePoint> points, Collection<ImagePoint> anotherPoints, int errorCost) {
        return points.stream()
                .filter(point -> anotherPoints.stream()
                        .map(anotherPoint -> ImagePoint.sqrDistance(point, anotherPoint))
                        .anyMatch(dist -> dist <= errorCost)
                )
                .collect(Collectors.toList());
    }

    public Collection<ImagePoint> getFilteredOutA() {
        return filteredOutA;
    }

    public Collection<ImagePoint> getFilteredOutB() {
        return filteredOutB;
    }

    public Map<ImagePoint, ImagePoint> getMapped() {
        return mapping;
    }

    private static class PointWithDistance {
        private ImagePoint point;
        private long distance;

        public PointWithDistance(ImagePoint point, long distance) {
            this.point = point;
            this.distance = distance;
        }

        public ImagePoint getPoint() {
            return point;
        }

        public long getDistance() {
            return distance;
        }
    }
}
