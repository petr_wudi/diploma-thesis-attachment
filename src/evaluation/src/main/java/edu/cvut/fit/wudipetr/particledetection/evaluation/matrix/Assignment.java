package edu.cvut.fit.wudipetr.particledetection.evaluation.matrix;

/**
 * Solution of Assignment problem.
 */
public class Assignment {
    int row;
    int col;
    
    public Assignment(int row, int col){
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public String toString() {
        return row + " -> " + col;
    }
}
