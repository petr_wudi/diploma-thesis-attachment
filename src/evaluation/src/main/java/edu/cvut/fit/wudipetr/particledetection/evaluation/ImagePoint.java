package edu.cvut.fit.wudipetr.particledetection.evaluation;

public class ImagePoint {
    private int x;
    private int y;

    public ImagePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return String.format("[%d, %d]", x, y);
    }

    public static long sqrDistance(ImagePoint a, ImagePoint b) {
        int xDiff = a.x - b.x;
        int yDiff = a.y - b.y;
        return xDiff * xDiff + yDiff * yDiff;
    }
}
