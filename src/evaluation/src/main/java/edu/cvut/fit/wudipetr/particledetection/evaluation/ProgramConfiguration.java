package edu.cvut.fit.wudipetr.particledetection.evaluation;

import org.apache.commons.cli.*;

import java.nio.file.Path;
import java.nio.file.Paths;

class ProgramConfiguration {
    private static final int MAX_DISTANCE = 10;

    private Path inputDirectory = Paths.get("");
    private Path trueFile = Paths.get("boundaries.txt");
    private String methodName = "";
    private int maxDistance = MAX_DISTANCE;

    ProgramConfiguration(String[] args) {
        Options options = new Options();
        options.addOption("i", "input", true, "Input directory location");
        options.addOption("r", "ref", true, "Location of the reference file");
        options.addOption("m", "method", true, "Name of the method (file prefix)");
        options.addOption("max", "maxdistance", true, "Maximum possible distance when points are matched");
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            loadProgramConfig(cmd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void loadProgramConfig(CommandLine cmd) {
        String inputDirName = cmd.getOptionValue("i");
        if (inputDirName != null) {
            inputDirectory = Paths.get(inputDirName);
        }
        String referenceDirName = cmd.getOptionValue("r");
        if (referenceDirName != null) {
            trueFile = Paths.get(referenceDirName);
        }
        methodName = cmd.getOptionValue("m");
        String maxDistanceStr = cmd.getOptionValue("max");
        if (maxDistanceStr != null && !maxDistanceStr.isEmpty()) {
            maxDistance = Integer.parseInt(maxDistanceStr);
        }
    }

    public Path getInputDirectory() {
        return inputDirectory;
    }

    public Path getTrueFile() {
        return trueFile;
    }

    public String getMethodName() {
        return methodName;
    }

    public int getMaxDistance() {
        return maxDistance;
    }
}
