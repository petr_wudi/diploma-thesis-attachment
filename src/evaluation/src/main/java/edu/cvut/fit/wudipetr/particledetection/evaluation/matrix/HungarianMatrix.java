package edu.cvut.fit.wudipetr.particledetection.evaluation.matrix;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import edu.cvut.fit.wudipetr.particledetection.evaluation.ImagePoint;

/**
 * Creates matrix describing assignment problem and solves it using hungarian algorithm
 */
public class HungarianMatrix {
    private static final int MAX_DISTANCE = 5;
    private static final int ERROR_COST_DEFAULT = MAX_DISTANCE * MAX_DISTANCE;

    int size;
    int[][] matrix;
    int trueBoundary;
    int detectedBoundary;
    private final int errorCost;

    public HungarianMatrix(List<ImagePoint> detectedPoints, List<ImagePoint> truePoints, int errorCost){
        System.out.println("DP = " + detectedPoints.size() + " TP=" + truePoints.size());
        computeSizes(detectedPoints.size(), truePoints.size());
        matrix = new int[size][size];
        this.errorCost = errorCost;
        for(int y = 0; y < size; ++y) {
            for(int x = 0; x < size; ++x) {
                matrix[y][x] = computeCell(x, y, detectedPoints, truePoints);
            }
        }
    }

    public HungarianMatrix(List<ImagePoint> detectedPoints, List<ImagePoint> truePoints){
        this(detectedPoints, truePoints, ERROR_COST_DEFAULT);
    }

    /**
     * Computes and sets size of the matrix and sizes of all table parts
     * @param trueSize Number of people in actual frame
     * @param detectedSize Number of people in previous frame
     */
    protected final void computeSizes(int detectedSize, int trueSize){
        size = Integer.max(trueSize, detectedSize);
        detectedBoundary = detectedSize;
        trueBoundary = trueSize;
    }

    protected final int computeCell(int x, int y, List<ImagePoint> detectedPoints, List<ImagePoint> truePoints) {
        if (y < trueBoundary) {
            if (x < detectedBoundary) {
                // match
                ImagePoint detectedPoint = detectedPoints.get(x);
                ImagePoint truePoint = truePoints.get(y);
                int distance = (int) ImagePoint.sqrDistance(truePoint, detectedPoint);
                if (distance > errorCost) {
                    distance = errorCost;
                }
                return distance;
            } else {
                // true -> no match
                return errorCost;
            }
        } else {
            if (x < detectedBoundary) {
                // detected -> no match
                return errorCost;
            } else {
                // empty cell (no match -> no match)
                return 0;
            }
        }
    }

    public int[][] getArray() { // todo remove
        return matrix;
    }

    /**
     * Solves assignment problem
     * @return Assignments between two sets defined in class initialize
     */
    public List<Assignment> solve(){
        if (size == 0) {
            return Collections.emptyList();
        }
        StolenHungarian shm = new StolenHungarian(matrix);
        int[] result = shm.execute();
        List<Assignment> rl = new ArrayList<>();
        for (int rowId = 0; rowId < size; rowId++) {
            int colId = result[rowId];
            if (matrix[rowId][colId] >= errorCost) {
                rl.add(new Assignment(rowId, detectedBoundary));
                rl.add(new Assignment(trueBoundary, colId));
            } else {
                rl.add(new Assignment(rowId, colId));
            }
        }
        return rl;
    }
}

