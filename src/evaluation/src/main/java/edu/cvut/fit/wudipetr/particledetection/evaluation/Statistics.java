package edu.cvut.fit.wudipetr.particledetection.evaluation;

public class Statistics {
    private long tp;
    private long fp;
    private long tn;
    private long fn;

    public Statistics(long tp, long fp, long tn, long fn) {
        this.tp = tp;
        this.fp = fp;
        this.tn = tn;
        this.fn = fn;
    }

    public long getTp() {
        return tp;
    }

    public long getFp() {
        return fp;
    }

    public long getTn() {
        return tn;
    }

    public long getFn() {
        return fn;
    }

    @Override
    public String toString() {
        return "tp=" + tp + ", fp=" + fp + ", tn=" + tn + ", fn=" + fn;
    }
}
