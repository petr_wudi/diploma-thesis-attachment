package edu.cvut.fit.wudipetr.particledetection.evaluation;

import edu.cvut.fit.wudipetr.particledetection.evaluation.matrix.Assignment;
import edu.cvut.fit.wudipetr.particledetection.evaluation.matrix.HungarianMatrix;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        ProgramConfiguration programConfiguration = new ProgramConfiguration(args);
        Path trueFile = programConfiguration.getTrueFile();
        Collection<ImagePoint> truePoints = loadPoints(trueFile);
        if (truePoints == null) {
            return;
        }
        Path inputDir = programConfiguration.getInputDirectory();
        String methodName = programConfiguration.getMethodName();
        int maxDistance = programConfiguration.getMaxDistance();
        Pattern fileNamePattern = Pattern.compile(Pattern.quote(methodName) + "__(?<parameters>.*).csv");
        Path outputDir = inputDir.resolve("..").resolve("statistics");
        Path errorsDir = inputDir.resolve("..").resolve("errors").resolve(methodName);
        if (!Files.exists(outputDir)) {
            try {
                Files.createDirectory(outputDir);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        if (!Files.exists(errorsDir)) {
            try {
                Files.createDirectories(errorsDir);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        Path outputFile = outputDir.resolve(methodName + ".csv");
//        Path fpFile = errorsDir.resolve("fp.csv");
//        Path fnFile = errorsDir.resolve("fn.csv");
        System.out.println("Output file " + outputFile.toAbsolutePath());
        try {
            List<String> rows = new ArrayList<>();
            for (Path path : Files.list(inputDir).collect(Collectors.toList())) {
                Matcher matcher = fileNamePattern.matcher(path.toFile().getName());
                if (matcher.matches()) {
                    String parameters = matcher.group("parameters");
                    Collection<ImagePoint> points = loadPoints(path);
                    if (points == null) {
                        points = Collections.emptyList();
                    }
                    Path fpFile = errorsDir.resolve("fp__" + path.toFile().getName());
                    Path fnFile = errorsDir.resolve("fn__" + path.toFile().getName());
                    Path tpFile = errorsDir.resolve("tp__" + path.toFile().getName());
                    Statistics statistics = computeStatistics(points, truePoints, maxDistance, tpFile, fpFile, fnFile);
                    long tp = statistics.getTp();
                    long fp = statistics.getFp();
                    long tn = statistics.getTn();
                    long fn = statistics.getFn();
                    String row = String.format("%s, %d, %d, %d, %d", parameters, tp, fp, tn, fn);
                    rows.add(row);
                }
            }
            String message = String.join("\n", rows);
            try {
                Files.write(outputFile, message.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Collection<ImagePoint> loadPoints(Path inputFile) {
        try {
            return Files.lines(inputFile)
                    .map(line -> line.split(","))
                    .filter(parts -> parts.length == 2)
                    .map(parts -> {
                        double xDouble = Double.parseDouble(parts[0]);
                        double yDouble = Double.parseDouble(parts[1]);
                        int x = (int) Math.round(xDouble);
                        int y = (int) Math.round(yDouble);
                        return new ImagePoint(x, y);
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Statistics computeStatistics(Collection<ImagePoint> detectedPoints,
                                                Collection<ImagePoint> truePoints,
                                                int maxDistance,
                                                Path tpFile, // todo use something like output
                                                Path fpFile, // todo use something like output
                                                Path fnFile) {
        Map<ImagePoint, ImagePoint> tpPoints;
        List<ImagePoint> fpPoints = new ArrayList<>();
        List<ImagePoint> fnPoints = new ArrayList<>();

        int errorCost = maxDistance * maxDistance;
        System.out.println("error cost " + errorCost);
//        List<ImagePoint> detectedPointsFiltered = filterPoints(detectedPoints, truePoints, errorCost);
//        List<ImagePoint> truePointsFiltered = filterPoints(truePoints, detectedPointsFiltered, errorCost);
        DummyFilter dummyFilter = new DummyFilter(detectedPoints, truePoints, errorCost);
        List<ImagePoint> detectedPointsFiltered2 = dummyFilter.getFirstPointsNotBest();
        List<ImagePoint> truePointsFiltered2 = dummyFilter.getSecondPointsNotBest();
        fpPoints.addAll(dummyFilter.getFilteredOutA());
        fnPoints.addAll(dummyFilter.getFilteredOutB());

        HungarianMatrix hungarianMatrix = new HungarianMatrix(detectedPointsFiltered2, truePointsFiltered2, errorCost);
//        HungarianMatrix hungarianMatrix = new HungarianMatrix(new ArrayList<>(detectedPoints), new ArrayList<>(truePoints));
        List<Assignment> assignmentList = hungarianMatrix.solve();
        tpPoints = dummyFilter.getMapped();
        long tp = tpPoints.size(); // currently zero
        long fp = fpPoints.size();
        long fn = fnPoints.size();
        long tn = 0;
        for (Assignment assignmentPair : assignmentList) {
            int detectedId = assignmentPair.getCol();
            int trueId = assignmentPair.getRow();
            if (detectedId < detectedPointsFiltered2.size()) {
                if (trueId < truePointsFiltered2.size()) {
                    /// detected and found in the ref file
                    ++tp;
                    ImagePoint detectedPoint = detectedPointsFiltered2.get(detectedId);
                    ImagePoint truePoint = truePointsFiltered2.get(trueId);
                    tpPoints.put(detectedPoint, truePoint);
                } else {
                    /// detected but not found in the ref file
                    ++fp;
                    ImagePoint point = detectedPointsFiltered2.get(detectedId);
                    fpPoints.add(point);
                }
            } else {
                if (trueId < truePointsFiltered2.size()) {
                    // point from a ref file was not found
                    ++fn;
                    ImagePoint point = truePointsFiltered2.get(trueId);
                    fnPoints.add(point);
                } else {
                    // otherwise OK - just technical mapping
                    // TODO note this is weird and should be documented in the thesis
                    ++tn;
                }
            }
        }

        StringBuilder tpStringBuilder = new StringBuilder();
        for (Map.Entry<ImagePoint, ImagePoint> entry : tpPoints.entrySet()) {
            ImagePoint detectedPoint = entry.getKey();
            ImagePoint truePoint = entry.getValue();
            tpStringBuilder.append(detectedPoint.getX()).append(", ").append(detectedPoint.getY()).append(", ")
                    .append(truePoint.getX()).append(", ").append(truePoint.getY()).append("\n");
        }
        StringBuilder fpStringBuilder = new StringBuilder();
        for (ImagePoint imagePoint : fpPoints) {
            fpStringBuilder.append(imagePoint.getX()).append(", ").append(imagePoint.getY()).append("\n");
        }
        StringBuilder fnStringBuilder = new StringBuilder();
        for (ImagePoint imagePoint : fnPoints) {
            fnStringBuilder.append(imagePoint.getX()).append(", ").append(imagePoint.getY()).append("\n");
        }
        try {
            Files.write(tpFile, tpStringBuilder.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            Files.write(fpFile, fpStringBuilder.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            Files.write(fnFile, fnStringBuilder.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }

        tn = truePoints.size() * detectedPoints.size();
//        tn = truePoints.size() / 10;
        System.out.println(fp);
        return new Statistics(tp, fp, tn, fn);
    }

    private static List<ImagePoint> filterPoints(Collection<ImagePoint> points, Collection<ImagePoint> anotherPoints, int errorCost) {
        return points.stream()
                .filter(point -> anotherPoints.stream()
                        .map(anotherPoint -> ImagePoint.sqrDistance(point, anotherPoint))
                        .anyMatch(dist -> dist <= errorCost)
                )
                .collect(Collectors.toList());
    }
}
