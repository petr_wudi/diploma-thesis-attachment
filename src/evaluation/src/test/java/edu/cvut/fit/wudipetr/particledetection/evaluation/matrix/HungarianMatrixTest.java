package edu.cvut.fit.wudipetr.particledetection.evaluation.matrix;

import edu.cvut.fit.wudipetr.particledetection.evaluation.ImagePoint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class HungarianMatrixTest {

    @Test
    public void testConstructor() {
        List<ImagePoint> detectedPoints = Arrays.asList(
                new ImagePoint(5, 10)
        );
        List<ImagePoint> truePoints = Arrays.asList(
                new ImagePoint(4, 10),
                new ImagePoint(3, 11)
        );
        int errorConst = 4;
        HungarianMatrix hungarianMatrix = new HungarianMatrix(detectedPoints, truePoints, errorConst);
        int[][] array = hungarianMatrix.getArray();
        Assertions.assertEquals(2, array.length);
        Assertions.assertEquals(2, array[0].length);

        Assertions.assertEquals(1, array[0][0]);
        Assertions.assertEquals(errorConst, array[0][1]);
        Assertions.assertEquals(errorConst, array[1][0]);
        Assertions.assertEquals(errorConst, array[1][1]);

        List<Assignment> assignment = hungarianMatrix.solve();

        Assertions.assertEquals(3, assignment.size());
        Assertions.assertEquals(0, assignment.get(0).getRow());
        Assertions.assertEquals(0, assignment.get(0).getCol());
    }

    @Test
    public void testAssignment() {
        List<ImagePoint> detectedPoints = Arrays.asList(
                new ImagePoint(5, 10)
        );
        List<ImagePoint> truePoints = Arrays.asList(
                new ImagePoint(3, 11),
                new ImagePoint(4, 10)
        );
        int errorConst = 10;
        HungarianMatrix hungarianMatrix = new HungarianMatrix(detectedPoints, truePoints, errorConst);
        List<Assignment> assignment = hungarianMatrix.solve();

        Assertions.assertEquals(3, assignment.size());
        Assertions.assertEquals(1, assignment.get(2).getRow());
        Assertions.assertEquals(0, assignment.get(2).getCol());
    }
}
