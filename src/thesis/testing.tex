\chapter{Testing}\label{chap:testing}

This chapter describes methods used to evaluate the single-particle detection methods.

The evaluation algorithm uses two input data sets: protein positions manually annotated by a human (called ``reference'') and positions detected by the evaluated algorithm (called ``testing'').

The test data are compared to the reference data.

A particle is often marked by some reference point and some testing point that are very close to each other but not exactly on the same pixel. Therefore, it is necessary to assign the particles marking the same protein to each other. The assignment algorithm is further described in the section \ref{test_mapping}.

Precision, recall and f-measure are computed to evaluate the performance of the particle detection methods.

Several interesting measures (like ROC curve and AUC that describe the whole method and not just its one configuration) can't be computed because those measures need the number of true negative samples. There is an infinite number of points with no protein particle where no particle has been detected -- hence the number of true negative would be infinite. It is possible to compute true negative pixels. It is a finite number but it is very high for all reasonable algorithms (so the ROC curves would look almost the same) and it changes when the image resolution changes.

\section{Assignment of particles}\label{test_mapping}

The mapping algorithm pairs particles in the reference set with particles detected in the testing set.

Mapping of the particles is considered an assignment problem.
\newpage
There are three possible types of assignment to make:

\begin{itemize}
  \item a reference point to a testing point -- means that the reference point and the testing point lie on the same particle
  \item a reference point to nothing -- means that reference point was not detected
  \item a testing point to nothing -- means that the testing point is a false positive
\end{itemize}

Each of the assignments described above has defined ``cost'' which denotes how unlikely is the occurrence to happen.

The assignment problem is defined as finding such assignment function $f_A$, for which the expression:

\begin{equation}\label{eq:assignment}
    \sum_{m = 1}^n\sum_{n = 1}^n{f_A(i, j) c(i, j)}
\end{equation}

is as minimal as possible \cite{assignment-problem}. Function $c(i, j)$ in the equation \ref{eq:assignment} is the cost of the assignment of $i$ to $j$.

Values of the function $f_A$ can be 1 (the objects $i$ and $j$ are assigned to each other) or 0 (the objects are not assigned). Exactly one $i$ can be assigned to one $j$.

In the case of this thesis, the cost $c$ is defined as:

\begin{itemize}
	\item for assignment of two points: square distance of those two points,
	\item for assignment of point to noting (and vice versa): constant of 50 px. Particles with the distance greater than 50 px could be never assigned to each other. The constant is further noted as $d_{max}$.
\end{itemize}

There are two possible ways to understand the assignment problem -- as a pairing in a bipartite graph or using a matrix.

\subsection{Matching in a bipartite graph}

Assignment problem can be understood as finding a matching\footnote{Set of edges with no common vertex.} in a bipartite graph\footnote{Graph whose nodes form two disjoint sets. The graph may contain edges between nodes from different sets but there can be no edges between nodes from the same set.}.

Nodes in the graph would represent reference and testing points. Edges between such two nodes would represent assigning the two points to each other. Only points representing points from different sets could be connected, therefore the graph is bipartite (having two sets of nodes where there could be no edge between nodes in the same set).

There is another restriction of this graph: each node could only have one edge (each point could be assigned to only one point).

The goal of the task is to find such a graph with maximal possible number of edges.

Though this algorithm might be much simpler, it can't utilize distance between two points. Two points very close to each other are equally likely to be assigned to each other than two points with the distance just slightly below $d_max$. The matrix solution was chosen for this reason.

\subsection{Assignment matrix}\label{sec:assignment-matrix}

%Having costs for each pair, ``assignment matrix'' could be created. It will serve as input for the Hungarian algorithm later, which solves the assignment problem.

Assignment matrix $A$ is a square matrix, in which each element $A_{i, j}$ is equal to the cost function $c(i, j)$ \cite{assignment-problem}.

The following text describes creation of such a matrix.

Let $F_i$ be the $i$-th reference point, $T_i$ be the $i$-th testing point, $k$ be number of the reference points, $l$ be number of the testing point and $d(a, b)$ be the (square Euclidean) distance between $a$ and $b$.

% $k$ be number of the 

% Let's define


% Let's define $k=|F|$, $l=|T|$, $F_i$ as $i$-th element of $F$ and $d(x, y)$ as (square euclidean) distance of points x and y. The matrix displayed below contains also fictional points $X_i$ -- assigning any testing or reference point to X-points means that the point has no counterpart in the another set. Assigning X-point to another X-point has no meaning at all.


% The size $n$ is defined as $n = \max \{| F |, | T |\}$, where $F$ is set of the reference points, T is set of the testing points and operator $| X |$  means number of points in a set.

% Let's define $k=|F|$, $l=|T|$, $F_i$ as $i$-th element of $F$ and $d(x, y)$ as (square euclidean) distance of points x and y. The matrix displayed below contains also fictional points $X_i$ -- assigning any testing or reference point to X-points means that the point has no counterpart in the another set. Assigning X-point to another X-point has no meaning at all.

The assignments matrix is defined as follows:

\[
\begin{blockarray}{ccccccc}
 & T_1 & \hdots & T_l & X_1 & \hdots & X_{n-l} \\
\begin{block}{c(cccccc)}
F_1 & d(F_1, T_1) & \hdots & d(F_1, T_l) & d_{max} & d_{max} & d_{max} \\
\vdots & \vdots & \ddots & \vdots & d_{max} & d_{max} & d_{max} \\
F_k & d(F_k, T_1) & \hdots & d(F_k, T_l) & d_{max} & d_{max} & d_{max} \\
X_1 & d_{max} & d_{max} & d_{max} & d_{max} & d_{max} & d_{max} \\
\vdots & d_{max} & d_{max} & d_{max} & d_{max} & d_{max} & d_{max} \\
X_{n-k} & d_{max} & d_{max} & d_{max} & d_{max} & d_{max} & d_{max} \\
\end{block}
\end{blockarray}
\]

This matrix serves as an input of Hungarian algorithm \cite{hungarian}, which creates the assignment function.

Assigning a column to a row in the matrix is interpreted this way:

\begin{itemize}
    \item Assignment between $F_i$ and $T_j$ -- true positive -- The testing point $T_j$ is assigned to the reference point $F_i$.
    \item Assignment between $X_i$ and $T_j$ -- false positive -- The testing point $T_j$ has no counterpart in the reference set.
    \item Assignment between $F_i$ and $X_j$ -- false negative -- The reference point $F_i$ has no counterpart in the testing set.
    \item Assignment between $X_i$ and $X_j$ -- no meaning (necessary for technical reasons).
\end{itemize}

\section{Observed measures}

Simple measures as true positive, false positive and false negative defined in the section \ref{sec:assignment-matrix} are not sufficient to be used to compare detection methods. Therefore, derived measures have to be computed.

Precision, recall and F-measure were selected.

Precision (defined in the equation \ref{eq:precision} \cite{fmeasure}) captures algorithm's ability to avoid false detections.

\begin{equation}\label{eq:precision}
    \mathrm{precision} = \frac{|\mathrm{true\ positive}|}{|\mathrm{true\ positive}| + |\mathrm{false\ positive}|}
\end{equation}

Recall (defined in the equation \ref{eq:recall} \cite{fmeasure}) captures algorithm's ability to detect particles.

\begin{equation}\label{eq:recall}
    \mathrm{recall} = \frac{|\mathrm{true\ positive}|}{|\mathrm{true\ positive}| + |\mathrm{false\ negative}|}
\end{equation}

F-measure combines both those measures into one number \cite{fmeasure}:

\begin{equation}\label{eq:fmeasure}
    F = 2 \cdot \frac{\mathrm{precision} \cdot \mathrm{recall}}{\mathrm{precision} + \mathrm{recall}}
\end{equation}

F-measure captures not only algorithm's precision or recall but also their balance \cite{fmeasure}. If one of those measures is very high and the second one very low, F-measure will be low.

\section{Results}

The following section contains the result of the tested methods. Measures described in the previous section were used.

The tables below contain selected combinations of algorithms. The combinations were selected manually -- preprocessing or validation phases were added to the existing set of algorithms to address some of their weakness.

Different parameters of the methods were tried to obtain the best possible combination of the parameters. Then the algorithm's performance was evaluated on the testing data.

\newpage

\subsection{Local maximum}

First approach (4-neighbourhood):

\noindent\begin{tabularx}{\textwidth}{ | X | r | r | r | }
    \hline
    Preprocessing/postprocessing method & Precision & Recall & F-measure \\ 
    \hline\hline
    \textit{No preprocessing/postprocessing} & 38.5 \% & 53.9 \% & 0.450 \\ 
    \arrayrulecolor{lightgray}\hline
    H-maximum               & 80.4 \% & 79.2 \% & 0.798 \\ 
    \arrayrulecolor{lightgray}\hline
    Bilateral filter + H-maximum     & 75.0 \% & 83.2 \% & 0.789 \\ 
    \arrayrulecolor{lightgray}\hline
    Laplace operator + H-maximum     & 84.2 \% & 32.1 \% & 0.465 \\ 
    \arrayrulecolor{lightgray}\hline
  
  
    
    % SRRF
    SRRF                               & 71.3 \% & 76.2 \% & 0.737 \\ 
    \arrayrulecolor{lightgray}\hline
    SRRF + H-maximum                    & 91.0 \% & 82.0 \% & 0.862 \\ 
    \arrayrulecolor{lightgray}\hline
    \makecell[l]{SRRF + Gaussian filter\\+ H-maximum}  & 91.5 \% & 82.2 \% & 0.866 \\ 
    \arrayrulecolor{lightgray}\hline
    \makecell[l]{SRRF + Bilateral filter + H-maximum} & 91.2 \% & 82.0 \% & 0.863 \\ 
    \arrayrulecolor{lightgray}\hline
    \makecell[l]{SRRF + Wiener filter + H-maximum}    & 99.7 \% & 29.8 \% & 0.459 \\ 
    \arrayrulecolor{black}\hline
\end{tabularx}

Second approach (sliding window):

\noindent\begin{tabularx}{\textwidth}{ | X | r | r | r | }
    \hline
    Preprocessing/postprocessing method & Precision & Recall & F-measure \\ 
    \hline\hline
    \textit{No preprocessing/postprocessing} & 63.5 \% & 65.5 \% & 0.645 \\ 
    \arrayrulecolor{lightgray}\hline
    H-maximum               & 73.5 \% & 80.9 \% & 0.771 \\ 
    \arrayrulecolor{lightgray}\hline
    SRRF                   & 72.0 \% & 76.0 \% & 0.739 \\ 
    \arrayrulecolor{black}\hline
\end{tabularx}

The sliding window local maximum algorithm is more complex than the 4-neighbourhood one but it didn't bring almost any advantage in the performance. Therefore, this algorithm was abandoned and all preprocessing methods were tested only for the first one.




\subsection{Region growing}

\noindent\begin{tabularx}{\textwidth}{ | X | r | r | r | }
  \hline
  Preprocessing/postprocessing method & Precision & Recall & F-measure \\ 
  \hline\hline
  \textit{No preprocessing/postprocessing} & 65.2 \% & 73.1 \% & 0.690 \\ 
  \arrayrulecolor{lightgray}\hline
  Gaussian blur          & 64.6 \% & 72.8 \% & 0.622 \\ 
  \arrayrulecolor{lightgray}\hline
  Prewitt subtraction    & 69.9 \% & 73.2 \% & 0.715 \\ 
  \arrayrulecolor{lightgray}\hline
  Prewitt subtraction + Gaussian filter & 75.3 \% & 60.9 \% & 0.673 \\ 
  \arrayrulecolor{lightgray}\hline
  Sobel subtraction      & 62.5 \% & 74.7 \% & 0.681 \\ 
  \arrayrulecolor{lightgray}\hline



% SRRF
SRRF                   & 53.9 \% & 58.8 \% & 0.562 \\ 
  \arrayrulecolor{lightgray}\hline
  SRRF + Prewitt subtraction   & 96.9 \% & 58.2 \% & 0.727 \\ 
  \arrayrulecolor{lightgray}\hline
  \makecell[l]{SRRF + Gaussian filter\\+ Prewitt subtraction}   & 99.4 \% & 34.3 \% & 0.510 \\ 
%   \arrayrulecolor{lightgray}\hline
%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
%   \arrayrulecolor{lightgray}\hline



%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
%   \arrayrulecolor{lightgray}\hline
%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
%   \arrayrulecolor{lightgray}\hline
%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
  \arrayrulecolor{black}\hline
\end{tabularx}

\subsection{Thresholding}

\noindent\begin{tabularx}{\textwidth}{ | X | r | r | r | }
  \hline
  Preprocessing/postprocessing method & Precision & Recall & F-measure \\ 
  \hline\hline
  \textit{No preprocessing/postprocessing}   & 52.5 \% & 65.0 \% & 0.580 \\ 
  \arrayrulecolor{lightgray}\hline
  Top hat                & 68.6 \% & 77.6 \% & 0.729 \\ 
  \arrayrulecolor{lightgray}\hline
  Laplacian filter + Top hat                  & 55.6 \% & 78.5 \% & 0.651 \\ 
  \arrayrulecolor{lightgray}\hline
  Flatfield correction (subtract)             & 76.3 \% & 45.5 \% & 0.570 \\ 
  \arrayrulecolor{lightgray}\hline
  \makecell[l]{Flatfield correction (image \\normalization)}  & 79.3 \% & 50.3 \% & 0.616 \\ 
  \arrayrulecolor{lightgray}\hline
  Prewitt subtraction    & 59.2 \% & 82.3 \% & 0.689 \\ 
  \arrayrulecolor{lightgray}\hline
  \makecell[l]{Flatfield (normalisation)\\+ Prewitt subtraction} & 59.7 \% & 79.1 \% & 0.680 \\ 
  \arrayrulecolor{lightgray}\hline

  % SRRF
  SRRF                   & 80.3 \% & 79.1 \% & 0.797 \\ 
  \arrayrulecolor{lightgray}\hline
  Gaussian filter        & 90.3 \% & 80.2 \% & 0.850 \\ 
  \arrayrulecolor{lightgray}\hline
  Bilateral filter       & 78.9 \% & 80.3 \% & 0.796 \\ 
  \arrayrulecolor{lightgray}\hline
  SRRF + Prewitt         & 87.9 \% & 80.1 \% & 0.838 \\ 
  \arrayrulecolor{lightgray}\hline
  SRRF + Prewitt subtraction                            & 80.7 \% & 80.5 \% & 0.806 \\ 
  \arrayrulecolor{lightgray}\hline
  SRRF + Gaussian filter + Prewitt                      & 89.9 \% & 78.3 \% & 0.837 \\ 
  \arrayrulecolor{lightgray}\hline
  SRRF + Bilateral filter + Prewitt                     & 87.6 \% & 79.7 \% & 0.834 \\ 
  \arrayrulecolor{lightgray}\hline
  SRRF + Top hat         & 80.2 \% & 79.1 \% & 0.797 \\ 
  \arrayrulecolor{lightgray}\hline
  SRRF + Gaussian filter + Top hat                      & 90.4 \% & 72.3 \% & 0.804 \\ 
  \arrayrulecolor{lightgray}\hline
  SRRF + Flatfield correction (subtract)                & 78.7 \% & 80.3 \% & 0.795 \\ 
  \arrayrulecolor{lightgray}\hline
  \makecell[l]{SRRF + Flatfield correction \\(image normalization)}     & 79.5 \% & 79.0 \% & 0.794 \\ 
%   \arrayrulecolor{lightgray}\hline
%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
%   \arrayrulecolor{lightgray}\hline
%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
%   \arrayrulecolor{lightgray}\hline
%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
%   \arrayrulecolor{lightgray}\hline
%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
%   \arrayrulecolor{lightgray}\hline
%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
%   \arrayrulecolor{lightgray}\hline
%   ---                    & XX.X \% & XX.X \% & 0.XXX \\ 
  \arrayrulecolor{black}\hline


\end{tabularx}

% \begin{figure}[h]
%     \centering
%     % \begin{subfigure}[b]{0.49\textwidth}
%         % \includegraphics[width=\textwidth]{img/roc/threshold.png}
%         % \caption{ROC curve}
%     % \end{subfigure}
%     % \hfill
%     % \begin{subfigure}[b]{0.49\textwidth}
%         \includegraphics[width=\textwidth]{img/fmeasure/threshold-by-threshold.png}
%         % \caption{F-measure of threshold by threshold}
%     % \end{subfigure}

%     \caption{F-measure of thresholding}\label{fig:roc-fmeasure-threshold}
% \end{figure}

Thresholding with the Otsu method was also tested:

\noindent\begin{tabularx}{\textwidth}{ | X | r | r | r | }
    \hline
    Preprocessing/postprocessing method & Precision & Recall & F-measure \\ 
    \hline\hline
    \textit{No preprocessing/postprocessing} & 69.6 \% & 27.4 \% & 0.393 \\ 
    \arrayrulecolor{lightgray}\hline
    SRRF                   & 94.8 \% & 48.0 \% & 0.637 \\ 
    \arrayrulecolor{black}\hline
\end{tabularx}
