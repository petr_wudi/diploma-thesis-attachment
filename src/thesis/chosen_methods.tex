\chapter{Chosen methods}\label{chap:chosen-methods}

This section describes already existing methods chosen to be implemented, evaluated and used in the resulting algorithm.

\section{Filters}

A filter is an operation that aims to suppress low or high frequencies in the image \cite{filters}.

A filter is often a~convolution of the original function with some filter function. Such filters are called linear.

Equation \ref{eq:linear-filter} contains filtering of the original function $f$ and the filter function $h$ resulting in the output function $g$. Operator $*$ marks convolution.

\begin{equation}\label{eq:linear-filter}
g(x, y) = f(x, y) * h(x, y) = \int_{-\infty}^{\infty} \int_{-\infty}^{\infty} f(t, u) \,h(x - t, y - u) \,dt\, du
\end{equation}

Computing the convolution of two discrete functions is very time-consuming. There are two approaches solving this issue. One of them is transforming the image to the frequency domain, multiplying both filters there and then transforming it back to the spatial domain \cite{filters}. Multiplying of signals in the frequency domain equals convolution in the spatial domain \cite{filters}.

The other approach is estimating the filter function by a~small matrix called kernel \cite{filters}.

Equation \ref{eq:linear-filter} defines computation of a~pixel $g(i, j)$ in a~discrete linear filter \cite{filters}. Size of the kernel $h$ is $M\times M$, where $M$ is an odd number.

\begin{equation}\label{eq:linear-discrete-filter}
  g(x, y) = \sum_{t=-\left\lfloor\frac{M}{2}\right\rfloor}^{\left\lfloor\frac{M}{2}\right\rfloor} \sum_{u=-\left\lfloor\frac{M}{2}\right\rfloor}^{\left\lfloor\frac{M}{2}\right\rfloor} f(x - t, y - u) \cdot h(t, u)
\end{equation}

\subsection{Laplacian filter}\label{sec:laplace}

Laplacian filter approximates the second derivation of the image \cite{laplace}.

Discrete Laplacian filter uses eg. this $3 \times 3$ matrix as kernel \cite{laplace}:
$$
  \left[ {\begin{array}{rrr}
   0 & 1 & 0 \\
   1 & -4 & 1 \\
   0 & 1 & 0 \\
  \end{array} } \right]
$$

A sharpened image can be obtained by subtracting the second derivation from the original image \cite{laplace}. Edges in the sharpened image are sharpened because the second derivation suppresses the onset of the edge and elevates the finish of the edge (see fig \ref{fig:laplace}).

\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{0.32\textwidth}
        \includegraphics[width=\textwidth]{img/laplace-1.pdf}
        \caption{Original edge}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.32\textwidth}
        \includegraphics[width=\textwidth]{img/laplace-2.pdf}
        \caption{Second derivation}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.32\textwidth}
        \includegraphics[width=\textwidth]{img/laplace-3.pdf}
        \caption{Sharpened edge}
    \end{subfigure}

    \caption{Sharpening image using Laplace filter}\label{fig:laplace}
\end{figure}

\subsection{Prewitt and Sobel operator}\label{sec:prewitt-sobel}

Both Prewitt operator and Sobel operator find estimate gradient in each pixel of the input image \cite{image-processing-java}. They use gradient to find edges in the image \cite{image-processing-java}.

There are two variants of both operators, one detects horizontal gradients and the other detects vertical gradients.

Both variants of both filters consist of a~3$\times$3 matrix (kernel) which is convolved with the image. Kernel for the horizontal filter is called $H_x$, the vertical one is $H_y$.

Figure \ref{fig:prewitt-kernel} displays kernels for Prewitt and \ref{fig:sobel-kernel} for Sobel operator.

\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{0.32\textwidth}
      \centering

      $$H_x = \left[ {\begin{array}{rrr}
       1 & 0 & -1 \\
       1 & 0 & -1 \\
       1 & 0 & -1 \\
      \end{array}} \right]$$
%      \caption{$G_x$ -- horizontal}
    \end{subfigure}
    \begin{subfigure}[b]{0.32\textwidth}
      \centering
      $$H_y = \left[ {\begin{array}{rrr}
       1 & 1 & 1 \\
       0 & 0 & 0 \\
       -1 & -1 & -1 \\
      \end{array}} \right]$$
      % \caption{$G_y$ -- vertical}
    \end{subfigure}
    \caption{Kernels of Prewitt filter}\label{fig:prewitt-kernel}
\end{figure}

\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{0.32\textwidth}
      \centering

      $$H_x = \left[ {\begin{array}{rrr}
       -1 & 0 & 1 \\
       -2 & 0 & 2 \\
       -1 & 0 & 1 \\
      \end{array}} \right]$$
      % \caption{$G_x$ -- horizontal}
    \end{subfigure}
    \begin{subfigure}[b]{0.32\textwidth}
      \centering
      $$H_y = \left[ {\begin{array}{rrr}
       1 & 2 & 1 \\
       0 & 0 & 0 \\
       -1 & -2 & -1 \\
      \end{array}} \right]$$
      % \caption{$G_y$ -- vertical}
    \end{subfigure}
    \caption{Kernels of Sobel filter}\label{fig:sobel-kernel}
\end{figure}

A single image of gradient magnitude is computed as Euclidean distance of both convolved images: $|g(x, y)| = \sqrt{(g_x(x, y))^2 + g_y(x, y))^2}$ \cite{image-processing-java}. 

This equation is often simplified to $|g(x, y)| = g_x(x, y) + g_y(x, y)$ for sake of performace \cite{sobel-ac}.

%To obtain a~single image of gradient magnitude using Prewitt filter, both gradients must be added up: $|G| = |G_x| + |G_y|$.

%Single gradient magnitude from Sobel operator is computed using: $|G| = \sqrt{|G_c| + |G_y|}$, which is often simplified to $|G| = |G_x| + |G_y|$ for sake of performance \cite{sobel-ac}.

Prewitt and Sobel perform both smoothing and gradient estimation \cite{image-processing-java}. The gradient estimation is thus less influenced by noise.

Their only difference lies in the smoothing kernel. Prewitt uses simple box smoothing $\left[\begin{array}{rrr}1&1&1\end{array}\right]$. Sobel's kernel keeps higher weight on the center image: $\left[\begin{array}{rrr}1&2&1\end{array}\right]$.

Those kernels are convolved with a~gradient estimation kernel $\left[\begin{array}{rrr}1&0&-1\end{array}\right]^\intercal$ (or functionally similar $\left[\begin{array}{rrr}-1&0&1\end{array}\right]^\intercal$ for Sobel), which produces the kernels $H_x$ above \cite{image-processing-java}. Vertical kernels $H_y$ are computed similarly.

\subsection{Wiener deconvolution}\label{sec:wiener-generic}

Wiener filter removes noise and de-blurs the image.

Each photograph or microscope image was created as a~convolution of the original object with point spread function (PSF). Photographies also tend to have noise in them.

Wiener filter relies on the assumption that creation of the image $g$ can be written as \cite{wiener}:

\begin{equation}
g(x, y) = f(x, y) * h(x, y) + w(x, y)
\end{equation}

where $f$ is the original object, operator $*$ represents convolution, $h$ is the point spread function and $w$ is level of the nose.

The goal of deconvolution is to find an estimate of the original object $\hat{f}$ using a~function $r$ such as:

\begin{equation}\label{eq:restoration-spatial}
\hat{f}(x, y) = g(x, y) * r(x, y).
\end{equation}

Convolution in the spatial domain is equivalent to multiplication in the frequency domain. Element-wise multiplication is much less complex operation than convolution. Therefore, all matrices are converted to the frequency domain using Fourier transform. Equation \ref{eq:restoration-frequency} is the frequency equivalent of the equation \ref{eq:restoration-spatial}.

\begin{equation}\label{eq:restoration-frequency}
  \hat{F}(u, v) = G(u, v) R(u, v)
\end{equation}

To solve this equation and equation \ref{eq:restoration-spatial}, it is necessary to find the function $r$ or its frequency equivalent $R$.

The function $R(u, v)$ in the Wiener filter is defined using the power (absolute value) of the original signal $P_{ff}(u, v)$ and power of the noise $P_{ww}(u, v)$ \cite{wiener}.

\begin{equation}\label{eq:wiener-full}
  R(u, v) = \frac{H^\ast(u, v) P_{ff}(u, v)}{\left| H(u, v)\right|^2 P_{ff}(u, v) + P_{ww}(u, v)}
\end{equation}

The function $H(u, v)$ is the frequency equivalent of the PSF.

The equation \ref{eq:wiener-full} contains the power $P_{ff}$ of the original signal, which is also subject of the computation. This issue can be partly addressed by dividing both numerator and denominator by $P_{ff}(u, v)$, which results in the equation \ref{eq:wiener-divided}.

\begin{equation}\label{eq:wiener-divided}
  R(u, v) = \frac{H^\ast(u, v)}{\left| H(u, v)\right|^2 + \frac{P_{ww}(u, v)}{P_{ff}(u, v)}}
\end{equation}

The only unknown part of the right side of the equation remains $\frac{P_{ww}(u, v)}{P_{ff}(u, v)}$. This expression is division of the noise power by the power of the original signal.

Let's assume that the value of the expression doesn't change too much across the image -- the ratio of noise to signal is always about the same.

The noise to signal ratio can be replaced by a~constant $\alpha \in [0, 1]$.

The filter function is described in the equation \ref{eq:wiener-final}.

\begin{equation}\label{eq:wiener-final}
  R(u, v) = \frac{H^\ast(u, v)}{\left| H(u, v)\right|^2 + \alpha}
\end{equation}

And finally, the estimation of the image in the frequency domain is:

\begin{equation}\label{eq:wiener-complete}
  \hat{F}(u, v) = \frac{H^\ast(u, v) G(u, v)}{\left| H(u, v)\right|^2 + \alpha}.
\end{equation}

\subsection{Gaussian filter}\label{sec:gauss-description}

Gaussian filter is used to blur images \cite{gauss-auckland}. It suppresses random noise but also blends details (unlike bilateral filter in section \ref{sec:bilateral-description}). Gaussian filtering is effective in removing Gaussian noise but not less effective in removing salt and pepper noise \cite{gauss-auckland}.

The kernel of the Gaussian filter is computed by the 2D Gaussian function:

\begin{equation}\label{eq:gauss-2d}
  G(x, y)=\frac{1}{2\pi\sigma^2} exp\left(-\frac{x^2 + y^2}{2\sigma^2}\right),
\end{equation}

where $\sigma^2$ is the variance of the Gaussian function (assuming that there is the same variance for the horizontal and vertical direction) and $exp(x)$ is the exponential function $e^x$.

Value of each pixel in the blurred image is weighted average of other values in the image \cite{gauss-fisher}. The central pixels in the Gaussian function have a~higher value than the pixels on the periphery \cite{gauss-auckland}. Therefore, value of each pixel in the blurred image is mostly influenced by its direct neighbourhood.

The Gaussian function must be discretized to be used as a~kernel in discrete filter according to equation \ref{eq:linear-discrete-filter}. The Gaussian function is never zero and thus the kernel function would be infinite. Therefore, the peripheral area of the function with low values has to be cut off \cite{gauss-fisher}.

It is also possible to speed up the calculation of the convolution by computing the horizontal and vertical components independently \cite{gauss-fisher}. First, the image is convolved with a~1D Gaussian function in one direction and then the result is convolved with a~1D Gaussian in the other direction.

The 1D Gaussian is computed as:

\begin{equation}
  G(x) = \frac{1}{\sqrt{2\pi\sigma^2}} exp\left(-\frac{x^2}{2\sigma^2}\right).
\end{equation}


\begin{figure}
  \centering
  % \hfill
  \begin{subfigure}[b]{0.32\textwidth}
      \centering
      \includegraphics[width=\textwidth]{img/gauss.pdf}
      % \caption{Input image}
  \end{subfigure}
  % \hfill
  \hspace{1cm}
  \begin{subfigure}[b]{0.2\textwidth}
      \centering
      \includegraphics[width=\textwidth]{img/gauss-discrete.pdf}
      % \caption{}
  \end{subfigure}
  % \hfill
  \caption{Visualization of a~2D Gaussian function and its discrete version}\label{fig:gauss}
\end{figure}

\subsection{Bilateral filter}\label{sec:bilateral-description}

The bilateral filter is the only non-linear filter described in this chapter. It means that the filter can't be described as a~convolution of some kernel with the image because the kernel is unique for each pixel \cite{filters}.

Bilateral filter smooths images while preserving edges \cite{bilateral-tomasi}.

Blurring filters often compute the value of a~pixel from its neighbours. Such filters rely on the assumption that the pixels around the currently computed pixel are similar because they depict the same object.

It s often true but this assumption fails if there is a~sharp edge in the image. Gauss filter and other similar filters blur the edges.

Bilateral filter introduces a~different definition of \textit{similarity} of the pixels. It uses a~combination of ``closeness similarity'' and ``range similarity'' \cite{bilateral-tomasi} -- a~pixel is similar to another pixel if they are located close to each other and if they have similar values.

The most often way to compute closeness similarity in the bilateral filter is the Gaussian function but it is possible to use other functions \cite{bilateral-tomasi}.

Kernel of the bilateral filter is computed for each pixel independently as multiplication of the Gaussian (or another) function and difference of the center pixel to the other pixels (see equation \ref{eq:bilateral}). Figure \ref{fig:bilateral-kernel} shows an example of such kernel.

Equation \ref{eq:bilateral} describes computation of discrete kernel $h_b$ for pixel $(x, y)$ using the closeness similarity function $h_c$ and the range similarity $h_r$.

\begin{equation}\label{eq:bilateral}
  h_b(t, u) = \frac{h_c(t-x, u-y) \cdot h_r\left(f(x, y) - f(t, u)\right)}{\sum_{r=1}^{width} \sum_{s=1}^{height} h_c(r-x, s-y) \cdot h_r\left(f(x, y) - f(r, s)\right)}
\end{equation}

A common closeness similairity function is the Gaussian function \cite{bilateral-sykora}. The range similarity often is the absolute value \cite{bilateral-tomasi}. In such case, the kernel would look like this \cite{bilateral-sykora}:

\begin{equation}\label{eq:bilateral-gauss}
  h_b(t, u) = \frac{G(t-x, u-y) \cdot \left|f(x, y) - f(t, u)\right|}{\sum_{r=1}^{width} \sum_{s=1}^{height} G(r-x, s-y) \cdot \left|f(x, y) - f(r, s)\right|}.
\end{equation}

\begin{figure}
  \begin{subfigure}[b]{0.32\textwidth}
      \centering
      \includegraphics[width=\textwidth]{img/bilateral-filter-input.pdf}
      \caption{Input image}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.32\textwidth}
      \centering
      \includegraphics[width=\textwidth]{img/bilateral-filter-kernel.pdf}
      \caption{Kernel for the center pixel}\label{fig:bilateral-kernel}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.32\textwidth}
      \centering
      \includegraphics[width=\textwidth]{img/bilateral-filter-output.pdf}
      \caption{Filtered image}
  \end{subfigure}
  \caption[Bilateral filter]{Bilateral filter. Image source: \cite{bilateral-tomasi}.}\label{fig:bilateral}
\end{figure}

\section{Thresholding}\label{sec:thresholding}

Thresholding is a~segmentation technique where pixels with value higher or equal specified parameter $\tau$ are considered to be part of the foreground and pixels with the value lower than $\tau$ are part of the background \cite{segmentation-overview}. The input image is segmented into several foreground blobs divided from each other by the background.

There are three common types of thresholding which treat the parameter $\tau$ differently -- global, adaptive and local.

Global thresholding uses the same value of $\tau$ across the whole image \cite{segmentation-overview}. Adaptive thresholding computes the value of $\tau$ using the position of the pixel \cite{segmentation-overview}. In the local thresholding, $\tau$ depends on the neighbourhood of the pixel \cite{segmentation-overview}.

\section{Flat-field correction}

Flat-field correction fixes effects of non-uniform illumination of a~photograph \cite{flatfield}.

Pixels in the center of a~photograph tend to be lighter than those on the margin even when capturing the same object. This phenomenon is called vignetting and is caused by physical limitations of the lens and the aperture \cite{applied-photography-vignetting}.

There is plenty of imperfections in camera sensor illumination beside vignetting. A large variety of approaches were created to eliminate them.

As Kask et al \cite{flatfield} point out, those approaches assume that there is a~function with the pixel location as input, that influences the value of the pixel in the processed image. This function can be called shading function \cite{flatfield} or flat-field function \cite{misao}.

Generally, there are two types of the flat-field function -- additive functions whose values are added to the original image (added background) and multiplicative whose value is multiplied with the pixel values (vignetting or another illumination imperfection) \cite{flatfield}.

Construction of the distorted image $I$ using the true object image $U$ is described in the function \ref{eq:flatfield-generic}. $S_M$ and $S_A$ are multiplicative and additive flat-field functions.

\begin{equation}\label{eq:flatfield-generic}
 I(x, y) = U(x, y) \cdot S_M(x, y) + S_A(x, y)
\end{equation}

The true function can be estimated using estimations of the functions $\hat S_M$ and $\hat S_A$:

\begin{equation}\label{eq:flatfield-generic-reverse}
  \hat U(x, y) = \frac{I(x, y) - \hat S_A(x, y)}{S_M(x, y)}.
\end{equation}

A flat-field correction technique can use both of the flat-field functions or just one of them \cite{flatfield}.

Yoshida \cite{misao}, mentioned in the \nameref{chap:research} chapter, uses the additive flat-field function in equation \ref{eq:flatfield-yoshida} to estimate the background of an image depicting stars.

\begin{equation}\label{eq:flatfield-yoshida}
\hat S_A(x, y) = a + bx + cy + dx^2 + ey^2 + fxy
\end{equation}

\section{Region growing}\label{sec:region-growing}

Region growing is a~segmentation technique that finds regions that satisfy some predefined similarity criterion \cite{region-growing}.

The algorithm needs a~set of starting pixels (seeds) which are then expanded.

The algorithm consists of three simple steps:

\begin{enumerate}
  \item \label{region-growing:start} Choose one seed pixel. Insert the seed pixel into an empty set called ``region''.
  \item \label{region-growing:insert} Find all pixels neighbouring to any pixels from the region. Add them into the region if they are similar to the seed pixel. The similarity of the pixels is checked using the similarity criterion.
  \item Repeat step \ref{region-growing:insert} until there are no pixels to add.
  \item Repeat step \ref{region-growing:start} with another pixel until there are no unprocessed seed pixels.
\end{enumerate}

\section{SRRF}\label{sec:SRRF-theory}

Super-Resolution Radial Fluctuations (SRRF) is a~purely analytical approach for increasing image resolution \cite{gustaffson16}.

``Purely analytical'' means that it can increase the resolution of already existing microscope images and doesn't require a~specific process during obtaining the image -- unlike methods like PALM, STORM and STED to which it is often compared \cite{culley18, gustaffson16, srrf-henriques}.

The input of the algorithm is a~series of images of point sources, not necessarily fluorescing protein \cite{gustaffson16}. The image recorded by a~microscope resulted as convolution of two functions: original point sources and point spread function \cite{gustaffson16} (PSF, function describing how does the microscope record point source). The goal of the algorithm is to obtain an image as close as possible to the original point sources.

The algorithm relies on the assumption that the image of a~particle convolved with the PSF is radially symmetric with center in the original position of the particles.

The word symmetry in image processing means invariance of the image to some transformation. Radial symmetry is invariance to rotation around the center.

Common approximations of the PSF like Airy's function or Gaussian function are invariant to rotation.

SRRF breaks every pixel in the image into subpixels.

A special transform called ``radiality'' is applied to each subpixel in the image. Radiality is computed inside a~fixed-size window around the subpixel. This transform highlights subpixels with high radial symmetry inside the window \cite{srrf-henriques}.

% Radiality is defined as the degree of local gradient convergence \cite{gustaffson16}.

To suppress the influence of noise, SRRF multiplies the radiality by the input intensity \cite{gustaffson16}. Noise in the image might be radially symmetric but it often has a~low intensity.

If there are more input images in the input series, one single radiality image is created from them, which also decreases the level of noise \cite{gustaffson16, srrf-henriques}.



% SRRF algorithm is implemented in the open source\footnote{Available at \url{https://github.com/HenriquesLab/NanoJ-SRRF}} library NanoJ, which could be installed as a~plugin to the image processing tool ImageJ.

% Input of the algorithm is series of images of point sources, not necessarily fluorescing protein \cite{gustaffson16}. The image recorded by a~microscope resulted as convolution of two functions: original point sources and point spread function \cite{gustaffson16} (PSF, function describing how does the microscope record point source). Goal of the algorithm is to obtain image as close as possible to the original point sources.

% The algorithm assumes that the PSF has higher degree of local symmetry than the background \cite{gustaffson16}. 

% \todo\textit{And now I wil explain symmetry bcs I think the original articles miss this:} The word symmetry in image processing means invariance of the image to some transformation, i.e. invariance to rotation in case of circular symmetry.

% There are two types of symmetry -- global and local. Global symmetry is invariance of whole image to the transformation while \todo: formullation, move it somewhere else

\section{Morphological operators}\label{sec:morpholohy}

Morphological operators is a~set of image analysis techniques \cite{owens97}. They extract image components such as boundaries, skeletons, convex hulls \cite{owens97}. Those components can be used to analyze the shape of objects in the image \cite{hlavac}.

Morphological operators are designed to process binary images but they were generalized to be used for grayscale or colour images too \cite{owens97}.

The image is analyzed using a~matrix called ``structuring element''. The structuring element has only binary values (can be also perceived as a~set of points).

Most of the structuring elements are squares or circles. One point, mostly the center, of the structuring element is called the origin.

%The origin of the structuring element (mostly center) is shifted across the input image.

Each pixel of the output image is computed using the original input image and the structuring element with origin placed on the currently computed point. Pixels of the input image that are below positive pixels of the structuring element serve as an input of some operation -- the operations are different for different morphological operators.

\subsection{Basic operators}

The operator called \textit{dilation} expands the original object in the image.

%Operator called \textit{dilation} fills small holes in the image \cite{hlavac}.

Value of a~pixel in dilated image is 1 if any of the input values (below the structuring element) are 1. The pixel value is 0 only if all of the values are also 0.

The operator called \textit{erosion} reduces dimensions of the object in the image.

Value of a~pixel in an eroded image is 1 if all the values below the structuring element are also 1.

Those two operators are combined to create another, more complex morphological operators.

\subsection{Opening and closing}

Operations called opening and closing are composed using dilation and erosion. Assuming $I$ is the input image, they are defined as:

\begin{equation}
\mathrm{opening}(I) = \mathrm{dilation}(\mathrm{erosion}(I)),
\end{equation}

\begin{equation}
\mathrm{closing}(I) = \mathrm{erosion}(\mathrm{dilation}(I)).
\end{equation}


% There are two basic operations: sum and intersection.



% The input image is multiplied with the structuring element -- each element of he input image


% Some basic operation is called on the input image and structuring element.

% an basic operation depending, which depends on the particular morphological operation. Origin of the structuring element is placed on the currently computed pixel. The basic operation is applied to all pixels from the input image, on which lies a~point from the structuring element.

% The basic operation can be Minkowski sum used in \textit{dilation}. If any of the input values is 1, result of the Minkowski sum is also 1. If all values are zeros, the Minkowski sum is zero.

% Dilation fills small holes in the image \cite{hlavac}.

% $$A \oplus B = \bigcup_{a \in A} B_a$$

% Dilation is 

% Another basic operation is Minkowski subtraction used in erosion. The result of Minkowski subtraction is intersection of all input pixels. If all values of the input are 1, the result is also 1, otherwise it is 0.

% % $$A \ominus B = \bigcap_{a \in A} B_{-a}$$

% Operations called opening and closing are composed using dilation and erosion. Assuming $I$ is the input image, they are defined as:

% \begin{equation}
% \mathrm{opening}(I) = \mathrm{dilation}(\mathrm{erosion}(I)),
% \end{equation}

% \begin{equation}
% \mathrm{closing}(I) = \mathrm{erosion}(\mathrm{dilation}(I)).
% \end{equation}

\subsection{Top hat transform}\label{sec:tophat-description}

Top hat transform suppresses slow trends in the image, while it lefts abrupt changes of values untouched \cite{serra1}. For example, it deletes gradual changes of the background caused by vignetting. Objects smaller than the structuring element won't be affected by the transform \cite{serra1}.

Top hat enhances image contrast \cite{serra1}.

The top hat transform is obtained as \cite{serra1}:

\begin{equation}
\mathrm{tophat}(I) = I - \mathrm{opening}(I).
\end{equation}

A similar operation called bottom hat \cite{serra1} is described by this formula:

\begin{equation}
\mathrm{tophat^*}(I) = \mathrm{closing}(I) - I.
\end{equation}

While top hat retrieves light objects on a~dark background, the bottom hat detects dark objects on a~light background \cite{tcheslavski}.

\subsection{Reconstruction by dilation}\label{sec:reconstruction-by-dilation}

The reconstruction by dilation operator removes objects smaller than the structuring element but doesn't (significantly) affect the bigger objects \cite{hmaxima}.

This operator doesn't use a~structuring element but rather another image called a~mask with equal size to the input image.

Morphological reconstruction can be understood as repeating of some operation (in this case dilation) until the output does not change.

The resulting image must ``fit under'' the mask. No pixel of the output image can have the value higher than the corresponding pixel in the mask. If it does, its value is lowered during every step to satisfy this constraint.

\subsection{H-maxima}\label{sec:hmaxima-description}

H-maxima transform suppresses all ``domes'', whose height is lower or equal a~threshold $h$ \cite{hmaxima}. It also lowers the value of all pixels by $h$.

It is defined in equation \ref{eq:hmaxima}, where $R_I^\delta(f)$ is reconstruction by dilation of $f$ using the mask $I$.

\begin{equation}\label{eq:hmaxima}
  HMAX_h(I) = R_I^\delta(I-h)
\end{equation}
