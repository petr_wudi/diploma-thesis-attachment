package edu.cvut.fit.wudipetr.particledetection.imagej.components;

import edu.cvut.fit.wudipetr.particledetection.imagej.ConfiguredMethods;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.MethodInfo;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.ManualProgramConfiguration;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;
import org.scijava.log.LogService;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Panel for selecting methods used to detect the proteins.
 */
public class MethodSelectPanel extends JPanel {
    private LogService log;
    private final ManualProgramConfiguration programConfiguration;
    private final JButton submitButton;

    /**
     * Creates new panel with list of methods.
     * @param programConfiguration configuration of the detection algorithm
     * @param log logger
     */
    public MethodSelectPanel(ManualProgramConfiguration programConfiguration,
                             LogService log) {
        setLog(log);
        this.programConfiguration = programConfiguration;

        setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;

        final JPanel preprocessingSelectPanel = createPreprocessingSelectPanel();
        add(preprocessingSelectPanel, gbc);
        final JPanel detectionSelectPanel = createDetectionSelectPanel();
        add(detectionSelectPanel, gbc);
        final JPanel postprocessingSelectPanel = createPostprocessingSelectPanel();
        add(postprocessingSelectPanel, gbc);

        submitButton = new JButton("Proceed to parameter setting");
        final JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.PAGE_AXIS));
        submitButton.setAlignmentX(RIGHT_ALIGNMENT);
        buttonPane.add(submitButton);
        add(buttonPane, gbc);

    }

    private JPanel createPreprocessingSelectPanel() {
        List<MethodInfo<Preprocessing>> preprocessingMethods = ConfiguredMethods.getPreprocessing();
        return createMethodSelectPanel("Preprocessing", preprocessingMethods, programConfiguration::setPreprocess);
    }

    private JPanel createDetectionSelectPanel() {
        List<MethodInfo<ProteinDetector>> detectionMethods = ConfiguredMethods.getDetectors();
        return createMethodSelectPanel("Detection", detectionMethods, programConfiguration::setDetector);
    }

    private JPanel createPostprocessingSelectPanel() {
        List<MethodInfo<Validator>> postprocessingMethos = ConfiguredMethods.getValidators();
        return createMethodSelectPanel("Postprocessing (validation)", postprocessingMethos, programConfiguration::setPostprocess);
    }

    private <T> JPanel createMethodSelectPanel(String name, List<MethodInfo<T>> methods, Consumer<MethodInfo<T>> setMethodConsumer) {
        final JPanel panel = new JPanel();
        panel.setAlignmentX(Component.LEFT_ALIGNMENT);
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.setBorder(new TitledBorder(name));
        ButtonGroup group = new ButtonGroup();
        Map<String, MethodInfo<T>> methodConfigMap = new HashMap<>();
        ActionListener actionListener = actionEvent -> {
            String methodName = actionEvent.getActionCommand();
            if (methodName == null) {
                log.error("Method name not set.");
                return;
            }
            MethodInfo<T> methodInfo = methodConfigMap.get(methodName);
            if (methodInfo == null) {
                log.error("Method named " + methodName + " was not found.");
                return;
            }
            setMethodConsumer.accept(methodInfo);
        };
        boolean isFirst = true;
        for (MethodInfo<T> method : methods) {
            JRadioButton methodButton = new JRadioButton(method.getName());
            methodConfigMap.put(method.getIdentifier(), method);
            methodButton.setActionCommand(method.getIdentifier());
            panel.add(methodButton);
            group.add(methodButton);
            methodButton.addActionListener(actionListener);
            if (isFirst) {
                methodButton.setSelected(true);
                setMethodConsumer.accept(method);
                isFirst = false;
            }
        }
        return panel;
    }

    public void setLog(LogService log) {
        this.log = log;
    }

    public void setSubmitAction(ActionListener buttonClickEvent) {
        submitButton.addActionListener(buttonClickEvent);
    }
}
