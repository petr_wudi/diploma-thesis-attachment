package edu.cvut.fit.wudipetr.particledetection.imagej.dialogs;

import edu.cvut.fit.wudipetr.particledetection.imagej.components.AdvancedMethodsPanel;
import edu.cvut.fit.wudipetr.particledetection.imagej.components.PreconfiguredPanel;
import ij.gui.GenericDialog;
import ij.gui.NonBlockingGenericDialog;
import org.scijava.log.LogService;
import org.scijava.thread.ThreadService;
import org.scijava.ui.UIService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.net.URL;

/**
 * Dialog for configuration of the whole detection algorithm (including preprocessing and postprocessing phases).
 */
public class DetectionMethodDialog extends NonBlockingGenericDialog {

    private static final String WINDOW_TITLE = "Protein particle detection";
    private static final String TAB_TITLE_PRECONFIGURED = "Preconfigured";
    private static final String TAB_TITLE_ADVANCED = "Advanced";

    private LogService log;

    private final PreconfiguredPanel preconfiguredPanel;
    private final AdvancedMethodsPanel advancedMethodsPanel;

    /**
     * Creates new dialog for configuration of the algorithm.
     * @param log logger for cases of UI creation failure
     */
    public DetectionMethodDialog(LogService log) {
        super(WINDOW_TITLE);
        setLog(log);
        setBounds(100, 100, 450, 500);
        setLayout(new BorderLayout());

        preconfiguredPanel = new PreconfiguredPanel(() -> this.setVisible(false));
        advancedMethodsPanel = new AdvancedMethodsPanel(log, () -> this.setVisible(false));

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab(TAB_TITLE_PRECONFIGURED, getIcon("tab-preconfigured"), preconfiguredPanel);
        tabbedPane.addTab(TAB_TITLE_ADVANCED, getIcon("tab-advanced"), advancedMethodsPanel);
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
        add(BorderLayout.CENTER, tabbedPane);
    }

    private Icon getIcon(String name) {
        String path = "/icon/" + name + ".png";
        URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            log.error("Couldn't find file: " + path);
            return null;
        }
    }

    public void setLog(final LogService log) {
        this.log = log;
        if (advancedMethodsPanel != null) {
            advancedMethodsPanel.setLog(log);
        }
        if (preconfiguredPanel != null) {
            preconfiguredPanel.setLog(log);
        }
    }

    public void setThreadService(final ThreadService threadService) {
        this.preconfiguredPanel.setThreadService(threadService);
        this.advancedMethodsPanel.setThreadService(threadService);
    }

    public void setUiService(final UIService uiService) {
        preconfiguredPanel.setUiService(uiService);
        advancedMethodsPanel.setUiService(uiService);
    }
}