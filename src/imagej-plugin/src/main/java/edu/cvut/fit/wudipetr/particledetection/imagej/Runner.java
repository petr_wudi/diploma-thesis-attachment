package edu.cvut.fit.wudipetr.particledetection.imagej;

import edu.cvut.fit.wudipetr.particledetection.imagej.dialogs.MethodProcessingMonolog;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.MethodConfiguration;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.AwtImage;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import org.scijava.log.LogService;
import org.scijava.thread.ThreadService;
import org.scijava.ui.UIService;

import java.awt.*;
import java.util.Collection;
import java.util.List;

public class Runner {
    private static final int PROTEIN_COLOR = 0x00bbbb;

    private LogService log;
    private UIService uiService;
    private ThreadService threadService;

    private final MethodProcessingMonolog methodProcessingMonolog = new MethodProcessingMonolog();

    public void findParticles(List<MethodConfiguration<Preprocessing>> preprocessProgramConfigs,
                              MethodConfiguration<ProteinDetector> detectorProgramConfig,
                              List<MethodConfiguration<Validator>> postprocessProgramConfigs,
                              Runnable hideWindow) {
        log.info("Finding particles.");
        final ImagePlus imagePlus = IJ.getImage();
        ProteinDetector proteinDetector = getStepEvaluator(detectorProgramConfig);
        if (proteinDetector == null) {
            log.error("No detection algorithm was found.");
            return;
        }
        log.info("Preprocessing");
        methodProcessingMonolog.setPhase("Preprocessing");
        hideWindow.run();
        methodProcessingMonolog.setVisible(true);
        Image pdImage = new AwtImage(imagePlus.getBufferedImage());
        for (MethodConfiguration<Preprocessing> preprocessProgramConfig : preprocessProgramConfigs) {
            Preprocessing preprocessing = getStepEvaluator(preprocessProgramConfig);
            if (preprocessing != null) {
                methodProcessingMonolog.setPhase("Preprocessing " + preprocessing.getClass().getSimpleName());
                pdImage = preprocessing.apply(pdImage);
            } else {
                log.info("No preprocessing");
            }
        }
        methodProcessingMonolog.setPhase("Detection");
        log.info("Detection: " + proteinDetector.getClass().getName());
        Collection<Protein> proteins = proteinDetector.detectMasks(pdImage);
        methodProcessingMonolog.setPhase("Validation");
        for (MethodConfiguration<Validator> postprocessProgramConfig : postprocessProgramConfigs) {
            Validator validator = getStepEvaluator(postprocessProgramConfig);
            if (validator != null) {
                log.info("Validation: " + validator.getClass().getName());
                proteins = validator.filter(proteins, pdImage);
            } else {
                log.info("No validation");
            }
        }
        methodProcessingMonolog.setPhase("Creating result image");
        ImagePlus proteinImage = createProteinImage(proteins, imagePlus);
        methodProcessingMonolog.setVisible(false);
        uiService.show(proteinImage);
    }

    private <T> T getStepEvaluator(MethodConfiguration<T> programConfig) {
        return programConfig == null ? null : programConfig.createMethodInstance();
    }

    private ImagePlus createProteinImage(Collection<Protein> proteins, ImagePlus inputImage) {
        ImagePlus outputImage = inputImage.duplicate();
        ImageProcessor outputImageProcessor = outputImage.getProcessor();
        for (Protein protein : proteins) {
            ImagePoint position = protein.getPosition();
            drawProtein(outputImageProcessor, position, inputImage.getWidth(), inputImage.getHeight(), 5);
        }
        return outputImage;
    }

    private void drawProtein(ImageProcessor imageProcessor, ImagePoint position, int imageWidth, int imageHeight,
                             int radius) {
        int x = position.getX();
        int y = position.getY();
        for (int crossX = Integer.max(x - radius, 0); crossX <= Integer.min(x + radius, imageWidth - 1); ++crossX) {
            imageProcessor.putPixel(crossX, y, PROTEIN_COLOR);
        }
        for (int crossY = Integer.max(y - radius, 0); crossY <= Integer.min(y + radius, imageHeight - 1); ++crossY) {
            imageProcessor.putPixel(x, crossY, PROTEIN_COLOR);
        }
    }

    public void setLog(final LogService log) {
        this.log = log;
        methodProcessingMonolog.setLog(log);
    }

    public void setUiService(UIService uiService) {
        this.uiService = uiService;
    }

    public void setThreadService(ThreadService threadService) {
        this.threadService = threadService;
    }
}
