package edu.cvut.fit.wudipetr.particledetection.imagej.components;

import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.MethodConfiguration;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.MethodParamInfo;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class RealNumberInput extends JTextField {

    public RealNumberInput(MethodParamInfo<?> param, int paramId, MethodConfiguration<?> parentConfiguration, int fieldWidth) {
        super();
        param.getDefaultValue()
                .map(Object::toString)
                .ifPresent(this::setText);
        getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                Object value = getText();
                double doubleValue;
                try {
                    doubleValue = Double.parseDouble(String.valueOf(value));
                } catch (NumberFormatException e) {
                    setBackground(new Color(0xff0000));
                    return;
                }
                setBackground(new Color(0xffffff));
                parentConfiguration.setParam(paramId, doubleValue);
                parentConfiguration.setParam(param, doubleValue);
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                insertUpdate(documentEvent);
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                insertUpdate(documentEvent);
            }
        });
        int preferredHeight = (int) getPreferredSize().getHeight();
        setPreferredSize(new Dimension(fieldWidth, preferredHeight));
    }
}
