package edu.cvut.fit.wudipetr.particledetection.imagej.dialogs;

import ij.gui.GenericDialog;

/**
 * A window telling the user that loading OpenCV failed.
 */
public class LibraryFailMonolog extends GenericDialog {

    public LibraryFailMonolog() {
        super("Library loading error");
        addMessage("Loading OpenCV library failed.");
        addMessage("Please make sure you have put:");
        addMessage("- opencv-400.jar to the 'jars' directory");
        addMessage("- opencv_java400.dll (Windows) or opencv_java400.so (Mac, Linux) to the 'lib' directory");
    }
}
