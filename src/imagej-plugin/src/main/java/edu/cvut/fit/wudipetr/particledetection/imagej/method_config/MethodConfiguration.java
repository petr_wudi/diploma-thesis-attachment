package edu.cvut.fit.wudipetr.particledetection.imagej.method_config;

/**
 * Configuration of one method.
 * @param <T> type of the method
 */
public class MethodConfiguration<T> {
    private final MethodInfo<T> method;
    private final MethodParamCombination paramInstance;

    /**
     * Creates new configuration of a method
     * @param method not null, static information about the method that is configured
     */
    public MethodConfiguration(MethodInfo<T> method) {
        this.method = method;
        paramInstance = new MethodParamCombination();
    }

    /**
     * @return static information about the method (not including parameter configuration)
     */
    public MethodInfo<T> getMethod() {
        return method;
    }

    /**
     * Sets n-th parameter.
     * @param paramOrder number of the parameter (from 0) to be set
     * @param value new value of the parameter
     */
    public void setParam(int paramOrder, Object value) {
        paramInstance.set(paramOrder, value);
    }

    /**
     * Sets the specified parameter.
     * @param param parameter to be set
     * @param value new value of the parameter
     */
    public void setParam(MethodParamInfo<?> param, Object value) {
        paramInstance.set(param, value);
    }

    /**
     * Creates new runnable instance of the method using the configuration. Parameters should be set before calling
     * this method.
     * @return new instance of the method
     */
    public T createMethodInstance() {
        return method.createMethodInstance(paramInstance);
    }
}
