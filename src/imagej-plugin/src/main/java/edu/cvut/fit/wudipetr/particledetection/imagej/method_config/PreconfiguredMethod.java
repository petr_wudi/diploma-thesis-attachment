package edu.cvut.fit.wudipetr.particledetection.imagej.method_config;

import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;

import java.util.List;

/**
 * Combination of preprocessing, detection and validation methods that has been already created and might be offered
 * to the user as an option in the GUI.
 */
public class PreconfiguredMethod implements ProgramConfiguration {
    private final String name;
    private final String identifier;
    private final List<MethodConfiguration<Preprocessing>> preprocessing;
    private final MethodConfiguration<ProteinDetector> detection;
    private final List<MethodConfiguration<Validator>> postprocess;

    /**
     * Creates new preconfigured configuration of a protein detection program.
     * @param name user-friendly name of the configuration (displayed in the GUI)
     * @param identifier unique identifier of the configuration
     * @param preprocessing preprocessing methods in the order in which they should run
     * @param detection detection method
     * @param postprocess validation methods in the order in which they should run
     */
    public PreconfiguredMethod(String name,
                               String identifier,
                               List<MethodConfiguration<Preprocessing>> preprocessing,
                               MethodConfiguration<ProteinDetector> detection,
                               List<MethodConfiguration<Validator>> postprocess) {
        this.name = name;
        this.identifier = identifier;
        this.preprocessing = preprocessing;
        this.detection = detection;
        this.postprocess = postprocess;
    }

    @Override
    public List<MethodConfiguration<Preprocessing>> getPreprocessing() {
        return preprocessing;
    }

    @Override
    public MethodConfiguration<ProteinDetector> getDetection() {
        return detection;
    }

    @Override
    public List<MethodConfiguration<Validator>>  getPostprocess() {
        return postprocess;
    }

    /**
     * @return user-friendly name of the preconfigured program configuration
     */
    public String getName() {
        return name;
    }

    /**
     * @return unique identifier of the program configuration
     */
    public String getIdentifier() {
        return identifier;
    }
}
