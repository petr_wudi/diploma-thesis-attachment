package edu.cvut.fit.wudipetr.particledetection.imagej.method_config;

import java.util.Optional;

/**
 * Description of parameter of a method.
 * @param <T> data type of the parameter
 */
public class MethodParamInfo<T> {
    private final String name;
    private final MethodParamType type;
    private final T defaultValue;

    /**
     * Creates new description of a parameter.
     * @param name user-friendly name of the parameter
     * @param type type of the parameter
     */
    public MethodParamInfo(String name, MethodParamType type) {
        this.name = name;
        this.type = type;
        this.defaultValue = null;
    }

    /**
     * Creates new description of a parameter with a default value.
     * @param name user-friendly name of the parameter
     * @param type type of the parameter
     * @param value parameter's default value
     */
    public MethodParamInfo(String name, MethodParamType type, T value) {
        this.name = name;
        this.type = type;
        this.defaultValue = value;
    }

    /**
     * @return user-friendly name of the parameter
     */
    public String getName() {
        return name;
    }

    /**
     * @return type of the parameter
     */
    public MethodParamType getType() {
        return type;
    }

    /**
     * @return parameter's default value (null means that there is no default value)
     */
    public Optional<T> getDefaultValue() {
        return Optional.ofNullable(defaultValue);
    }
}
