package edu.cvut.fit.wudipetr.particledetection.imagej;

import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.*;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.*;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.*;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.HMaximaValidator;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.SizeValidator;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class ConfiguredMethods {

    public static List<MethodInfo<Preprocessing>> getPreprocessing() {
        Function<MethodParamCombination, Preprocessing> flatfield1SuplierFunction = (params) -> {
            double maxSigmaMultiplier = (double)  params.get(0);
            return new FlatfieldFilter(maxSigmaMultiplier);
        };
        MethodInfo<Preprocessing> flatfieldConfig1 = new MethodInfo<>("Flatfield background suppression", "flatfield1", flatfield1SuplierFunction);
        flatfieldConfig1.addParam(new MethodParamInfo("maximum sigma multiplier", MethodParamType.REAL));

        Function<MethodParamCombination, Preprocessing> flatfield2SuplierFunction = (params) -> new FlatfieldNormalizer();
        MethodInfo<Preprocessing> flatfieldConfig2 = new MethodInfo<>("Flatfield normalizer", "flatfield2", flatfield2SuplierFunction);


        Function<MethodParamCombination, Preprocessing> laplaceSuplierFunction = (params) -> {
            double multiplier = (double)  params.get(0);
            int kernelSize = (int)  params.get(1);
            return new LaplaceSharpen(multiplier, kernelSize);
        };
        MethodInfo<Preprocessing> laplaceConfig = new MethodInfo<>("Laplacian filter", "laplace", laplaceSuplierFunction);
        laplaceConfig.addParam(new MethodParamInfo("multiplier", MethodParamType.REAL));
        laplaceConfig.addParam(new MethodParamInfo("kernel size", MethodParamType.INT, 5));


        Function<MethodParamCombination, Preprocessing> gaussSuplierFunction = (params) -> {
            int kernelSize = (Integer)  params.get(0);
            int variance = (Integer) params.get(1);
            return new GaussBlur(kernelSize, variance);
        };
        MethodInfo<Preprocessing> gaussConfig = new MethodInfo<>("Gaussian filter", "gauss", gaussSuplierFunction);
        gaussConfig.addParam(new MethodParamInfo("kernelSize", MethodParamType.INT));
        gaussConfig.addParam(new MethodParamInfo("variance", MethodParamType.REAL));


        Function<MethodParamCombination, Preprocessing> bilateralSuplierFunction = (params) -> {
            int kernelSize = (Integer)  params.get(0);
            double stdColor = (double) params.get(1);
            double stdSpace = (double) params.get(2);
            return new BilateralFilter(kernelSize, stdColor, stdSpace);
        };
        MethodInfo<Preprocessing> bilateralConfig = new MethodInfo<>("Bilateral filter", "bilateral", bilateralSuplierFunction);
        bilateralConfig.addParam(new MethodParamInfo("kernel size", MethodParamType.INT));
        bilateralConfig.addParam(new MethodParamInfo("standard deviation of color", MethodParamType.REAL));
        bilateralConfig.addParam(new MethodParamInfo("standard deviation of space", MethodParamType.REAL));


        Function<MethodParamCombination, Preprocessing> wienerSuplierFunction = (params) -> {
            double ratio = (double) params.get(0);
            return new WienerDeconvolution(ratio);
        };
        MethodInfo<Preprocessing> wienerConfig = new MethodInfo<>("Wiener deconvolution", "wiener", wienerSuplierFunction);
        wienerConfig.addParam(new MethodParamInfo("signal to noise ratio", MethodParamType.REAL));


        Function<MethodParamCombination, Preprocessing> tophatSuplierFunction = (params) -> {
            int kernelDiameter = (int) params.get(0);
            return new TopHat(kernelDiameter);
        };
        MethodInfo<Preprocessing> tophatConfig = new MethodInfo<>("Top hat", "tophat", tophatSuplierFunction);
        tophatConfig.addParam(new MethodParamInfo("kernel diameter", MethodParamType.INT));


        Function<MethodParamCombination, Preprocessing> prewittSuplierFunction = (params) -> {
            int kernelDiameter = (int) params.get(0);
            return new PrewittOperator(kernelDiameter);
        };
        MethodInfo<Preprocessing> prewittConfig = new MethodInfo<>("Prewitt operator", "prewitt", prewittSuplierFunction);
        prewittConfig.addParam(new MethodParamInfo("kernel diameter", MethodParamType.INT));


        Function<MethodParamCombination, Preprocessing> prewittDoubleSuplierFunction = (params) -> {
            int kernelDiameter = (int) params.get(0);
            return new DoublePrewittOperator(kernelDiameter);
        };
        MethodInfo<Preprocessing> prewittDoubleConfig = new MethodInfo<>("Prewitt double operator", "prewittdouble", prewittDoubleSuplierFunction);
        prewittDoubleConfig.addParam(new MethodParamInfo("kernel diameter", MethodParamType.INT));

        Function<MethodParamCombination, Preprocessing> prewittSubtractFunction = (params) -> {
            int kernelDiameter = (int) params.get(0);
            return new PrewittSubtract(kernelDiameter);
        };
        MethodInfo<Preprocessing> prewittSubtractConfig = new MethodInfo<>("Prewitt subtract operator", "prewittsubtract", prewittSubtractFunction);
        prewittSubtractConfig.addParam(new MethodParamInfo("kernel diameter", MethodParamType.INT));


        Function<MethodParamCombination, Preprocessing> sobelSuplierFunction = (params) -> {
            int kernelDiameter = (int) params.get(0);
            return new SobelOperator(kernelDiameter);
        };
        MethodInfo<Preprocessing> sobelConfig = new MethodInfo<>("Sobel operator", "sobel", sobelSuplierFunction);
        sobelConfig.addParam(new MethodParamInfo("kernel diameter", MethodParamType.INT));


        Function<MethodParamCombination, Preprocessing> sobelDoubleSuplierFunction = (params) -> {
            int kernelDiameter = (int) params.get(0);
            return new DoubleSobelOperator(kernelDiameter);
        };
        MethodInfo<Preprocessing> sobelDoubleConfig = new MethodInfo<>("Double sobel operator", "sobeldouble", sobelDoubleSuplierFunction);
        sobelDoubleConfig.addParam(new MethodParamInfo("kernel diameter", MethodParamType.INT));


        Function<MethodParamCombination, Preprocessing> sobelSubtractSuplierFunction = (params) -> {
            int kernelDiameter = (int) params.get(0);
            return new SobelSubtract(kernelDiameter);
        };
        MethodInfo<Preprocessing> sobelSubractConfig = new MethodInfo<>("Sobel subtract operator", "sobelsubtract", sobelSubtractSuplierFunction);
        sobelSubractConfig.addParam(new MethodParamInfo("kernel diameter", MethodParamType.INT));

        return Arrays.asList(
                new MethodInfo<>("None", "none", (params) -> null),
//                new MethodInfo<>("Wiener", "wiener", )
                gaussConfig,
                flatfieldConfig1,
                flatfieldConfig2,
                laplaceConfig,
                bilateralConfig,
                wienerConfig,
                tophatConfig,
                prewittConfig,
                prewittDoubleConfig,
                prewittSubtractConfig,
                sobelConfig,
                sobelDoubleConfig,
                sobelSubractConfig
        );
    }

    public static List<MethodInfo<ProteinDetector>> getDetectors() {
        Function<MethodParamCombination, ProteinDetector> hmaximaSupplier = (params) -> {
            int h = (Integer) params.get(0);
            int winRadius = (Integer) params.get(1);
            int duplicityThreshold = (Integer) params.get(2);
            return new HMaximaDetector(h, winRadius, duplicityThreshold);
        };
        Function<MethodParamCombination, ProteinDetector> thresholdSupplier = (params) -> {
            int thresholdMin = (Integer) params.get(0);
            return new ThresholdDetector(thresholdMin);
        };
        Function<MethodParamCombination, ProteinDetector> localMaxSupplier = (params) -> {
            int thresholdMin = (Integer) params.get(0);
            return new LocalMaxDetector(thresholdMin);
        };
        Function<MethodParamCombination, ProteinDetector> localMax2Supplier = (params) -> {
            int thresholdMin = (Integer) params.get(0);
            return new SimpleLocalMaxDetector(thresholdMin);
        };
        Function<MethodParamCombination, ProteinDetector> regionGrowingSupplier = (params) -> {
            int threshold = (Integer) params.get(0);
            int minThreshold = (Integer) params.get(1);
            int diameter = (Integer) params.get(2);
            return new RegionGrowingDetector(threshold, minThreshold, diameter);
        };
        Function<MethodParamCombination, ProteinDetector> watershedSupplier = (params) -> {
            int threshold = (Integer) params.get(0);
            return new WatershedDetector(threshold);
        };

        MethodInfo<ProteinDetector> hmaximaConfig = new MethodInfo<>("H-maxima + NMS", "hmaxima", hmaximaSupplier);
        hmaximaConfig.addParam(new MethodParamInfo("h", MethodParamType.INT));
        hmaximaConfig.addParam(new MethodParamInfo("winRadius", MethodParamType.INT));
        hmaximaConfig.addParam(new MethodParamInfo("duplicityThreshold", MethodParamType.INT));
        MethodInfo<ProteinDetector> thresholdConfig = new MethodInfo<>("Threshold", "threshold", thresholdSupplier);
        thresholdConfig.addParam(new MethodParamInfo<>("thresholdMin", MethodParamType.INT, 150));
        MethodInfo<ProteinDetector> localMaxConfig = new MethodInfo<>("Local maximums", "locmax", localMaxSupplier);
        localMaxConfig.addParam(new MethodParamInfo<>("minIntensity", MethodParamType.INT, 20));
        MethodInfo<ProteinDetector> localMax2Config = new MethodInfo<>("Simple local maximums", "locmax2", localMax2Supplier);
        localMax2Config.addParam(new MethodParamInfo<>("minIntensity", MethodParamType.INT, 20));
        MethodInfo<ProteinDetector> regionGrowingConfig = new MethodInfo<>("Region growing", "reggrow", regionGrowingSupplier);
        regionGrowingConfig.addParam(new MethodParamInfo<>("regionIntensityRange", MethodParamType.INT, 20));
        regionGrowingConfig.addParam(new MethodParamInfo<>("minIntensity", MethodParamType.INT, 100));
        regionGrowingConfig.addParam(new MethodParamInfo<>("diameter", MethodParamType.INT, 20));
        MethodInfo<ProteinDetector> watershedConfig = new MethodInfo<>("Watershed", "watershed", watershedSupplier);
        watershedConfig.addParam(new MethodParamInfo<>("threshold", MethodParamType.INT, 100));
        return Arrays.asList(hmaximaConfig, thresholdConfig, localMaxConfig, localMax2Config, regionGrowingConfig);
    }

    public static List<MethodInfo<Validator>> getValidators() {
        Function<MethodParamCombination, Validator> sizeSupplier = (params) -> {
            int minArea = (Integer) params.get(0);
            return new SizeValidator(minArea);
        };
        Function<MethodParamCombination, Validator> hmaximaSupplier = (params) -> {
            int h = (Integer) params.get(0);
            return new HMaximaValidator(h);
        };
        MethodInfo<Validator> nonePostprocess = new MethodInfo<>("None", "none", (params) -> null);
        MethodInfo<Validator> sizeConfig = new MethodInfo<>("Filter out small objects", "size", sizeSupplier);
        sizeConfig.addParam(new MethodParamInfo("minArea", MethodParamType.INT));
        MethodInfo<Validator> hmaximaConfig = new MethodInfo<>("H-maxima", "hmaxima", hmaximaSupplier);
        hmaximaConfig.addParam(new MethodParamInfo("h", MethodParamType.INT));
        return Arrays.asList(nonePostprocess, sizeConfig, hmaximaConfig);
    }
}
