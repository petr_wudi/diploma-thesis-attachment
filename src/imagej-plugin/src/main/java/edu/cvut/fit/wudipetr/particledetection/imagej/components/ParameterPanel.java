package edu.cvut.fit.wudipetr.particledetection.imagej.components;

import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.MethodConfiguration;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.MethodParamInfo;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.MethodParamType;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.ManualProgramConfiguration;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

/**
 * Panel for setting parameters of protein detection (+preprocessing, validation) methods.
 */
public class ParameterPanel extends JPanel {
    public static final String SUBMIT_BUTTON_TEXT = "Detect Particles";
    private static final int TEXT_FIELD_WIDTH = 150;
    private final Border LABEL_BORDER = BorderFactory.createEmptyBorder(0, 0, 0, 5);
    private final Border VALUE_BORDER = BorderFactory.createEmptyBorder(0, 5, 0, 0);

    private final JButton backButton;
    private final JButton submitButton;
    private final JPanel preprocessingPanel;
    private final JPanel detectionPanel;
    private final JPanel validatePanel;

    private final ManualProgramConfiguration programConfiguration;

    public ParameterPanel(ManualProgramConfiguration programConfiguration) {
        this.programConfiguration = programConfiguration;
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        preprocessingPanel = createParamPanel("Preprocessing");
        add(preprocessingPanel);
        detectionPanel = createParamPanel("Detection");
        add(detectionPanel);
        validatePanel = createParamPanel("Postprocessing");
        add(validatePanel);


        JPanel bottomPanel = new JPanel();
        backButton = new JButton("Back to method select");
        bottomPanel.add(backButton);
        submitButton = new JButton(SUBMIT_BUTTON_TEXT);
        bottomPanel.add(submitButton);
        add(bottomPanel);
    }

    private JPanel createParamPanel(String name) {
        JPanel paramPanel = new JPanel();
        paramPanel.setBorder(new TitledBorder(name));
        return paramPanel;
    }

    public void refresh() {
        setParams(preprocessingPanel, programConfiguration.getPreprocessing());
        setParams(detectionPanel, programConfiguration.getDetector());
        setParams(validatePanel, programConfiguration.getPostprocess());
    }

    private String getMethodName(MethodConfiguration<?> programConfig) {
        return programConfig.getMethod() == null ? "---" : programConfig.getMethod().getName();
    }

    private void setParams(JPanel paramsPanel, MethodConfiguration<?> methodConfiguration) {
        String methodName = getMethodName(methodConfiguration);
        List<MethodParamInfo<?>> parameters = methodConfiguration.getMethod().getParams();
        paramsPanel.removeAll();
        int numberOfRows = parameters.size() + 1;
        paramsPanel.setLayout(new GridLayout(numberOfRows, 2));

        final JLabel methodNameLeftLabel = new JLabel("Method name:");
        methodNameLeftLabel.setHorizontalAlignment(JLabel.RIGHT);
        methodNameLeftLabel.setBorder(LABEL_BORDER);
        final JLabel methodNameRightLabel = new JLabel(methodName); // todo
        methodNameRightLabel.setBorder(VALUE_BORDER);
        paramsPanel.add(methodNameLeftLabel, 0);
        paramsPanel.add(methodNameRightLabel, 1);


        Iterator<MethodParamInfo<?>> paramIterator = parameters.iterator();
        for (int rowId = 1; rowId <= parameters.size(); ++rowId) {
            int paramId = rowId - 1;
            MethodParamInfo<?> param = paramIterator.next();
            final JLabel keyLabel;
            if (param.getType() != MethodParamType.BOOLEAN) {
                keyLabel = new JLabel(param.getName());
            } else {
                keyLabel = new JLabel();
            }
            keyLabel.setBorder(LABEL_BORDER);
            keyLabel.setHorizontalAlignment(JLabel.RIGHT);
            paramsPanel.add(keyLabel, rowId * 2);

            JComponent component = createComponent(param, paramId, methodConfiguration);
            JPanel componentPanel = new JPanel();
            FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
            componentPanel.setLayout(layout);
            componentPanel.add(component);
            componentPanel.setBorder(VALUE_BORDER);
            paramsPanel.add(componentPanel, rowId * 2 + 1);
        }
    }

    private JComponent createComponent(MethodParamInfo<?> param, int paramId, MethodConfiguration<?> parentConfiguration) {
        switch (param.getType()) {
            case INT:
                JSpinner spinner = new JSpinner();
                // tod automatically set
                spinner.addChangeListener((ChangeEvent changeEvent) -> {
                    Object value = spinner.getValue();
                    parentConfiguration.setParam(paramId, value);
                    parentConfiguration.setParam(param, value);
                });
                param.getDefaultValue()
                        .ifPresent(spinner::setValue);
                int preferredHeight = (int) spinner.getPreferredSize().getHeight();
                spinner.setPreferredSize(new Dimension(TEXT_FIELD_WIDTH, preferredHeight));
                return spinner;
            case BOOLEAN:
                JCheckBox checkBox = new JCheckBox(param.getName());
                checkBox.addActionListener(changeEvent -> {

                });
                return checkBox;
            case REAL:
                return new RealNumberInput(param, paramId, parentConfiguration, TEXT_FIELD_WIDTH);
            default:
                throw new RuntimeException("Invalid param type: " + param.getType());
        }
    }

    /**
     * Sets action of the "back" button.
     * @param buttonClickEvent new action of the button
     */
    public void setBackAction(ActionListener buttonClickEvent) {
        backButton.addActionListener(buttonClickEvent);
    }

    /**
     * Sets action of the "submit" button.
     * @param buttonClickEvent new action of the button
     */
    public void setSubmitAction(ActionListener buttonClickEvent) {
        submitButton.addActionListener(buttonClickEvent);
    }
}
