package edu.cvut.fit.wudipetr.particledetection.imagej.method_config;

import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;

import java.util.List;

/**
 * Combination of preprocessing, detection and validation methods.
 */
public interface ProgramConfiguration {
    /**
     * @return configuration of the preprocessing method used by the protein detection algorithm
     */
    List<MethodConfiguration<Preprocessing>> getPreprocessing();

    /**
     * @return configuration of the detection method used by the whole protein detection algorithm
     */
    MethodConfiguration<ProteinDetector> getDetection();

    /**
     * @return configuration of the postprocessing method used by the protein detection algorithm
     */
    List<MethodConfiguration<Validator>>  getPostprocess();
}
