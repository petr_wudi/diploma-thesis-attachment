package edu.cvut.fit.wudipetr.particledetection.imagej.components;

import edu.cvut.fit.wudipetr.particledetection.imagej.Runner;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.MethodConfiguration;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.ManualProgramConfiguration;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;
import org.scijava.log.LogService;
import org.scijava.thread.ThreadService;
import org.scijava.ui.UIService;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;

/**
 * Panel allowing the user to manually set sequence of algorithms to be run.
 */
public class AdvancedMethodsPanel extends JPanel {

    private ThreadService threadService;

    private final MethodSelectPanel methodSelectPanel;
    private final ParameterPanel parameterPanel;
    private final ManualProgramConfiguration programConfiguration = new ManualProgramConfiguration();
    private final Runner runner = new Runner();
    private final Runnable hideWindow;

    /**
     * Creates new panel with manual algorithm configuration.
     * @param log logger
     * @param hideWindow procedure that hides the configuration window
     */
    public AdvancedMethodsPanel(LogService log, Runnable hideWindow) {
        methodSelectPanel = new MethodSelectPanel(programConfiguration, log);
        methodSelectPanel.setSubmitAction(actionEvent -> setParameterPanel());
        parameterPanel = new ParameterPanel(programConfiguration);
        parameterPanel.setBackAction(actionEvent -> setMethodSelectPanel());
        parameterPanel.setSubmitAction(actionEvent -> threadService.run(this::findParticles));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(methodSelectPanel);
        this.hideWindow = hideWindow;
    }

    private void findParticles() {
        MethodConfiguration<Preprocessing> preprocessProgramConfig = programConfiguration.getPreprocessing();
        MethodConfiguration<ProteinDetector> detectorProgramConfig = programConfiguration.getDetector();
        MethodConfiguration<Validator> postprocessProgramConfig = programConfiguration.getPostprocess();
        runner.findParticles(Collections.singletonList(preprocessProgramConfig),
                detectorProgramConfig,
                Collections.singletonList(postprocessProgramConfig),
                hideWindow);
    }

    /**
     * Step forward from method select panel to parameter setting panel.
     */
    private void setParameterPanel() {
        threadService.run(() -> {
            remove(methodSelectPanel);
            parameterPanel.refresh();
            add(BorderLayout.CENTER, parameterPanel);
            repaint();
            revalidate();
        });
    }

    /**
     * Go back from parameter panel to method select panel.
     */
    private void setMethodSelectPanel() {
        threadService.run(() -> {
            remove(parameterPanel);
            add(BorderLayout.CENTER, methodSelectPanel);
            repaint();
            revalidate();
        });
    }

    public void setThreadService(final ThreadService threadService) {
        this.threadService = threadService;
    }

    public void setLog(final LogService log) {
        if (methodSelectPanel != null) {
            methodSelectPanel.setLog(log);
        }
        runner.setLog(log);
    }

    public void setUiService(final UIService uiService) {
        runner.setUiService(uiService);
    }
}
