package edu.cvut.fit.wudipetr.particledetection.imagej.method_config;

import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;

import java.util.HashMap;
import java.util.Map;

/**
 * Manual configuration of a protein detection algorithm.
 */
public class ManualProgramConfiguration {
    private final Map<MethodInfo<Preprocessing>, MethodConfiguration<Preprocessing>> preprocessMap = new HashMap<>();
    private final Map<MethodInfo<ProteinDetector>, MethodConfiguration<ProteinDetector>> detectorMap = new HashMap<>();
    private final Map<MethodInfo<Validator>, MethodConfiguration<Validator>> postprocessMap = new HashMap<>();
    private MethodConfiguration<Preprocessing> preprocessProgramConfig = null;
    private MethodConfiguration<ProteinDetector> detectorProgramConfig = null;
    private MethodConfiguration<Validator> postprocessProgramConfig = null;

    /**
     * Selects preprocessing method used by the algorithm.
     * @param preprocessMethodInfo static information about the selected method
     */
    public void setPreprocess(MethodInfo<Preprocessing> preprocessMethodInfo) {
        this.preprocessProgramConfig = preprocessMap.computeIfAbsent(preprocessMethodInfo, MethodConfiguration::new);
    }

    /**
     * Selects detection method used by the algorithm.
     * @param detectorMethodInfo static information about the selected method
     */
    public void setDetector(MethodInfo<ProteinDetector> detectorMethodInfo) {
        this.detectorProgramConfig = detectorMap.computeIfAbsent(detectorMethodInfo, MethodConfiguration::new);
    }

    /**
     * Selects postprocessing method used by the algorithm.
     * @param postprocessMethodInfo static information about the selected method
     */
    public void setPostprocess(MethodInfo<Validator> postprocessMethodInfo) {
        this.postprocessProgramConfig = postprocessMap.computeIfAbsent(postprocessMethodInfo, MethodConfiguration::new);
    }

    /**
     * @return configuration of the preprocessing method used by the protein detection algorithm
     */
    public MethodConfiguration<Preprocessing> getPreprocessing() {
        return preprocessProgramConfig;
    }

    /**
     * @return configuration of the detection method used by the whole protein detection algorithm
     */
    public MethodConfiguration<ProteinDetector> getDetector() {
        return detectorProgramConfig;
    }

    /**
     * @return configuration of the postprocessing method used by the protein detection algorithm
     */
    public MethodConfiguration<Validator> getPostprocess() {
        return postprocessProgramConfig;
    }
}
