package edu.cvut.fit.wudipetr.particledetection.imagej.components;

import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.*;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;
import org.scijava.log.LogService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Consumer;

public class PreconfiguredMethodPanel extends JPanel {

    private LogService log;

    private final JPanel radioButtonPanel = new JPanel();
    private final JPanel descriptionPanel = new JPanel();
    private JRadioButton methodButton;
    private Runnable selectAction;

    public PreconfiguredMethodPanel(PreconfiguredMethod method, ButtonGroup group, Consumer<PreconfiguredMethod> selectConsumer) {
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        add(radioButtonPanel);
        radioButtonPanel.setLayout(new BoxLayout(radioButtonPanel, BoxLayout.PAGE_AXIS));
        add(descriptionPanel);
        descriptionPanel.setLayout(new BoxLayout(descriptionPanel, BoxLayout.PAGE_AXIS));
        addButton(method, group);
        JLabel headline = new JLabel(method.getName());
        Font font = headline.getFont();
        headline.setFont(font.deriveFont(font.getStyle() & Font.BOLD));
        descriptionPanel.add(headline);
//        addTypeHeadliner("Preprocessing");
        for (MethodConfiguration<Preprocessing> preprocessingMethod : method.getPreprocessing()) {
            String methodName = preprocessingMethod.getMethod().getName();
            addMethodName(methodName, MethodType.PREPROCESSING);
        }
//        if (method.getPreprocessing().isEmpty()) {
//            addMethodName("---");
//        }
//        addTypeHeadliner("Detection");
        addMethodName(method.getDetection().getMethod().getName(), MethodType.DETECTION);
//        addTypeHeadliner("Validation");
        for (MethodConfiguration<Validator> postprocessMethod : method.getPostprocess()) {
            String methodName = postprocessMethod.getMethod().getName();
            addMethodName(methodName, MethodType.POSTPROCESSING);
        }
//        if (method.getPostprocess().isEmpty()) {
//            addMethodName("---");
//        }
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                select();
            }
        });

        selectAction = () -> selectConsumer.accept(method);
        methodButton.addActionListener(event -> selectConsumer.accept(method));
    }

    private void select() {
        methodButton.setSelected(true);
        selectAction.run();
    }

    private void addButton(PreconfiguredMethod method, ButtonGroup group) {
        methodButton = new JRadioButton();
//        methodConfigMap.put(method.getIdentifier(), method); // todo
        methodButton.setActionCommand(method.getIdentifier());
        group.add(methodButton);
        radioButtonPanel.add(methodButton);
    }

    private void addTypeHeadliner(String name) {
        JLabel headline = new JLabel(name);
        descriptionPanel.add(headline);
    }

    private void addMethodName(String name, MethodType methodType) {
        String methodTypeName = "D";
        if (methodType == MethodType.PREPROCESSING) {
            methodTypeName = "P";
        } else if (methodType == MethodType.POSTPROCESSING) {
            methodTypeName = "V";
        }
        JLabel label = new JLabel(methodTypeName + "  " + name);
        Font font = label.getFont();
        label.setFont(font.deriveFont(font.getStyle() ^ Font.BOLD));
        descriptionPanel.add(label);
    }

    public void setLog(LogService log) {
        this.log = log;
    }

    private enum MethodType {
        PREPROCESSING, DETECTION, POSTPROCESSING
    }
}
