package edu.cvut.fit.wudipetr.particledetection.imagej.components;

import edu.cvut.fit.wudipetr.particledetection.imagej.ConfiguredMethodCombinations;
import edu.cvut.fit.wudipetr.particledetection.imagej.Runner;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.PreconfiguredMethod;
import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.MethodConfiguration;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;
import org.scijava.log.LogService;
import org.scijava.thread.ThreadService;
import org.scijava.ui.UIService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

public class PreconfiguredPanel extends JPanel {

    private ThreadService threadService;
    private LogService log;

    private JPanel methodListPanel = new JPanel();
    private ButtonGroup buttonGroup = new ButtonGroup();
    private final List<PreconfiguredMethodPanel> preconfiguredMethods = new ArrayList<>();
    private PreconfiguredMethod selectedMethod;
    private Runner runner = new Runner();
    private final Runnable hideWindow;

    public PreconfiguredPanel(Runnable hideWindow) {
        setLayout(new BorderLayout());

        methodListPanel.setLayout(new BoxLayout(methodListPanel, BoxLayout.PAGE_AXIS));
        add(methodListPanel, BorderLayout.CENTER);

        JButton submitButton = new JButton(ParameterPanel.SUBMIT_BUTTON_TEXT);
        add(submitButton, BorderLayout.SOUTH);
        submitButton.addActionListener(this::findParticles);

        List<PreconfiguredMethod> preconfiguredMethods = ConfiguredMethodCombinations.get();
        for (PreconfiguredMethod method : preconfiguredMethods) {
            addMethod(method);
        }
        this.hideWindow = hideWindow;
    }

    private void addMethod(PreconfiguredMethod method) {
        PreconfiguredMethodPanel methodPanel = new PreconfiguredMethodPanel(method, buttonGroup, this::selectMethod);
        methodPanel.setAlignmentX(LEFT_ALIGNMENT);
        methodListPanel.add(methodPanel);
        preconfiguredMethods.add(methodPanel);
    }

    private void selectMethod(PreconfiguredMethod method) {
        this.selectedMethod = method;
    }

    private void findParticles(ActionEvent actionEvent) {
        if (this.selectedMethod == null) {
            log.error("Particle detection method not set");
            return;
        }
        threadService.run(() -> {
            List<MethodConfiguration<Preprocessing>> preprocess = selectedMethod.getPreprocessing();;
            MethodConfiguration<ProteinDetector> detect = selectedMethod.getDetection();
            List<MethodConfiguration<Validator>> postprocess = selectedMethod.getPostprocess();
            runner.findParticles(preprocess, detect, postprocess, hideWindow);
        });
    }

    public void setThreadService(ThreadService threadService) {
        this.threadService = threadService;
        this.runner.setThreadService(threadService);
    }

    public void setLog(final LogService log) {
        this.log = log;
        runner.setLog(log);
        preconfiguredMethods.forEach(method -> method.setLog(log));
    }

    public void setUiService(UIService uiService) {
        runner.setUiService(uiService);
    }
}
