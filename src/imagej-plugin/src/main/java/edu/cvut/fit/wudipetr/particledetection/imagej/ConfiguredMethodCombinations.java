package edu.cvut.fit.wudipetr.particledetection.imagej;

import edu.cvut.fit.wudipetr.particledetection.imagej.method_config.*;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.HMaximaDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.RegionGrowingDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ThresholdDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.GaussBlur;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.PrewittSubtract;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.TopHat;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class ConfiguredMethodCombinations {

    public static List<PreconfiguredMethod> get() {
        return Arrays.asList(
                generateHmaxima(),
                generateBlurThreshold(),
                generateTopHatThreshold(),
                generateRegionGrowing()
        );
    }

    private static final PreconfiguredMethod generateHmaxima() {
        List<MethodConfiguration<Preprocessing>> preprocessing = Collections.emptyList();
        Function<MethodParamCombination, ProteinDetector> methodSupplier = (params) -> new HMaximaDetector((int) params.get(0), (int) params.get(1), 0);
        MethodInfo<ProteinDetector> methodInfo = new MethodInfo<>("Local maxima + H-maximum", "hmaxima-srrf", methodSupplier);
        MethodParamInfo hParam = new MethodParamInfo<>("h", MethodParamType.INT, 75);
        MethodParamInfo winSizeParam = new MethodParamInfo<>("winSize", MethodParamType.INT, 4);
        MethodConfiguration<ProteinDetector> programConfig = new MethodConfiguration<>(methodInfo);
        programConfig.setParam(0, 75);
        programConfig.setParam(1, 4);
        List<MethodConfiguration<Validator>> postprocess = Collections.emptyList();
        return new PreconfiguredMethod("Local maximums (for usage with SRRFd image)", "hmaxima_srrf", preprocessing, programConfig, postprocess);
    }

    private static final PreconfiguredMethod generateBlurThreshold() {
        Function<MethodParamCombination, Preprocessing> gaussMethodSupplier = (params) -> new GaussBlur((int) params.get(0), (double) params.get(1));
        MethodInfo<Preprocessing> gaussMethodInfo = new MethodInfo<>("Gaussian blur", "gauss", gaussMethodSupplier);
        MethodParamInfo kernelSizeParam = new MethodParamInfo<>("kernel diameter", MethodParamType.INT, 13);
        MethodParamInfo varianceParam = new MethodParamInfo<>("variance", MethodParamType.REAL, 0.7);
        MethodConfiguration<Preprocessing> gaussProgramConfig = new MethodConfiguration<>(gaussMethodInfo);
        gaussProgramConfig.setParam(0, 13);
        gaussProgramConfig.setParam(1, 0.7);
        List<MethodConfiguration<Preprocessing>> preprocessing = Collections.singletonList(gaussProgramConfig);

        Function<MethodParamCombination, ProteinDetector> methodSupplier = (params) -> new ThresholdDetector((int) params.get(0));
        MethodInfo<ProteinDetector> methodInfo = new MethodInfo<>("Threshold", "threshold", methodSupplier);
        MethodParamInfo thresholdParam = new MethodParamInfo<>("threshold", MethodParamType.INT, 75);
        MethodConfiguration<ProteinDetector> programConfig = new MethodConfiguration<>(methodInfo);
        programConfig.setParam(0, 10);
        List<MethodConfiguration<Validator>> postprocess = Collections.emptyList();
        return new PreconfiguredMethod("Threshold (for usage with SRRFd image)", "threshold_srrf", preprocessing, programConfig, postprocess);
    }

    private static final PreconfiguredMethod generateTopHatThreshold() {
        Function<MethodParamCombination, Preprocessing> tophatMethodSupplier = (params) -> new TopHat((int) params.get(0));
        MethodInfo<Preprocessing> gaussMethodInfo = new MethodInfo<>("Top hat", "tophat", tophatMethodSupplier);
        MethodParamInfo kernelSizeParam = new MethodParamInfo<>("kernel diameter", MethodParamType.INT, 5);
        MethodConfiguration<Preprocessing> gaussProgramConfig = new MethodConfiguration<>(gaussMethodInfo);
        gaussProgramConfig.setParam(0, 5);
        List<MethodConfiguration<Preprocessing>> preprocessing = Collections.singletonList(gaussProgramConfig);

        Function<MethodParamCombination, ProteinDetector> methodSupplier = (params) -> new ThresholdDetector((int) params.get(0));
        MethodInfo<ProteinDetector> methodInfo = new MethodInfo<>("Threshold", "threshold", methodSupplier);
        MethodParamInfo thresholdParam = new MethodParamInfo<>("threshold", MethodParamType.INT, 75);
        MethodConfiguration<ProteinDetector> programConfig = new MethodConfiguration<>(methodInfo);
        programConfig.setParam(0, 40);
        List<MethodConfiguration<Validator>> postprocess = Collections.emptyList();
        return new PreconfiguredMethod("Threshold (for usage with plain image)", "threshold_tophat", preprocessing, programConfig, postprocess);
    }

    private static final PreconfiguredMethod generateRegionGrowing() {
        Function<MethodParamCombination, Preprocessing> tophatMethodSupplier = (params) -> new PrewittSubtract((int) params.get(0));
        MethodInfo<Preprocessing> gaussMethodInfo = new MethodInfo<>("Subtraction prewitt", "prewittsubtract", tophatMethodSupplier);
        MethodParamInfo kernelSizeParam = new MethodParamInfo<>("kernel diameter", MethodParamType.INT, 3);
        MethodConfiguration<Preprocessing> gaussProgramConfig = new MethodConfiguration<>(gaussMethodInfo);
        gaussProgramConfig.setParam(0, 3);
        List<MethodConfiguration<Preprocessing>> preprocessing = Collections.singletonList(gaussProgramConfig);

        Function<MethodParamCombination, ProteinDetector> methodSupplier = (params) -> new RegionGrowingDetector((int) params.get(0), (int) params.get(1), (int) params.get(2));
        MethodInfo<ProteinDetector> methodInfo = new MethodInfo<>("Region growing", "regiongrowing", methodSupplier);
        MethodParamInfo thresholdParam = new MethodParamInfo<>("region height", MethodParamType.INT, 40);
        MethodParamInfo minThresholdParam = new MethodParamInfo<>("minimum required value of region peak", MethodParamType.INT, 80);
        MethodParamInfo diameterParam = new MethodParamInfo<>("maximum radius", MethodParamType.INT, 80);
        MethodConfiguration<ProteinDetector> programConfig = new MethodConfiguration<>(methodInfo);
        programConfig.setParam(0, 40);
        programConfig.setParam(1, 80);
        programConfig.setParam(2, 20);
        List<MethodConfiguration<Validator>> postprocess = Collections.emptyList();
        return new PreconfiguredMethod("Region growing (for usage with plain image)", "regiongrowing", preprocessing, programConfig, postprocess);
    }
}
