package edu.cvut.fit.wudipetr.particledetection.imagej.dialogs;

import ij.gui.GenericDialog;
import ij.gui.NonBlockingGenericDialog;
import org.scijava.log.LogService;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

/**
 * A window telling the user which phase of the detection algorithms is currently running.
 * It's something like a dialog, except for there is no way for the user to interact.
 */
public class MethodProcessingMonolog extends NonBlockingGenericDialog {
    private static final String WINDOW_TITLE = "Processing...";
    private static final int ANIMATION_TIME = 5000;
    private static final int IMAGE_WIDTH = 50;
    private static final int IMAGE_HEIGHT = 50;
    private static final int NUM_OF_ICONS = 4;

    private LogService log;
    private JLabel currentPhaseLabel;
    private Image[] spinnerImages;
    private int currentImageNum = 0;
    private final ImageLoaderCanvas canvas = new ImageLoaderCanvas();
    private final Timer timer;

    public MethodProcessingMonolog() {
        super(WINDOW_TITLE);
        loadImages();
        setSize(200, 200);
        JPanel rootPanel = new JPanel();
        rootPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(rootPanel);

        BoxLayout layout = new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS);
        rootPanel.setLayout(layout);
        rootPanel.add(Box.createVerticalStrut(40));
        JPanel canvasPanel = new JPanel();
        canvasPanel.setSize(IMAGE_WIDTH, IMAGE_HEIGHT);
        rootPanel.add(canvasPanel);
        canvas.setSize(IMAGE_WIDTH, IMAGE_HEIGHT);
        canvas.setMaximumSize(canvas.getSize());
        canvasPanel.add(canvas);
        currentPhaseLabel = new JLabel("");
        currentPhaseLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        rootPanel.add(currentPhaseLabel);
        rootPanel.add(Box.createVerticalStrut(30));

        timer = new Timer(ANIMATION_TIME, new CallbackListener(this));
    }

    private void loadImages() {
        spinnerImages = new Image[NUM_OF_ICONS];
        for (int i = 0; i < NUM_OF_ICONS; ++i) {
            String imagePath = String.format("/loader/load%02d.png", i);
            URL imageURL = MethodProcessingMonolog.class.getResource(imagePath);
            try {
                BufferedImage img = ImageIO.read(imageURL);
                spinnerImages[i] = img;
            } catch (IOException e) {
                log.error(e);
            }
        }
    }

    public void setPhase(String phase) {
        currentPhaseLabel.setText("Current phase: " + phase);
    }

    public void setVisible(boolean value) {
        if (!value) {
            timer.stop();
        }
        super.setVisible(value);
        if (value) {
            updateImage();
            timer.start();
        }
    }

    private synchronized void updateImage(){
        currentImageNum = (currentImageNum + 1) % NUM_OF_ICONS;
        canvas.setImageNum(currentImageNum);
        canvas.repaint();
    }

    private static class CallbackListener implements ActionListener {

        MethodProcessingMonolog mpm;

        CallbackListener(MethodProcessingMonolog mpm) {
            this.mpm = mpm;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            mpm.updateImage();
        }
    }

    private static class ImageLoaderCanvas extends Canvas {
        private LogService log;
        private final Image[] spinnerImages;
        private int currentImageNum;

        public ImageLoaderCanvas() {
            spinnerImages = new Image[NUM_OF_ICONS];
            for (int i = 0; i < NUM_OF_ICONS; ++i) {
                String imagePath = String.format("/loader/load%02d.png", i);
                URL imageURL = ImageLoaderCanvas.class.getResource(imagePath);
                try {
                    BufferedImage img = ImageIO.read(imageURL);
                    spinnerImages[i] = img;
                } catch (IOException e) {
                    log.error(e);
                }
            }
        }

        public void setImageNum(int newImageNum) {
            this.currentImageNum = newImageNum;
        }

        public void paint(Graphics g){
            g.drawImage(spinnerImages[currentImageNum], 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, this); // todo move getGraphics
        }


        public void update(Graphics g) {
            g.clearRect(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
            g.drawImage(spinnerImages[currentImageNum], 0, 0, this); // todo move getGraphics
        }

        public void setLog(final LogService log) {
            this.log = log;
        }
    }

    public void setLog(final LogService log) {
        this.log = log;
        canvas.setLog(log);
    }
}
