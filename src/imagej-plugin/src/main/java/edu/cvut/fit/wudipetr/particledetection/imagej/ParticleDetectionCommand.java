package edu.cvut.fit.wudipetr.particledetection.imagej;

import edu.cvut.fit.wudipetr.particledetection.imagej.dialogs.DetectionMethodDialog;
import edu.cvut.fit.wudipetr.particledetection.imagej.dialogs.LibraryFailMonolog;
import net.imglib2.type.numeric.RealType;

import org.opencv.core.Core;
import org.scijava.command.Command;
import org.scijava.log.LogService;
import org.scijava.plugin.Parameter;
import org.scijava.plugin.Plugin;
import org.scijava.thread.ThreadService;
import org.scijava.ui.UIService;

import javax.swing.*;

@Plugin(type = Command.class, menuPath = "Plugins>PW Particle detection")
public class ParticleDetectionCommand<T extends RealType<T>> implements Command {
    @Parameter
    private UIService uiService;

    @Parameter
    private ThreadService threadService;

    @Parameter
    private LogService log;

    private DetectionMethodDialog dialog = null;

    /**
     * Run the deconvolution process
     */
    @Override
    public void run() {
        SwingUtilities.invokeLater(() -> {
            try {
                System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            } catch (UnsatisfiedLinkError | NoClassDefFoundError e) {
                LibraryFailMonolog failMonolog = new LibraryFailMonolog();
                failMonolog.showDialog();
                return;
            }
            if (dialog == null) {
                dialog = new DetectionMethodDialog(log);
            }
            dialog.setLog(log);
            dialog.setThreadService(threadService);
            dialog.setUiService(uiService);
            dialog.setVisible(true);
        });
    }
}