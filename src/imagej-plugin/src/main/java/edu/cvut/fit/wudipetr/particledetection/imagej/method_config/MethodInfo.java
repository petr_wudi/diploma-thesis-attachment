package edu.cvut.fit.wudipetr.particledetection.imagej.method_config;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Information about a method used to create configuration buttons.
 * @param <T> type (phase) to which the method belongs
 */
public class MethodInfo<T> {
    private final String name;
    private final String identifier;
    private final List<MethodParamInfo<?>> parameterInfo = new ArrayList<>();
    private final Function<MethodParamCombination, T> supplierFunction;

    /**
     * Creates statical information about a method.
     * @param name name of the method displayed to the user
     * @param identifier identifier of the method used as ID in a method map
     * @param supplierFunction function converting method parameters to a method instance
     */
    public MethodInfo(String name, String identifier, Function<MethodParamCombination, T> supplierFunction) {
        this.name = name;
        this.identifier = identifier;
        this.supplierFunction = supplierFunction;
    }

    /**
     * Adds a parameter that the method requires.
     * @param parameter information about the parameter
     */
    public void addParam(MethodParamInfo<?> parameter) {
        parameterInfo.add(parameter);
    }
    // todo remove (use spring or Scijava Common to create method configs)

    /**
     * @return user-friendly name of the method
     */
    public String getName() {
        return name;
    }

    /**
     * @return unique identifier of the method
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @return all parameters of the method added by {@link #addParam(MethodParamInfo)}
     */
    public List<MethodParamInfo<?>> getParams() {
        return parameterInfo;
    }

    /**
     * Creates new runnable instance of the method using the specified parameters.
     * @param params parameters used to call the methods
     * @return new instance of the method
     */
    public T createMethodInstance(MethodParamCombination params) {
        return supplierFunction.apply(params);
    }
}
