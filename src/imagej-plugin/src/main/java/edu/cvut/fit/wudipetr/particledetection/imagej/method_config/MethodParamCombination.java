package edu.cvut.fit.wudipetr.particledetection.imagej.method_config;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Series of method parameter values.
 */
public class MethodParamCombination {
    private final Map<Integer, Object> map = new HashMap<>();
    private final Map<MethodParamInfo<?>, Object> map2 = new HashMap<>();

    /**
     * Sets parameter on the given index.
     * @param parameterIndex index of the parameter to be set
     * @param value new value of the parameter
     */
    public void set(int parameterIndex, Object value) {
        map.put(parameterIndex, value);
    }

    /**
     * Sets value of the given parameter.
     * @param param the parameter to be set
     * @param value new value of the parameter
     */
    public void set(MethodParamInfo<?> param, Object value) {
        map2.put(param, value);
    }

    /**
     * Returns value of the parameter.
     * @param param the requested parameter
     * @return parameter's value
     */
    public Object get(MethodParamInfo<?> param) {
        Object o = map2.get(param);
        if (o == null) {
            System.out.println("Can't find method param " + param);
        }
        return o;
    }

    /**
     * Returns value of the parameter on the given index.
     * @param parameterIndex index of the requested parameter
     * @return value of the requested parameter
     */
    public Object get(int parameterIndex) {
        Object o = map.get(parameterIndex);
        if (o == null) {
            System.out.println("Can't find method param number " + parameterIndex);
        }
        return o;
    }

    @Override
    public String toString() {
        return map.entrySet().stream()
                .map(entry -> entry.getKey() + "->" + entry.getValue())
                .collect(Collectors.joining(", "));
    }
}
