package edu.cvut.fit.wudipetr.particledetection.imagej.method_config;

/**
 * Type of a parameter.
 */
public enum MethodParamType {
    /** Whole number (integer) */
    INT,
    /** True of false (boolean) */
    BOOLEAN,
    /** Real number (double) */
    REAL
}
