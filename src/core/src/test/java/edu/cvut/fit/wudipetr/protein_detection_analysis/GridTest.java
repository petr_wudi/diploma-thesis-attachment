package edu.cvut.fit.wudipetr.protein_detection_analysis;

import edu.cvut.fit.wudipetr.protein_detection_analysis.cli.Starter;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.LocalMaxDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.HMaximaValidator;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class GridTest {

    @BeforeAll
    public static void loadLib() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    @Test
    public void testPure() {
        File dir = new File("../img/A100IMAGES/lattice/1/1t001.tif/");
        File outputDir = new File("../out_txt/1t001.tif/");
        outputDir.mkdirs();
        testDir(dir, outputDir, "pure", 20, 5);
    }

    @Test
    public void testSrrfd() {
        File dir = new File("../img/A100IMAGES/lattice/1_SRRFd/1t001.tif/");
        File outputDir = new File("../out_txt/1t001.tif_srrfd/");
        outputDir.mkdirs();
        testDir(dir, outputDir, "srrf", 2, 50);
    }

    private void testDir(File dir, File outputDir, String methodName, int scale, int hMaxParam) {
        /*
        for (File inputFile : dir.listFiles()) {
            // for (int hmaxima : new int[]{5, 20, 50}) {
            List<Preprocessing> preprocessingSteps = new ArrayList<>();
            //preprocessing.add(new GaussBlur(5, 10.0));
            ProteinDetector detector = new LocalMaxDetector();
            List<Validator> validators = new ArrayList<>();
            validators.add(new HMaximaValidator(hMaxParam));

            if (!inputFile.exists()) {
                System.err.println("Error: Input file does not exist: " + inputFile.getName()
                        + "\nCan't locate file: " + inputFile.getAbsolutePath());
                continue;
            }
            System.out.println("Processing " + inputFile.getName());

            System.out.println(inputFile.getAbsolutePath());
            Mat img = Imgcodecs.imread(inputFile.getPath());

            long start = System.currentTimeMillis();
            Image imgPreprocessed = new MatImage(img);
            if (!preprocessingSteps.isEmpty()) {
                for (Preprocessing preprocessing : preprocessingSteps) {
                    imgPreprocessed = preprocessing.apply(imgPreprocessed);
                }
            }
            Mat kernel = new Mat(3, 3, CvType.CV_8U);
            kernel.setTo(new Scalar(1));
//            Imgproc.morphologyEx(imgPreprocessed.opencv(), imgPreprocessed.opencv(), Imgproc.MORPH_TOPHAT, kernel);
            Imgproc.morphologyEx(imgPreprocessed.opencv(), imgPreprocessed.opencv(), Imgproc.MORPH_BLACKHAT, kernel);

            Collection<Protein> proteins = findParticles(imgPreprocessed, detector, validators);
            long end = System.currentTimeMillis();
            long time = (end - start);
            System.out.println("Found " + proteins.size() + " particles");
            System.out.println("Time: " + time + " ms (" + time / 1000 + " s)");

            File outputFile = new File(outputDir, inputFile.getName() + ".txt");
            logPoints(proteins, outputFile);
            File imgOutputDir = new File("../img/A100IMAGES_output");
            imgOutputDir.mkdirs();
            File imgOutputFile = new File(imgOutputDir, inputFile.getName() + "_" + methodName + ".png");
            Starter.logPoints(img, proteins, imgOutputFile, scale);
        }
        */
    }

    private static void logPoints(Collection<Protein> proteins, File outputFile) {
        System.out.println("Writing to " + outputFile.getAbsolutePath());
        try (FileOutputStream fos = new FileOutputStream(outputFile)) {
            String resString = proteins.stream()
                    .map(Protein::getPosition)
                    .map(p -> p.getX() + "," + p.getY())
                    .collect(Collectors.joining("\n"));
            fos.write(resString.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
