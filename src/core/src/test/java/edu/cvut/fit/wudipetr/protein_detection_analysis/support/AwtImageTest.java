package edu.cvut.fit.wudipetr.protein_detection_analysis.support;

import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ThresholdDetectorTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AwtImageTest {

    @Test
    public void testOpencv() throws URISyntaxException, IOException {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        URL inputFileUrl = ThresholdDetectorTest.class.getClassLoader().getResource("test.png"); // todo regular test
        Assertions.assertNotNull(inputFileUrl);
        Path imageFile = Paths.get(inputFileUrl.toURI());
        BufferedImage bufferedImage = ImageIO.read(imageFile.toFile());
        AwtImage awtImage = new AwtImage(bufferedImage);
        Mat opencvConverted = awtImage.opencv();
        Mat opencvOriginal = Imgcodecs.imread(imageFile.toFile().getPath());
        Imgcodecs.imwrite("./rgb.png", opencvConverted);
        Assertions.assertTrue(compareMats(opencvConverted, opencvOriginal));
    }

    private boolean compareMats(Mat a, Mat b) {
        Mat dst = new Mat();
        Core.compare(a, b, dst, Core.CMP_NE);
        if (dst.type() == CvType.CV_8UC3) {
            Imgproc.cvtColor(dst, dst, Imgproc.COLOR_BGR2GRAY);
        }
        return Core.countNonZero(dst) == 0;
    }
}
