package edu.cvut.fit.wudipetr.protein_detection_analysis.configuration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration.ArgumentConfiguration;

public class ArgumentConfigurationTest {
    @Test
    public void testBlank() {
        String[] args = {};
        ArgumentConfiguration configuration = new ArgumentConfiguration(args);
        Assertions.assertEquals(ArgumentConfiguration.Mode.RUN, configuration.getMode());
        Assertions.assertNotNull(configuration.getDetectionMethod());
        Assertions.assertNotNull(configuration.getDetectionParams());
        Assertions.assertTrue(configuration.getDetectionParams().isEmpty());
        Assertions.assertNotNull(configuration.getErrorMessages());
        Assertions.assertTrue(configuration.getErrorMessages().isEmpty());
        Assertions.assertTrue(configuration.getPreprocessingMethods().isEmpty());
        Assertions.assertTrue(configuration.getPreprocessingParams().isEmpty());
        Assertions.assertTrue(configuration.getValidationMethods().isEmpty());
        Assertions.assertTrue(configuration.getValidationParams().isEmpty());
    }

    @Test
    public void testConfiguration() {
        String[] args = {"-p", "GaussBlur", "key1", "val1", "key2", "val2", "-p", "GaussBlur2",
                "-d", "Threshold", "Key1", "Val1"};
        ArgumentConfiguration configuration = new ArgumentConfiguration(args);
        Assertions.assertEquals(ArgumentConfiguration.Mode.RUN, configuration.getMode());
        Assertions.assertEquals(2, configuration.getPreprocessingMethods().size());
        Assertions.assertEquals("GaussBlur", configuration.getPreprocessingMethods().get(0));
        Assertions.assertEquals("GaussBlur2", configuration.getPreprocessingMethods().get(1));
        Assertions.assertEquals(2, configuration.getPreprocessingParams().size());
        Assertions.assertEquals(2, configuration.getPreprocessingParams().get(0).size());
        Assertions.assertTrue(configuration.getPreprocessingParams().get(1).isEmpty());
        Assertions.assertEquals("Threshold", configuration.getDetectionMethod());
        Assertions.assertEquals(1, configuration.getDetectionParams().size());
        Assertions.assertEquals("Val1", configuration.getDetectionParams().get("Key1"));
        Assertions.assertTrue(configuration.getValidationMethods().isEmpty());
        Assertions.assertTrue(configuration.getValidationParams().isEmpty());

        String[] args2 = {"-v", "AreaValidator", "key1", "val1", "key2", "val2",
                "-v", "AreaValidator",
                "-v", "SizeValidator", "k1", "v1"};
        ArgumentConfiguration configuration2 = new ArgumentConfiguration(args2);
        Assertions.assertEquals(ArgumentConfiguration.Mode.RUN, configuration2.getMode());
        Assertions.assertNotNull(configuration2.getDetectionMethod());
        Assertions.assertEquals(3, configuration2.getValidationMethods().size());
        Assertions.assertEquals("AreaValidator", configuration2.getValidationMethods().get(0));
        Assertions.assertEquals("AreaValidator", configuration2.getValidationMethods().get(1));
        Assertions.assertEquals("SizeValidator", configuration2.getValidationMethods().get(2));
        Assertions.assertEquals(3, configuration2.getValidationParams().size());
        Assertions.assertEquals(2, configuration2.getValidationParams().get(0).size());
        Assertions.assertEquals("val2", configuration2.getValidationParams().get(0).get("key2"));
        Assertions.assertEquals(0, configuration2.getValidationParams().get(1).size());
        Assertions.assertEquals(1, configuration2.getValidationParams().get(2).size());
        Assertions.assertEquals("v1", configuration2.getValidationParams().get(2).get("k1"));
    }

    @Test
    public void testHelp() {
        String[] args = {"--help"};
        ArgumentConfiguration configuration = new ArgumentConfiguration(args);
        Assertions.assertEquals(ArgumentConfiguration.Mode.HELP, configuration.getMode());
        Assertions.assertTrue(configuration.getErrorMessages().isEmpty());
        String[] args2 = {"-d", "something", "-h"};
        ArgumentConfiguration configuration2 = new ArgumentConfiguration(args2);
        Assertions.assertEquals(ArgumentConfiguration.Mode.HELP, configuration2.getMode());
        Assertions.assertTrue(configuration2.getErrorMessages().isEmpty());
        String[] args3 = {"-d", "-h"};
        ArgumentConfiguration configuration3 = new ArgumentConfiguration(args3);
        Assertions.assertEquals(ArgumentConfiguration.Mode.RUN, configuration3.getMode());
        Assertions.assertTrue(configuration3.getErrorMessages().isEmpty());
        Assertions.assertEquals("-h", configuration3.getDetectionMethod());
    }

    @Test
    public void testInvalid() {
        String[] args = {"whatever"};
        ArgumentConfiguration configuration = new ArgumentConfiguration(args);
        Assertions.assertEquals(ArgumentConfiguration.Mode.BROKEN, configuration.getMode());
        Assertions.assertEquals(1, configuration.getErrorMessages().size());
        String[] args2 = {"-d Hmaxima parameterKeyWithoutValue"};
        ArgumentConfiguration configuration2 = new ArgumentConfiguration(args2);
        Assertions.assertEquals(ArgumentConfiguration.Mode.BROKEN, configuration2.getMode());
        Assertions.assertEquals(1, configuration2.getErrorMessages().size());
    }
}
