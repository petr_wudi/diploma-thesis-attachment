package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LocalMaxDetectorTest {

    @Test
    public void testDetect() throws URISyntaxException, IOException, ClassNotFoundException {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        URL inputFileUrl = LocalMaxDetectorTest.class.getClassLoader().getResource("test.tif");
        URL refFileUrl = LocalMaxDetectorTest.class.getClassLoader().getResource("local-max.ser");
        Assertions.assertNotNull(inputFileUrl);
        Assertions.assertNotNull(refFileUrl);
        Path inputFile = Paths.get(inputFileUrl.toURI());
        Path refFile = Paths.get(refFileUrl.toURI());
        Mat mat = Imgcodecs.imread(inputFile.toString());
        LocalMaxDetector detector = new LocalMaxDetector(100, 11);
        Collection<ImagePoint> centers = detector.detectCenters(new MatImage(mat));
        ObjectInputStream inputStream = new ObjectInputStream(Files.newInputStream(refFile));
        List<ImagePoint> refPoints = (List<ImagePoint>) inputStream.readObject();
        Set<ImagePoint> centerSet = new HashSet<>(centers);
        Set<ImagePoint> refPointSet = new HashSet<>(refPoints);
        Assertions.assertEquals(refPointSet, centerSet);

        Path outputFile = Paths.get("./output.txt");
        System.out.println(outputFile.toAbsolutePath());
        OutputStream outputStream = Files.newOutputStream(outputFile);
        ObjectOutputStream oos = new ObjectOutputStream(outputStream);
        oos.writeObject(centers);
        oos.close();
    }

    @Test
    public void testWinSizeFail() {
        Assertions.assertThrows(RuntimeException.class, () -> new LocalMaxDetector(50, -1));
        Assertions.assertThrows(RuntimeException.class, () -> new LocalMaxDetector(50, 0));
        Assertions.assertThrows(RuntimeException.class, () -> new LocalMaxDetector(50, 2));
    }
}
