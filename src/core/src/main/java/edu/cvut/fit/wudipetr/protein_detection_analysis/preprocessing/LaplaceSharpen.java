package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class LaplaceSharpen implements Preprocessing {
    private static final double MULTIPLIER_DEFAULT = 0.1;
    private static final int KERNEL_SIZE_DEFAULT = 5;

    private final int kernelSize;
    private final double multiplier;

    public LaplaceSharpen() {
        this(MULTIPLIER_DEFAULT);
    }

    public LaplaceSharpen(double multiplier) {
        this(multiplier, KERNEL_SIZE_DEFAULT);
    }

    public LaplaceSharpen(double multiplier, int kernelSize) {
        this.multiplier = multiplier;
        this.kernelSize = kernelSize;
    }

    @Override
    public Image apply(Image image) {
        Mat source = image.opencv();
        List<Mat> channels = new ArrayList<>();
        for (int channelId = 0; channelId < source.channels(); ++channelId) {
            Mat channel = new Mat();
            Core.extractChannel(source, channel, channelId);
            Mat channelProcessed = applyToChannel(channel);
            channels.add(channelProcessed);
        }
        Mat target = new Mat();
        Core.merge(channels, target);
        return new MatImage(target);
    }

    private Mat applyToChannel(Mat source) {
        Mat derivation = new Mat();
        Imgproc.Laplacian(source, derivation, CvType.CV_64F, kernelSize, 1, 0);
        Mat derivationMultiplied = new Mat();
        Core.multiply(derivation, new Scalar(multiplier), derivationMultiplied);
        Mat target = new Mat();
        source.convertTo(source, CvType.CV_64FC1);
        Core.subtract(source, derivationMultiplied, target);
        target.convertTo(target, CvType.CV_8UC1);
        return target;
    }

    @Override
    public String getIdentifier() {
        return "laplace";
    }

    @Override
    public String getFullName() {
        return getIdentifier() + "__" + multiplier + "_" + kernelSize;
    }
}
