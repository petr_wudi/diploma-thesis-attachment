package edu.cvut.fit.wudipetr.protein_detection_analysis.cli;

import edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration.ArgumentConfiguration;
import edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration.ConfigException;
import edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration.ConfigurationLoader;
import edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration.ProgramConfiguration;
import org.opencv.core.Core;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Starts the program. Loads arguments, builds configuration and runs it.
 */
public class Starter {
    private static final Logger LOGGER = Logger.getLogger(Starter.class.getName());

    /**
     * Starts the program. Loads arguments, builds configuration and runs it.
     * @param args program arguments
     */
    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        ArgumentConfiguration argumentConfiguration = new ArgumentConfiguration(args);
        if (argumentConfiguration.getMode() == ArgumentConfiguration.Mode.BROKEN) {
            for (String error : argumentConfiguration.getErrorMessages()) {
                System.err.println(error);
                printHelp();
            }
        } else if (argumentConfiguration.getMode() == ArgumentConfiguration.Mode.HELP) {
            printHelp();
        } else if (argumentConfiguration.getMode() == ArgumentConfiguration.Mode.RUN) {
            try {
                ProgramConfiguration programConfig = ConfigurationLoader.load(argumentConfiguration);
                Runner.run(programConfig);
            } catch (ConfigException e) {
                LOGGER.log(Level.SEVERE, "Can't run the program due to invalid configuration.\n" + e.getMessage());
            }
        }
    }

    /**
     * Prints usage of the program to System out.
     */
    private static void printHelp() {
        System.out.println("Usage:");
        System.out.println("-h      Prints help.");
        System.out.println("-d      Specifies detection method.");
        System.out.println("-p      Specifies preprocessing method. Multiple usage of this command means definition "
                + "of multiple preprocessing methods. This parameter is optional.");
        System.out.println("-v      Specifies validation method. Multiple usage of this command means definition "
                + "of multiple validators. This parameter is optional.");
        System.out.println();
        System.out.println("Example: -d detect_method arg_name1 arg_value1 -p pmethod prepr_arg_name prepr_argv");
        System.out.println("Example: -v hmaxima h 50 -d watershed threshold 150 -v size minArea 50");
    }
}
