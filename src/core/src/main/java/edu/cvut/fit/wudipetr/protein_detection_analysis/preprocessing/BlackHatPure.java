package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class BlackHatPure extends MorphologicalOperation {
    private static final double MULTIPLIER_DEFAULT = 1.0;

    public BlackHatPure(int kernelDiameter) {
        this(kernelDiameter, MULTIPLIER_DEFAULT);
    }

    public BlackHatPure(int kernelDiameter, double multiplier) {
        super(Imgproc.MORPH_TOPHAT, kernelDiameter, multiplier);
    }

    @Override
    public Image apply(Image image) {
        Mat dstMat = applyOperation(image.opencv(), Imgproc.MORPH_BLACKHAT);
        return new MatImage(dstMat);
    }

    @Override
    public String getIdentifier() {
        return "black hat";
    }
}
