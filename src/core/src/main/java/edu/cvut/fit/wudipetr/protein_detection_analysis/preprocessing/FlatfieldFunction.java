package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;

class FlatfieldFunction {
        // val = A + Bx + Cy + Dx^2 + Ey^2 + Fxy
    double indexConst;
    double indexX;
    double indexY;
    double indexX2;
    double indexY2;
    double indexXY;
    double sigma;

    public FlatfieldFunction(double indexConst, double indexX, double indexY, double indexX2, double indexY2, double indexXY) {
        this.indexConst = indexConst;
        this.indexX = indexX;
        this.indexY = indexY;
        this.indexX2 = indexX2;
        this.indexY2 = indexY2;
        this.indexXY = indexXY;
    }

    public void computeSigma(Image image) {
        double diffSum = 0;
        for (int y = 0; y < image.height(); ++y) {
            for (int x = 0; x < image.width(); ++x) {
                double value = image.get(x, y);
                double expectedValue = predict(x, y);
                double diff = value - expectedValue;
                diffSum += diff * diff;
            }
        }
        int n = image.width() * image.height();
        double variable = diffSum / n;
        sigma = Math.sqrt(variable);
        System.out.println("sigma = " + sigma);
    }

    public double getSigma() {
        return sigma;
    }

    public double predict(int x, int y) {
        return indexConst + indexX * x + indexX2 * x * x + indexY * y + indexY2 * y * y + indexXY * x * y;
    }

    @Override
    public String toString() {
        return "const=" + indexConst + ", x=" + indexX + ", x^2=" + indexX2 + ", y=" + indexY + ", x^2=" + indexY2
                + ", xy=" + indexXY;

    }
}
