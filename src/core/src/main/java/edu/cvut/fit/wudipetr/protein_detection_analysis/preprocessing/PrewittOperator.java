package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

public class PrewittOperator implements Preprocessing {
    protected final int kernelSize;

    public PrewittOperator(int kernelSize) {
        this.kernelSize = kernelSize;
        if (kernelSize <= 1 || kernelSize % 2 != 1) {
            throw new IllegalArgumentException("Kernel size must be an odd number higher or equal 3");
        }
    }

    @Override
    public Image apply(Image image) {
        Mat source = image.opencv();
        Mat derivation1 = apply(source);
        return new MatImage(derivation1);
    }

    protected Mat apply(Mat source) {
        Mat targetHorizontal = new Mat();
        Mat targetVertical = new Mat();
        Mat kernelHorizontal = generateKernelHorizontal();
        Mat kernelVertical = generateKernelVertical();
        Imgproc.filter2D(source, targetHorizontal, -1, kernelHorizontal);
        Imgproc.filter2D(source, targetVertical, -1, kernelVertical);
        Mat target = new Mat();
        Core.add(targetHorizontal, targetVertical, target);
        return target;
    }

    private Mat generateKernelVertical() {
        Mat kernelHorizontal = Mat.zeros(kernelSize, kernelSize, CvType.CV_8S);
        for (int row = 0; row < kernelSize; ++row) {
            kernelHorizontal.put(row, 0, 1);
            kernelHorizontal.put(row, kernelSize - 1, -1);
        }
        return kernelHorizontal;
    }

    private Mat generateKernelVertical2() {
        Mat kernelHorizontal = Mat.zeros(3, 3, CvType.CV_8S);
        for (int row = 0; row < 3; ++row) {
            kernelHorizontal.put(row, 0, 1);
            kernelHorizontal.put(row, 2, -1);
        }
        return kernelHorizontal;
    }

    private Mat generateKernelHorizontal() {
        Mat kernelHorizontal = Mat.zeros(kernelSize, kernelSize, CvType.CV_8S);
        for (int col = 0; col < kernelSize; ++col) {
            kernelHorizontal.put(0, col, 1);
            kernelHorizontal.put(kernelSize - 1, col, -1);
        }
        return kernelHorizontal;
    }

    private Mat generateKernelHorizontal2() {
        Mat kernelHorizontal = Mat.zeros(3, 3, CvType.CV_8S);
        for (int col = 0; col < 3; ++col) {
            kernelHorizontal.put(0, col, 1);
            kernelHorizontal.put(2, col, -1);
        }
        return kernelHorizontal;
    }

    @Override
    public String getIdentifier() {
        return "prewitt";
    }

    @Override
    public String getFullName() {
        return "prewitt__" + kernelSize;
    }
}
