package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class RegionGrowingDetector implements ProteinDetector {
    /**
     * Value of a pixel considered so low, it could be ignored
     */
    private static final int ZERO_VALUE = 5;
    private static final int MIN_THRESHOLD_DEFAUULT = 20;
    private static final int MAX_DISTANCE_DEFAULT = 20;

    private final int threshold;
    private final int minThreshold;
    private final int zeroValue;
    private final int maxDistanceSqr;

    public RegionGrowingDetector(int threshold) {
        this(threshold, MIN_THRESHOLD_DEFAUULT);
    }

    public RegionGrowingDetector(int threshold, int minThreshold) {
        this(threshold, minThreshold, MAX_DISTANCE_DEFAULT);
    }

    public RegionGrowingDetector(int threshold, int minThreshold, int maxDistance) {
        this.threshold = threshold;
        this.minThreshold = minThreshold;
        zeroValue = ZERO_VALUE;
        maxDistanceSqr = maxDistance * maxDistance;
    }

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        return detectMasks(image).stream().map(Protein::getPosition).collect(Collectors.toList());
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        List<Protein> proteins = new ArrayList<>();
        PrimitiveMask pmask = new PrimitiveMask(image.width(), image.height());
        int[][] labels = new int[image.height()][image.width()];
        int label = 0;
        ImagePart imagePart = new ImagePart(image);
        while (true) {
            label += 1;
            ImagePoint pt = imagePart.getMax();
            if (pt == null) {
                break;
            }
            double maxValue = image.get(pt);
            double minValue = Double.max(maxValue - threshold, zeroValue);
            if (maxValue < minThreshold) {
                break;
            }
            List<ImagePoint> added = spread(labels, label, pmask, pt.getX(), pt.getY(),
                    (x1, y1) -> image.get(x1, y1) >= minValue
                            && RegionGrowingDetector.getSqrDistance(pt, x1, y1) <= maxDistanceSqr);
//            List<ImagePoint> added = spread(labels, label, pmask, pt.getX(), pt.getY(), image, minValue);
            imagePart.erase(added);
            ProteinMask mask = new ArrayProteinMask(pmask);
            ImagePoint center = pmask.getCenter();
            Protein p = new Protein(center, mask);
            pmask.clear();
            proteins.add(p);
        }
        return proteins;
    }

    private static int getSqrDistance(ImagePoint pt, Integer x1, Integer y1) {
        int distX = pt.getX() - x1;
        int distY = pt.getY() - y1;
        int sqrDist = distX * distX + distY * distY;
        return  sqrDist;
    }

    public static List<ImagePoint> spread(int[][] labels, int label, PrimitiveMask isIn, int x, int y, BiFunction<Integer, Integer, Boolean> fct) {
        List<ImagePoint> todo = new LinkedList<>();
        List<ImagePoint> added = new LinkedList<>();
        addPoint(todo, x, y, labels, label, isIn);
        while (!todo.isEmpty()) {
            ImagePoint pt = todo.remove(0);
            added.add(pt);
            int xx = pt.getX();
            int yy = pt.getY();
            tryAddPoint(xx-1, yy, labels, label, isIn, fct, todo);
            tryAddPoint(xx+1, yy, labels, label, isIn, fct, todo);
            tryAddPoint(xx, yy-1, labels, label, isIn, fct, todo);
            tryAddPoint(xx, yy+1, labels, label, isIn, fct, todo);
        }
        return added;
    }


    public static List<ImagePoint> spread(int[][] labels, int label, PrimitiveMask isIn, int x, int y, Image image, double minValue) {
        List<ImagePoint> todo = new LinkedList<>();
        List<ImagePoint> added = new LinkedList<>();
        addPoint(todo, x, y, labels, label, isIn);
        while (!todo.isEmpty()) {
            ImagePoint pt = todo.remove(0);
            added.add(pt);
            int xx = pt.getX();
            int yy = pt.getY();
            tryAddPoint(xx-1, yy, labels, label, isIn, image, minValue, todo);
            tryAddPoint(xx+1, yy, labels, label, isIn, image, minValue, todo);
            tryAddPoint(xx, yy-1, labels, label, isIn, image, minValue, todo);
            tryAddPoint(xx, yy+1, labels, label, isIn, image, minValue, todo);
        }
        return added;
    }

    // todo remove
    private static void addPoint(List<ImagePoint> todo, int x, int y, int[][] labels, int label, PrimitiveMask isIn) {
        labels[y][x] = label;
        isIn.set(x, y, true);
        todo.add(new ImagePoint(x, y));
    }

    // todo remove
    private static void tryAddPoint(int x, int y, int[][] labels, int label, PrimitiveMask isIn, BiFunction<Integer, Integer, Boolean> fct, List<ImagePoint> todo) {
        if (checkPoint(x, y, labels, fct)) {
            addPoint(todo, x, y, labels, label, isIn);
        }
    }

    // todo remove
    private static void tryAddPoint(int x, int y, int[][] labels, int label, PrimitiveMask isIn, Image image, double minValue, List<ImagePoint> todo) {
        if (checkPoint(x, y, labels, image, minValue)) {
            addPoint(todo, x, y, labels, label, isIn);
        }
    }

    // todo remove
    private static boolean checkPoint(int x, int y, int[][] labels, BiFunction<Integer, Integer, Boolean> fct) {
        return x >= 0 && y >= 0 && y < labels.length && x < labels[0].length && labels[y][x] == 0 && fct.apply(x, y);
    }

    // todo remove
    private static boolean checkPoint(int x, int y, int[][] labels, Image image, double minValue) {
        return x >= 0 && y >= 0 && y < labels.length && x < labels[0].length && labels[y][x] == 0
                && image.get(x, y) >= minValue;
    }

    private static class ImagePart {
        private static final int NUMBER_OF_SPLITS = 6;

        private final ImagePoint start;
        private final ImagePoint end; // exclude
        private List<ImagePart> subParts = Collections.emptyList();
        private boolean maximumIsUpToDate = false;
        private ImagePoint lastMaximum = null;
        private final Image image;
        private final boolean[][] taken;


        ImagePart(Image image) {
            start = new ImagePoint(0, 0);
            end = new ImagePoint(image.width(), image.height());
            if (NUMBER_OF_SPLITS > 0) {
                this.image = image.copy(); // todo to split argument
                taken = null;
                split(NUMBER_OF_SPLITS);
            } else {
                this.image = image.crop(start, end);
                taken = new boolean[getHeight()][getWidth()];
            }
        }

        private ImagePart(Image image, int startX, int startY, int endX, int endY) {
            start = new ImagePoint(startX, startY);
            end = new ImagePoint(endX, endY);
            this.image = image.crop(start, end);
            taken = new boolean[getHeight()][getWidth()];
        }

        private List<ImagePart> split(int numberOfSplits) {
            subParts = split();
            int size = getWidth() * getHeight();
            if (numberOfSplits > 0 && size > 1024) {
                subParts.forEach(part -> split(numberOfSplits - 1));
            }
            return subParts;
        }

        List<ImagePart> split() {
            List<ImagePart> parts = new ArrayList<>(4);
            int sizeX = getWidth();
            int stepX = sizeX / 2;
            int sizeY = getHeight();
            int stepY = sizeY / 2;
            int[] xStarts = {start.getX(), start.getX() + stepX};
            int[] xEnds = {start.getX() + stepX, end.getX()};
            int[] yStarts = {start.getY(), start.getY() + stepY};
            int[] yEnds = {start.getY() + stepY, end.getY()};
            for (int y = 0; y < 2; ++y) {
                for (int x = 0; x < 2; ++x) {
                    int xStart = xStarts[x];
                    int yStart = yStarts[y];
                    int xEnd = xEnds[x];
                    int yEnd = yEnds[y];
                    ImagePart imagePart = new ImagePart(image, xStart, yStart, xEnd, yEnd);
                    parts.add(imagePart);
                }
            }
            return parts;
        }

        private int getWidth() {
            return end.getX() - start.getX();
        }

        private int getHeight() {
            return end.getY() - start.getY();
        }

        public ImagePoint getMax() {
            if (!maximumIsUpToDate) {
                if (!subParts.isEmpty()) {
                    lastMaximum = maximumFromSubParts();
                } else {
                    lastMaximum = maximumFromPixels();
                }
                maximumIsUpToDate = true;
            }
            return lastMaximum;
        }

        private ImagePoint maximumFromSubParts() {
            double globalValue = Double.MIN_VALUE;
            ImagePoint globalMaximum = null;
            for (ImagePart part : subParts) {
                ImagePoint localMaximum = part.getMax();
                if (localMaximum == null) {
                    continue;
                }
                int x = localMaximum.getX() - start.getX();
                int y = localMaximum.getY() - start.getY();
                double localValue = image.get(x, y);
                if (localValue > globalValue) {
                    globalMaximum = localMaximum;
                    globalValue = localValue;
                }
            }
            return globalMaximum;
        }

        private ImagePoint maximumFromPixels() {
            double globalValue = Double.MIN_VALUE;
            ImagePoint globalMaximum = null;
            for (int y = 0; y < image.height(); ++y) {
                for (int x = 0; x < image.width(); ++x) {
                    if (!taken[y][x] && image.get(x, y) > globalValue) {
                        globalMaximum = new ImagePoint(x + start.getX(), y + start.getY());
                        globalValue = image.get(x, y);
                    }
                }
            }
            return globalMaximum;
        }

        private boolean isIn(ImagePoint point) {
            int x = point.getX();
            int y = point.getY();
            return x >= start.getX() && x < end.getX() && y >= start.getY() && y < end.getY();
        }

        void erase(List<ImagePoint> toErase) {
            if (subParts.isEmpty()) {
                for (ImagePoint point : toErase) {
                    if (isIn(point)) {
                        int x = point.getX() - start.getX();
                        int y = point.getY() - start.getY();
                        image.set(x, y, Double.MIN_VALUE);
                        taken[y][x] = true;
                        if (point.getX() == lastMaximum.getX() && point.getY() == lastMaximum.getY()) {
                            maximumIsUpToDate = false;
                        }
                    }
                }
            } else {
                for (ImagePart part : subParts) {
                    part.erase(toErase);
                    maximumIsUpToDate = maximumIsUpToDate && part.maximumIsUpToDate;
                }
            }
        }
    }
}
