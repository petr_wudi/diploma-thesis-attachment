package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.opencv.core.Mat;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

/**
 * Thresholding detector - common code for Otsu and Thresholding.
 */
public abstract class AbstractThresholdDetector implements ProteinDetector {

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        return detectMasks(image).stream().map(Protein::getPosition).collect(Collectors.toList());
    }

    /**
     * Segments the image into groups of adjacent non-zero pixels.
     * @param image the image to be segment
     * @return list of segmented proteins in the input image
     */
    protected List<Protein> segmentate(Mat image) {
        boolean[][] isNotZeroArr = isNotZero(image);
        int height = image.height();
        int width = image.width();
        int[][] labels = new int[height][width];
        int maxLabel = 1;
        List<Protein> proteins = new ArrayList<>();
        PrimitiveMask isIn = new PrimitiveMask(width, height);
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (labels[y][x] == 0 && isNotZeroArr[y][x]) {
                    DetectionSupport.spread(labels, maxLabel++, isIn, x, y, isNotZeroArr);
                    ProteinMask mask = new ArrayProteinMask(isIn);
                    ImagePoint center = isIn.getCenter();
                    Protein p = new Protein(center, mask);
                    proteins.add(p);
                    isIn.clear();
                }
            }
        }
        return proteins;
    }

    private boolean[][] isNotZero(Mat image) {
        boolean[][] isZeroArr = new boolean[image.height()][image.width()];
        for (int y = 0; y < image.height(); ++y) {
            for (int x = 0; x < image.width(); ++x) {
                double[] values = image.get(y, x);
                isZeroArr[y][x] = values[0] > 0 && values[1] > 0 && values[2] > 0; // todo maybe ignore red
            }
        }
        return isZeroArr;
    }
}
