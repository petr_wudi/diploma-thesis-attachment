package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

import java.util.*;
import java.util.stream.Collectors;

public class HMaximaDetector implements ProteinDetector {
    /**
     * People which square distance is closer than this value will be considered
     * as one person.
     */
    protected int duplicityThreshold = 50;

    /**
     * Describes radius of neighborhood. Area with this radius will be scanned
     * during looking for local maxima.
     */
    protected int maximaRadius = 5;

    /**
     * Local maxima must be higher than everything in its neighborhood higher
     * than or equal to local maxima's value minus this number
     */
    protected int h = 10;


    protected int width;
    protected int height;
    protected int[][] arr;

    /**
     * Creates the detector with default values
     */
    public HMaximaDetector() {
    }

    /**
     * @param h Local maxima must be higher than everything in its neighborhood
     * higher than or equal to local maxima's value minus this number
     * @param winRadius Describes radius of neighborhood. Area with this radius
     * will be scanned during looking for local maxima.
     * @param duplicityThreshold people closer to each other than this will be ignored
     */
    public HMaximaDetector(int h, int winRadius, int duplicityThreshold) {
        this.h = h;
        this.maximaRadius = winRadius;
        this.duplicityThreshold = duplicityThreshold;
    }

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        List<ImagePoint> centers = detectMasks(image).stream().map(Protein::getPosition).collect(Collectors.toList());
        return centers;
        //return DuplicateElliminator.elliminateDuplicities(centers, duplicityThreshold);
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        //this.arr = image.getArray();
        this.height = image.height();
        this.width = image.width();
        arr = image.getArray();
        List<ImagePoint> localMaximums = getLocalMaximums();
        return hmaximaTransform(localMaximums);
        // todo implement duplicity ellimination
    }

    /**
     * Returns all input image points that are higher than its neighborhood
     * @return List of maximums
     */
    protected List<ImagePoint> getLocalMaximums() {
        List<ImagePoint> maximums = new ArrayList<>();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (arr[y][x] > h && isMaximum(x, y)) {
                    maximums.add(new ImagePoint(x, y));
                }
            }
        }
        return maximums;
    }

    /**
     * Returns whether specified point is local maximum in square neighborhood
     * with diameter of {@link #maximaRadius}
     * @param x X coord of the point
     * @param y Y coord of the point
     * @return true if point is local maximum, false if not
     */
    protected boolean isMaximum(int x, int y) {
        int center = arr[y][x];
        int yMin = Integer.max(y - maximaRadius, 0),
                xMin = Integer.max(x - maximaRadius, 0),
                yMax = Integer.min(y + maximaRadius, height - 1),
                xMax = Integer.min(x + maximaRadius, width - 1);
        for(int yWin = yMin; yWin <= yMax; yWin ++){
            for(int xWin = xMin; xWin <= xMax; xWin ++){
                if(xWin == x && yWin == y) continue;
                int pixel = arr[yWin][xWin];
                if(pixel > center){
                    return false;
                }
            }
        }
        return true;

    }

    /**
     * Eliminates some pixel that are not enough higher over its neighborhood
     * @param localMaxima list of all potential maximums
     * @return list of maxima that fulfill the condition
     */
    private List<Protein> hmaximaTransform(List<ImagePoint> localMaxima) {
        List<Protein> hMaximums = new ArrayList<>();
        boolean[][] visited = new boolean[height][width];
        PrimitiveMask newVisited = new PrimitiveMask(width, height);
        for (ImagePoint point : localMaxima) {
            int x = point.getX();
            int y = point.getY();
            if (visited[y][x]) continue;
            if (isHmaxima(x, y, arr[y][x] - h, visited, newVisited)) {
                boolean isCoveringAnother = false;
                for (Protein currentMax : hMaximums) {
                    if (newVisited.get(currentMax.getPosition().getX(), currentMax.getPosition().getY())) {
                        isCoveringAnother = true;
                    }
                }
                if (!isCoveringAnother) {
                    ProteinMask mask = new ArrayProteinMask(newVisited);
                    Protein protein = new Protein(point, mask);
                    hMaximums.add(protein);
                }
            }
            newVisited.clear();
        }
        return hMaximums;
    }

    /**
     * Return whether all values in specified point's neighborhood that are
     * higher that lowerBound are also lower or equal than specified point
     * @param refX X coord of the point
     * @param refY Y coord of the point
     * @param lowerBound Algorithm will ignore values lower than this
     * @param visited Array of points that are checked and could be ignored
     * @return Whether point is h-maxima or not
     */
    private boolean isHmaxima(int refX, int refY, int lowerBound, boolean[][] visited, PrimitiveMask newVisited) {
        List<ImagePoint> toProcess = new LinkedList<>();
        List<Boolean> wereAlreadyVisited = new LinkedList<>();
        toProcess.add(new ImagePoint(refX, refY));
        wereAlreadyVisited.add(false);
        newVisited.set(refX, refY, true);
        visited[refY][refX] = true;
        while (!toProcess.isEmpty()) {
            ImagePoint point = toProcess.remove(0);
            boolean pointWereVisited = wereAlreadyVisited.remove(0);
            int x = point.getX();
            int y = point.getY();
            if(arr[y][x] > arr[refY][refX]) {
                return false;
            }
            if(arr[y][x] == arr[refY][refX] && pointWereVisited) {
                return false;
            }
            newVisited.set(x, y, true);
            tryAddToHmaxima(toProcess, wereAlreadyVisited, x-1, y, lowerBound, visited, newVisited);
            tryAddToHmaxima(toProcess, wereAlreadyVisited, x+1, y, lowerBound, visited, newVisited);
            tryAddToHmaxima(toProcess, wereAlreadyVisited, x, y-1, lowerBound, visited, newVisited);
            tryAddToHmaxima(toProcess, wereAlreadyVisited, x, y+1, lowerBound, visited, newVisited);
        }
        return true;
    }

    /**
     * Tries to call {@link #isHmaxima}
     * @param x X coord of point that will be used as argument
     * @param y Y coord of point that will be used as argument
     * @param lowerBound Point with value lower than this will be ignored
     * @param visited Array of points that are checked and could be ignored
     * @return Result of calling the function
     */
    private void tryAddToHmaxima(List<ImagePoint> queue, List<Boolean> wereAlreadyVisited,
                                 int x, int y, int lowerBound, boolean[][] visited, PrimitiveMask newVisited) {
        if(!isInsideImage(x, y) || newVisited.get(x, y) || arr[y][x] < lowerBound) return;
        queue.add(new ImagePoint(x, y));
        wereAlreadyVisited.add(visited[y][x]);
        newVisited.set(x, y, true);
        visited[y][x] = true;
    }

    private boolean isInsideImage(int x, int y) {
        return x >= 0 && y >= 0 && x < width && y < height;
    }
}