package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;

import java.util.*;

/**
 * Class for elimination of duplicity points.
 */
public class DuplicateEliminator {


    /**
     * Deletes points which are too close to each other
     * @param points list of the points to be filtered
     * @param duplicityThreshold minimum allowed distance between points
     * @return list of isolated points
     */
    public static Collection<ImagePoint> eliminateDuplicities(Collection<ImagePoint> points, int duplicityThreshold) {
        Map<ImagePoint, List<ImagePoint>> grid = new HashMap<>();
        List<ImagePoint> pointsToPerserve = new ArrayList<>();
        for (ImagePoint point : points) {
            ImagePoint gridPosition = getGridPosition(point, duplicityThreshold);
            List<ImagePoint> pointsInGrid = getList(grid, gridPosition);
            if (checkPoint(point, grid, gridPosition, duplicityThreshold)) {
                pointsToPerserve.add(point);
            }
            pointsInGrid.add(point);
        }
        return pointsToPerserve;
    }

    private static boolean checkPoint(ImagePoint point, Map<ImagePoint, List<ImagePoint>> grid, ImagePoint gridPosition, int duplicityThreshold) {
        int gridX = gridPosition.getX();
        int gridY = gridPosition.getY();
        for (int x = Integer.max(0, gridX - 1); x <= gridX + 1; ++x) {
            for (int y = Integer.max(0, gridY - 1); y <= gridY + 1; ++y) {
                ImagePoint neighbourhoodPosition = new ImagePoint(x, y);
                List<ImagePoint> neighbourhoodGrid = getList(grid, neighbourhoodPosition);
                for (ImagePoint pt : neighbourhoodGrid) {
                    int xDist = point.getX() - pt.getX();
                    int yDist = point.getY() - pt.getY();
                    int dist = xDist * xDist + yDist * yDist;
                    if (dist < duplicityThreshold) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private static List<ImagePoint> getList(Map<ImagePoint, List<ImagePoint>> grid, ImagePoint gridPosition) {
        return grid.computeIfAbsent(gridPosition, k -> new ArrayList<>());
    }

    private static ImagePoint getGridPosition(ImagePoint point, int radius) {
        return new ImagePoint(point.getX() / radius, point.getY() / radius);
    }
}
