package edu.cvut.fit.wudipetr.protein_detection_analysis.cli;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration.ProgramConfiguration;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.statistics.DistanceToNthClosest;
import edu.cvut.fit.wudipetr.protein_detection_analysis.statistics.DistanceToNthClosestRatio;
import edu.cvut.fit.wudipetr.protein_detection_analysis.statistics.NumOfParticlesInDistance;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;

public class Task {
    private static final int LOG_POINTS_SCALE = 2;
    private static final int LOG_DENSITY_SCALE = 5;
    private List<File> inputFiles;
    private File outputDir;
    private ProgramConfiguration programConfiguration;
    private long runTime = 0;

    public void run() {
        if (checkCanRun()) {
            for (File file : inputFiles) {
                run(file, programConfiguration);
            }
        }
    }

    private boolean checkCanRun() {
        boolean isBroken = false;
        if (inputFiles == null || inputFiles.isEmpty()) {
            System.out.println("There is no input file to be processed.");
            isBroken = true;
        }
        if (outputDir == null) {
            System.err.println("Output dir is not specified.");
            isBroken = true;
        }
        if (outputDir != null && !outputDir.exists()) {
            System.err.println("Output dir " + outputDir + " does not exist.");
            isBroken = true;
        }
        if (programConfiguration == null) {
            System.err.println("Program configuration was not specified.");
            isBroken = true;
        }
        if (programConfiguration != null && programConfiguration.getDetector() == null) {
            System.err.println("Detector algorithm was not specified.");
            isBroken = true;
        }
        if (programConfiguration != null && programConfiguration.getMethodName() == null) {
            System.err.println("Method name is not specified.");
            isBroken = true;
        }
        for (File file : inputFiles) {
            if (!file.exists()) {
                System.err.println("Input file " + file + " does not exist.");
                isBroken = true;
            }
        }
        return !isBroken;
    }

    private void run(File inputFile, ProgramConfiguration configuration) {
        Mat image = Imgcodecs.imread(inputFile.getPath());
        File fileOutputDir = new File(outputDir, inputFile.getName());
        if (!fileOutputDir.exists() && !fileOutputDir.mkdirs()) {
            System.err.println("Can't create output directory " + fileOutputDir);
        }

        long start = System.currentTimeMillis();
        Image imgPreprocessed = new MatImage(image);
        List<Preprocessing> preprocessingSteps = programConfiguration.getPreprocessing();
        System.out.println("PREPROCESSING " + preprocessingSteps);
        if (!preprocessingSteps.isEmpty()) {
            for (Preprocessing preprocessing : preprocessingSteps) {
                long timeStart = System.currentTimeMillis();
                imgPreprocessed = preprocessing.apply(imgPreprocessed);
                long timeEnd = System.currentTimeMillis();
                System.out.println("Time of " + preprocessing.getIdentifier() + ": " + (timeEnd - timeStart));
            }
            logPreprocessing(imgPreprocessed, preprocessingSteps, fileOutputDir);
        }

        long timeStartDetector = System.currentTimeMillis();
        Collection<Protein> proteins = programConfiguration.getDetector().detectMasks(imgPreprocessed);
        long timeEndDetector = System.currentTimeMillis();
        System.out.println("Time of " + programConfiguration.getDetector().getClass().getName() + ": "
                + (timeEndDetector - timeStartDetector));
        for (Validator validator : programConfiguration.getValidators()) {
            long timeStart = System.currentTimeMillis();
            proteins = validator.filter(proteins, imgPreprocessed);
            long timeEnd = System.currentTimeMillis();
            System.out.println("Time of " + validator.getIdentifier() + ": " + (timeEnd - timeStart));
        }

        long end = System.currentTimeMillis();
        runTime += (end - start);

        String methodName = programConfiguration.getMethodName();
        System.out.println("Using method name: " + methodName);
        if (programConfiguration.shouldLogPoints()) {
            logPoints(image, proteins, methodName, fileOutputDir, LOG_POINTS_SCALE); // todo full method name, even preprocessing
        }
        if (programConfiguration.shouldLogAreas()) {
            logAreas(image, proteins, methodName, fileOutputDir);
        }
        if (programConfiguration.shouldLogAreasBlank()) {
            logAreasBlank(image, proteins, methodName, fileOutputDir);
        }
        // todo parameter for logging distance
        logDistances(proteins, methodName, fileOutputDir);
        logNumOfClose(proteins, image.width() * image.height(), methodName, fileOutputDir);

        // log runtime
        File timeLogDir = new File(fileOutputDir, "time");
        timeLogDir.mkdirs();
        File timeLogFile = new File(timeLogDir, methodName + ".txt");
        try {
            Files.write(timeLogFile.toPath(), Collections.singleton(Long.toString(runTime)));
        } catch (IOException e) {
            e.printStackTrace(); // todo log, move to some method
        }

        System.out.println("Runtime: " + runTime);
    }

    public static void logPoints(Mat img, Collection<Protein> proteins, String methodName, File fileOutputDir, int scale) {
        File pointOutputDir = new File(fileOutputDir, "positions");
        File densityOutputDir = new File(fileOutputDir, "density");
        pointOutputDir.mkdirs();
        densityOutputDir.mkdirs();
        File outputFile = new File(pointOutputDir, methodName + ".tif");
        File densityOutputFile = new File(densityOutputDir, methodName + ".tif");
        File densityOutputFile2 = new File(densityOutputDir, methodName + "-circled.tif");
        logPoints(img, proteins, outputFile, scale);
        logPointsCsv(proteins, methodName, fileOutputDir);
        logDensity(img, proteins, densityOutputFile, densityOutputFile2, LOG_DENSITY_SCALE);
    }

    public static void logPoints(Mat img, Collection<Protein> proteins, File outputFile, int scale) {
        Mat imgBig = resize(img, scale);
        Mat imgClone = imgBig.clone();
        List<ImagePoint> proteinPositions = proteins.stream()
                .map(Protein::getPosition)
                .map(pos -> new ImagePoint(pos.getX() * scale + scale/2, pos.getY() * scale + scale/2))
                .collect(Collectors.toList());
        View.getInstance().drawPoints(imgClone, proteinPositions);
        Imgcodecs.imwrite(outputFile.getPath(), imgClone);
    }

    public static void logDensity(Mat img, Collection<Protein> proteins, File outputFile, File outputFile2, int scale) {
        Size size = new Size(img.size().width * scale, img.size().height * scale);
        List<ImagePoint> proteinPositions = proteins.stream()
                .map(Protein::getPosition)
                .map(pos -> new ImagePoint(pos.getX() * scale + scale/2, pos.getY() * scale + scale/2))
                .collect(Collectors.toList());
        Mat imgBig = resize(img, scale);
        Mat densityImg = View.getInstance().drawDensity(size, proteinPositions);
        Mat circledImg = View.getInstance().drawDensityCircle(imgBig, proteinPositions);
        Imgcodecs.imwrite(outputFile.getPath(), densityImg);
        Imgcodecs.imwrite(outputFile2.getPath(), circledImg);
    }

    private static Mat resize(Mat img, int scale) {
        Size newSize = new Size(img.size().width * scale, img.size().height * scale);
        Mat imgDst = new Mat((int) newSize.height, (int) newSize.width, img.type());
        Imgproc.resize(img, imgDst, new Size(0, 0), scale, scale, Imgproc.INTER_NEAREST);
        return imgDst;
    }

    private static void logPointsCsv(Collection<Protein> proteins, String methodName, File fileOutputDir) {
        System.out.println("Log csv");
        File csvOutputDir = new File(fileOutputDir, "csv");
        csvOutputDir.mkdirs();
        File outputFile = new File(csvOutputDir, methodName + ".csv");
        try (OutputStream outputStream = new FileOutputStream(outputFile)) {
            for (Protein protein : proteins) {
                ImagePoint position = protein.getPosition();
                String message = position.getX() + "," + position.getY() + "\n";
                outputStream.write(message.getBytes());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void logAreas(Mat img, Collection<Protein> proteins, String methodName, File fileOutputDir) {
        System.out.println("Log areas");
        File areaOutputDir = new File(fileOutputDir, "areas");
        areaOutputDir.mkdirs();
        Mat imgClone = img.clone();
        Imgproc.cvtColor(imgClone, imgClone, Imgproc.COLOR_BGR2GRAY);
        Imgproc.cvtColor(imgClone, imgClone, Imgproc.COLOR_GRAY2BGR);
        View.getInstance().drawFilling(imgClone, proteins.stream().map(Protein::getMask).collect(Collectors.toList()));
        File outputFile = new File(areaOutputDir, methodName + ".tif");
        Imgcodecs.imwrite(outputFile.getPath(), imgClone);
    }

    /**
     * Saves blank image of the areas (to be automatically processed
     * @param img
     * @param proteins
     * @param methodName
     * @param fileOutputDir
     */
    private static void logAreasBlank(Mat img, Collection<Protein> proteins, String methodName, File fileOutputDir) {
        File areaOutputDir = new File(fileOutputDir, "areas-blank");
        areaOutputDir.mkdirs();
        Mat imgBlank = new Mat(img.size(), CvType.CV_8UC3);
        imgBlank.setTo(new Scalar(0, 0, 0));
        View.getInstance().drawFilling(imgBlank, proteins.stream().map(Protein::getMask).collect(Collectors.toList()), true);
        File outputFile = new File(areaOutputDir, methodName + ".tif");
        Imgcodecs.imwrite(outputFile.getPath(), imgBlank);
    }

    /**
     * Saves preprocessing image
     * @param img result of preprocessing (will be saved)
     */
    private static void logPreprocessing(Image img, List<Preprocessing> steps, File fileOutputDir) {
        String methodName = steps.stream()
                .map(Preprocessing::getIdentifier)
                .collect(Collectors.joining("_"));
        File preprOutputDir = new File(fileOutputDir, "preprocessing");
        File methodOutputDir = new File(preprOutputDir, methodName);
        methodOutputDir.mkdirs();
        String fullMethodName = steps.stream()
                .map(Preprocessing::getFullName)
                .collect(Collectors.joining("___"));
        File preprocessingOutputFile = new File(methodOutputDir, fullMethodName + ".tif");
        System.out.println("LOG PREPROCESSING TO " + preprocessingOutputFile);
        Imgcodecs.imwrite(preprocessingOutputFile.getPath(), img.opencv());
    }

    private static void logDistances(Collection<Protein> proteins, String methodName, File fileOutputDir) {
        System.out.println("Log distances");
        File statisticOutputDir = new File(fileOutputDir, "distances");
        File statisticOutputDir2 = new File(fileOutputDir, "distances-ratios");
        statisticOutputDir.mkdirs();
        statisticOutputDir2.mkdirs();
        File outputFile = new File(statisticOutputDir, methodName + ".csv");
        File outputFile2 = new File(statisticOutputDir2, methodName + ".csv");

        DistanceToNthClosest distanceEvaluator = new DistanceToNthClosest(5);
        Map<Integer, Integer> distances = distanceEvaluator.get(proteins);
        try (OutputStream outputStream = new FileOutputStream(outputFile)) {
            for (Map.Entry<Integer, Integer> entry : distances.entrySet()) {
                String line = entry.getKey() + "," + entry.getValue() + "\n";
                outputStream.write(line.getBytes());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        DistanceToNthClosestRatio disanceRatioEvaluator = new DistanceToNthClosestRatio();
        Map<Integer, Integer> distancesRatios = disanceRatioEvaluator.get(proteins);
        try (OutputStream outputStream = new FileOutputStream(outputFile2)) {
            for (Map.Entry<Integer, Integer> entry : distancesRatios.entrySet()) {
                String line = entry.getKey() + "," + entry.getValue() + "\n";
                outputStream.write(line.getBytes());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void logNumOfClose(Collection<Protein> proteins, int imageSize, String methodName, File fileOutputDir) {
        System.out.println("Log number of close");
        File statisticOutputDir = new File(fileOutputDir, "num-of-close");
        statisticOutputDir.mkdirs();
        File outputFile = new File(statisticOutputDir, methodName + ".csv");
        NumOfParticlesInDistance closeNumEvaluator = new NumOfParticlesInDistance(10, imageSize);
        List<Integer> numsOfClosest = closeNumEvaluator.get(proteins);
        try (OutputStream outputStream = new FileOutputStream(outputFile)) {
            for (Integer numsOfParticles : numsOfClosest) {
                String line = numsOfParticles + "\n";
                outputStream.write(line.getBytes());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setInputDir(File inputDir) {
        File[] files = inputDir.listFiles();
        if (files == null) {
            System.err.println("Can't list files in directory: " + inputDir.getAbsolutePath());
            return;
        }
        inputFiles = Arrays.asList(files);
    }

    public void setInputFiles(List<File> inputFiles) {
        this.inputFiles = inputFiles;
    }

    public void setOutputDir(File outputDir) {
        this.outputDir = outputDir;
    }

    public void setProgramConfiguration(ProgramConfiguration programConfiguration) {
        this.programConfiguration = programConfiguration;
    }

    public long getTime() {
        return runTime;
    }
}
