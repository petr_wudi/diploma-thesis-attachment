package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;
import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class FlatfieldThresholdDetector implements ProteinDetector {

    private final double maxSigmaMultiplier;

    public FlatfieldThresholdDetector() {
        maxSigmaMultiplier = 3;
    }

    public FlatfieldThresholdDetector(double maxSigmaMultiplier) {
        this.maxSigmaMultiplier = maxSigmaMultiplier;
    }

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        return detectMasks(image).stream().map(Protein::getPosition).collect(Collectors.toList());
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
//        boolean[][] binaryMask1 = getBinaryMask(image);
//        Mat mat2 = image.opencv().clone();
//        for (int y = 0; y < image.height(); ++y) {
//            for (int x = 0; x < image.width(); ++x) {
//                if (binaryMask1[y][x]) {
//                    mat2.put(0, 0, 0, 0, 0);
//                }
//            }
//        }
//        Image image2 = new MatImage(mat2);
//        boolean[][] binaryMask = getBinaryMask(image2);
        boolean[][] binaryMask = getBinaryMask(image);

        int height = image.height();
        int width = image.width();
        int[][] labels = new int[height][width];
        int maxLabel = 1;
        List<Protein> proteins = new ArrayList<>();
        PrimitiveMask isIn = new PrimitiveMask(width, height);
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (labels[y][x] == 0 && binaryMask[y][x]) {
                    DetectionSupport.spread(labels, maxLabel++, isIn, x, y, binaryMask);
                    ProteinMask mask = new ArrayProteinMask(isIn);
                    ImagePoint center = isIn.getCenter();
                    Protein p = new Protein(center, mask);
                    proteins.add(p);
                    isIn.clear();
                }
            }
        }
        return proteins;
    }

    public boolean[][] getBinaryMask(Image image) {
        boolean[][] binaryMask = new boolean[image.height()][image.width()];
        FlatfieldFunction function = computeFlatfield(image);
        double sigma = function.getSigma();
        for (int y = 0; y < image.height(); ++y) {
            for (int x = 0; x < image.width(); ++x) {
                double value = image.get(x, y);
                double ffValue = function.predict(x, y);
                binaryMask[y][x] = value > ffValue + maxSigmaMultiplier * sigma;
            }
        }
        return binaryMask;
    }


    private FlatfieldFunction computeFlatfield(Image image) {
        double sumXY = 0;
        double sumX = 0;
        double sumY = 0;
        long sumX2 = 0;
        for (int y = 0; y < image.height(); ++y) {
            for (int x = 0; x < image.width(); ++x) {
                double value = image.get(x, y);
                int distSqr = x * x + y * y;
                double dist = Math.sqrt(distSqr);
                sumX += dist;
                sumX2 += distSqr;
                sumY += value;
                sumXY += dist * value;
            }
        }
        int n = image.width() * image.height();
        double b1 = (sumXY - sumX * sumY / n) / (sumX2 - sumX * sumX / n);
        double b0 = (sumY - b1 * sumX) / n;
        // todo indexX2, indexY2, indexXY
        FlatfieldFunction f = new FlatfieldFunction(b0, b1, b1, 0, 0, 0);
        f.computeSigma(image);
        return f;
    }

    private class FlatfieldFunction {
        // val = A + Bx + Cy + Dx^2 + Ey^2 + Fxy
        double indexConst;
        double indexX;
        double indexY;
        double indexX2;
        double indexY2;
        double indexXY;
        double sigma;

        public FlatfieldFunction(double indexConst, double indexX, double indexY, double indexX2, double indexY2, double indexXY) {
            this.indexConst = indexConst;
            this.indexX = indexX;
            this.indexY = indexY;
            this.indexX2 = indexX2;
            this.indexY2 = indexY2;
            this.indexXY = indexXY;
        }

        public void computeSigma(Image image) {
            double diffSum = 0;
            for (int y = 0; y < image.height(); ++y) {
                for (int x = 0; x < image.width(); ++x) {
                    double value = image.get(x, y);
                    double expectedValue = predict(x, y);
                    double diff = value - expectedValue;
                    diffSum += diff * diff;
                }
            }
            int n = image.width() * image.height();
            double variable = diffSum / n;
            sigma = Math.sqrt(variable);
            System.out.println("sigma = " + sigma);
        }

        public double getSigma() {
            return sigma;
        }

        public double predict(int x, int y) {
            return indexConst + indexX * x + indexX2 * x * x + indexY * y + indexY2 * y * y + indexXY * x * y;
        }
    }
}
