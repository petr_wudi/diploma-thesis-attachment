package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

public class Erosion extends MorphologicalOperation {

    public Erosion(int kernelDiameter) {
        this(kernelDiameter, 1.0);
    }

    public Erosion(int kernelDiameter, double multiplier) {
        super(Imgproc.MORPH_OPEN, kernelDiameter, multiplier);
    }

    @Override
    public Image apply(Image image) {
        Mat imageMat = image.opencv();

        Mat kernel = Mat.zeros(5, 5, CvType.CV_8U);
        Imgproc.circle(kernel, new Point(2, 2), 2, new Scalar(255), -1);
//        Imgproc.erode(target, target, kernel);


//        Mat erosion = negative(applyOperation(negative(imageMat), Imgproc.MORPH_ERODE));
        Mat erosion = applyOperation(imageMat, Imgproc.MORPH_ERODE);
//        Mat erosion = negative(applyOperation(negative(imageMat), Imgproc.MORPH_ERODE));

        Mat diff = new Mat();
        Core.subtract(imageMat, erosion, diff);
        Core.multiply(diff, new Scalar(3, 3, 3), diff);
        Mat result = new Mat();
        Core.subtract(imageMat, diff, result);
        return new MatImage(result);
    }

    @Override
    public String getIdentifier() {
        return "erosion";
    }
}
