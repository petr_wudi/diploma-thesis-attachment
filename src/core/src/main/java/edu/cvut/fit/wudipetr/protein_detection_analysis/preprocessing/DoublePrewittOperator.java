package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class DoublePrewittOperator extends PrewittOperator {
    public DoublePrewittOperator(int kernelSize) {
        super(kernelSize);
    }

    @Override
    public Image apply(Image image) {
        Mat source = image.opencv();
        Mat derivation1 = apply(source);
        Core.subtract(source, derivation1, derivation1);
        Mat derivation2 = apply(derivation1);
        return new MatImage(derivation2);
    }

    @Override
    public String getIdentifier() {
        return "double-prewitt";
    }

    @Override
    public String getFullName() {
        return "double-prewitt__" + kernelSize;
    }
}
