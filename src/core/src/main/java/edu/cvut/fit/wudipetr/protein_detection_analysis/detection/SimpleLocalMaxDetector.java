package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Algorithm marks all local maximums in their 4-neighbourhood as protein particles.
 */
public class SimpleLocalMaxDetector implements ProteinDetector {
    protected static final double EPS = 0.0001;

    protected final int valueThreshold;

    public SimpleLocalMaxDetector() {
        valueThreshold = 20;
    }

    public SimpleLocalMaxDetector(int threshold) {
        this.valueThreshold = threshold;
    }

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        // todo not duplicate
        int height = image.height();
        int width = image.width();
        int[][] labels = new int[height][width];
        int labelMax = 1;
        List<ImagePoint> proteins = new ArrayList<>();
        PrimitiveMask isIn = new PrimitiveMask(width, height);
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (labels[y][x] == 0 && isLocalMax(image, x, y)) {
                    isIn.clear();
                    DetectionSupport.spread(labels, labelMax++, isIn, x, y, (x1, y1) -> isLocalMax(image, x1, y1));
                    ImagePoint center = isIn.getCenter();
                    proteins.add(center);
                }
            }
        }
        return proteins;
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        int height = image.height();
        int width = image.width();
        int[][] labels = new int[height][width];
        int labelMax = 1;
        List<Protein> proteins = new ArrayList<>();
        PrimitiveMask isIn = new PrimitiveMask(width, height);
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (labels[y][x] == 0 && isLocalMax(image, x, y)) {
                    isIn.clear();
                    DetectionSupport.spread(labels, labelMax++, isIn, x, y, (x1, y1) -> isLocalMax(image, x1, y1));
                    ProteinMask mask = new ArrayProteinMask(isIn);
                    ImagePoint center = isIn.getCenter();
                    Protein p = new Protein(center, mask);
                    proteins.add(p);
                }
            }
        }
        return proteins;
    }

    protected boolean isLocalMax(Image image, int x, int y) {
        double center = image.get(x, y);
        return center > valueThreshold
                && center + EPS >= getValue(image, x+1, y)
                && center + EPS >= getValue(image, x-1, y)
                && center + EPS >= getValue(image, x, y+1)
                && center + EPS >= getValue(image, x, y-1);
    }

    private double getValue(Image image, int x, int y) {
        if (x < 0 || y < 0 || x >= image.width() || y >= image.height()) return -1.0;
        return image.get(x, y);
    }

    protected boolean isSame(Image image, int x, int y, int xx, int yy) {
        double difference = image.get(x, y) - image.get(xx, yy);
        return difference <= EPS && difference >= -EPS;
    }
}
