package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class BilateralFilter implements Preprocessing {
    int winSize;
    double sigmaColor;
    double sigmaSpace;

    public BilateralFilter(int winSize, double sigmaColor, double sigmaSpace) {
        this.winSize = winSize;
        this.sigmaColor = sigmaColor;
        this.sigmaSpace = sigmaSpace;
    }

    @Override
    public Image apply(Image image) {
        Mat imageMat = image.opencv();
        Mat dst = new Mat();
        Imgproc.bilateralFilter(imageMat, dst, winSize, sigmaColor, sigmaSpace);
        return new MatImage(dst);
    }

    @Override
    public String getIdentifier() {
        return "bilateral";
    }

    @Override
    public String getFullName() {
        return getIdentifier() + "__" + winSize + "_" + sigmaColor + "_" + sigmaSpace;
    }
}
