package edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;

public class PrimitiveMask {
    private int minX;
    private int minY;
    private int maxX;
    private int maxY;
    private boolean[][] isIn;

    public PrimitiveMask(int width, int height) {
        isIn = new boolean[height][width];
        minX = Integer.MAX_VALUE;
        maxX = Integer.MIN_VALUE;
        minY = Integer.MAX_VALUE;
        maxY = Integer.MIN_VALUE;
    }

    public boolean get(int x, int y) {
        return isIn[y][x];
    }

    public void set(int x, int y, boolean value) {
        isIn[y][x] = value;
        if (x < minX) minX = x;
        if (x > maxX) maxX = x;
        if (y < minY) minY = y;
        if (y > maxY) maxY = y;
    }

    public int getMinX() {
        return minX;
    }

    public int getMinY() {
        return minY;
    }

    public boolean isEmpty() {
        return minX > maxX;
    }

    public boolean[][] getMinArray() {
        int width = maxX - minX + 1;
        int height = maxY - minY + 1;
        boolean[][] arr = new boolean[height][width];
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                arr[y][x] = isIn[y + minY][x + minX];
            }
        }
        return arr;
    }

    public ImagePoint getCenter() {
        int xSum = 0;
        int ySum = 0;
        int sum = 0;
        for (int y = minY; y <= maxY; ++y) {
            for (int x = minX; x <= maxX; ++x) {
                if (isIn[y][x]) {
                    xSum += x;
                    ySum += y;
                    ++sum;
                }
            }
        }
        if (sum == 0) {
            sum = 1;
        }
        return new ImagePoint(xSum / sum, ySum / sum);
    }

    public void clear() {
        for (int y = minY; y <= maxY; ++y) {
            for (int x = minX; x <= maxX; ++x) {
                isIn[y][x] = false;
            }
        }
        minX = Integer.MAX_VALUE;
        maxX = Integer.MIN_VALUE;
        minY = Integer.MAX_VALUE;
        maxY = Integer.MIN_VALUE;
    }

    public boolean[][] getValues() {
        return isIn;
    }
}
