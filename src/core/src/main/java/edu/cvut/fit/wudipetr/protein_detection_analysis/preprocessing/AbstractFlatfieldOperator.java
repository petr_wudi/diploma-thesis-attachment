package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public abstract class AbstractFlatfieldOperator implements Preprocessing {
    private static final double REFERENCE_COEF_DEFAULT = 0.5;

    protected final double referenceCoef;
    private FlatfieldFunction genericFunction = null;

    public AbstractFlatfieldOperator() {
        this(REFERENCE_COEF_DEFAULT);
    }

    public AbstractFlatfieldOperator(double referenceCoef) {
        this.referenceCoef = referenceCoef;
        if (referenceCoef < 0 || referenceCoef > 1) {
            throw new IllegalArgumentException("Reference coefficient " + referenceCoef + " is not in interval 0-1");
        }
    }

    public void setReferenceImages(Collection<Image> images) {
        if (images.isEmpty()) {
            System.out.println("No reference image was specified");
            return;
        }
        Image randomImage = images.iterator().next();
        int width = randomImage.width();
        int height = randomImage.height();
        if (!checkSize(images, width, height)) {
            System.err.println("Image sizes differ");
            return;
        }
        int pixelPosition = (int) Math.round(Integer.max(images.size() - 1, 0) * referenceCoef);
        Mat medianMat = new Mat(height, width, CvType.CV_8UC3);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                final int currentX = x;
                final int currentY = y;
                List<Double> sortedPixelValues = images.stream()
                        .map(image -> image.get(currentX, currentY))
                        .sorted()
                        .collect(Collectors.toList());
                int pixel = sortedPixelValues.get(pixelPosition).intValue();
                medianMat.put(y, x, pixel, pixel, pixel);
            }
        }
        Image medianImage = new MatImage(medianMat);
        genericFunction = computeFlatfield(medianImage);
    }

    private boolean checkSize(Collection<Image> images, int width, int height) {
        List<Image> badImages = images.stream()
                .filter(image -> image.width() != width || image.height() != height)
                .collect(Collectors.toList());
        for (Image badImage : badImages) {
            System.err.println("Image size differs: " + badImage.width() + "x" + badImage.height() + " vs"
                    + width + "x" + height);
        }
        return badImages.isEmpty();
    }

    protected FlatfieldFunction computeFlatfield(Image image) {
        double[][] matrix = new double[6][];
        for (int i = 0; i < 6; ++i) {
            double[] matrixRow = getMatrixRow(i, image);
            matrix[i] = matrixRow;
        }
        gem(matrix, 7, 6);
        double indexConst = matrix[5][6];
        double indexX = matrix[0][6];
        double indexY = matrix[2][6];
        double indexX2 = matrix[1][6];
        double indexY2 = matrix[3][6];
        double indexXY = matrix[4][6];
        System.out.println(indexX + " " + indexY + " " + indexX2 + " " + indexY2 + " " + indexXY);
        FlatfieldFunction f = new FlatfieldFunction(indexConst, indexX, indexY, indexX2, indexY2, indexXY);
        f.computeSigma(image);
        System.out.println(f);
        return f;
    }

    private double[] getMatrixRow(int selected, Image image) {
        BiFunction<Integer, Integer, Integer>  multiplierFunction = getMultiplierFunction(selected);
        double sumImgA = 0;
        double sumA = 0;
        double sumB = 0;
        double sumC = 0;
        double sumD = 0;
        double sumE = 0;
        double sumF = 0;
        int centerX = image.width() / 2;
        int centerY = image.height() / 2;
        for (int imageY = 0; imageY < image.height(); ++imageY) {
            int y = imageY - centerY;
            for (int imageX = 0; imageX < image.width(); ++imageX) {
                int x = imageX - centerX;
                int multiplier = multiplierFunction.apply(x, y);
                sumImgA += image.get(imageX, imageY) * multiplier;
                sumA += multiplier * x;
                sumB += multiplier * (x * x);
                sumC += multiplier * (y);
                sumD += multiplier * (y * y);
                sumE += multiplier * (x * y);
                sumF += multiplier;
            }
        }
        return new double[]{sumA, sumB, sumC, sumD, sumE, sumF, sumImgA};
    }

    private BiFunction<Integer, Integer, Integer> getMultiplierFunction(int selected) {
        switch (selected) {
            case 0:
                return (x, y) -> x;
            case 1:
                return (x, y) -> x * x;
            case 2:
                return (x, y) -> y;
            case 3:
                return (x, y) -> y * y;
            case 4:
                return (x, y) -> x * y;
            case 5:
                return (x, y) -> 1;
            default:
                throw new IllegalStateException("Multiplier index should not be " + selected);
        }
    }

    /**
     * Performs gauss elimination method on the given matrix.
     * @param matrix input and also output matrix of the gauss elimination
     * @param width width of the matrix
     * @param height height of the matrix
     */
    private void gem(double[][] matrix, int width, int height) {
        for (int row = 0; row < height - 1; ++row) {
            double multiplier = matrix[row][row];
            multiplyNext(matrix, multiplier, row, width, height);
            subtractNext(matrix, row, width, height);
        }
        for (int row = 0; row < height; ++row) {
            double multiplier = matrix[row][row];
            multiplyRow(matrix[row], 1/multiplier);
        }
        for (int row = height - 1; row > 0; --row) {
            subtractPrevious(matrix, row, width, height);
        }
    }

    private void subtractPrevious(double[][] matrix, int rowIndex, int width, int height) {
        double[] row = matrix[rowIndex];
        for (int y = 0; y < rowIndex; ++y) {
            double multiplierRow = matrix[y][rowIndex] / row[rowIndex];
            for (int x = rowIndex; x < width; ++x) {
                matrix[y][x] -= row[x] * multiplierRow;
            }
        }
    }

    /**
     * Multiplies every item in the row by the given number.
     * @param row input and also output row
     * @param multiplier number by which every item in the row is multiplied
     */
    private void multiplyRow(double[] row, double multiplier) {
        for (int x = 0; x < row.length; ++x) {
            row[x] *= multiplier;
        }
    }

    private void subtractNext(double[][] matrix, int rowIndex, int width, int height) {
        double[] firstRow = matrix[rowIndex];
        for (int i = rowIndex + 1; i < height; ++i) {
            for (int x = 0; x < width; ++x) {
                matrix[i][x] -= firstRow[x];
            }
            if (matrix[i][rowIndex] < 0.001 && matrix[i][rowIndex] > -0.001) {
                matrix[i][rowIndex] = 0.0;
            } else {
                System.out.println("AJAJAJ " + matrix[i][rowIndex]);
                matrix[i][rowIndex] = 0.0;
            }
        }
    }

    private void multiplyNext(double[][] matrix, double multiplier, int rowIndex, int width, int height) {
        for (int i = rowIndex + 1; i < height; ++i) {
            double rowMultiplier = multiplier / matrix[i][rowIndex];
            if (Double.isInfinite(rowMultiplier)) {
                continue;
            }
            for (int x = 0; x < width; ++x) {
                matrix[i][x] *= rowMultiplier;
            }
        }
    }
}
