package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class GaussBlur implements Preprocessing {

    private final int kernelSize;
    private final double variance;

    public GaussBlur(int kernelSize, double variance) {
        this.kernelSize = kernelSize;
        this.variance = variance;
    }

    @Override
    public Image apply(Image image) {
        Mat clone = image.opencv().clone();
        Imgproc.GaussianBlur(clone, clone, new Size(kernelSize, kernelSize), variance);
        return new MatImage(clone);
    }

    @Override
    public String getIdentifier() {
        return String.format("gaussblur");
    }

    @Override
    public String getFullName() {
        return getIdentifier() + "__" + kernelSize + "_" + variance;
    }
}
