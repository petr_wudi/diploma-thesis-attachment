package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.Collection;

/**
 * Algorithm splitting the detector using thresholding algorithm. All values below the specified value are suppressed.
 * Remaining pixels are segmented - adjacent pixels form a segment divided from the rest of particles.
 */
public class ThresholdDetector extends AbstractThresholdDetector {
    private static final int DEFAULT_THRESHOLD_MIN = 128;
    private static final int DEFAULT_THRESHOLD_MAX = 255;

    protected final int thresholdMin;
    protected final int thresholdMax;

    /**
     * Creates a new thresholding algorithm.
     */
    public ThresholdDetector() {
        this(DEFAULT_THRESHOLD_MIN);
    }

    /**
     * Creates a new thresholding algorithm.
     * @param thresholdMin minimum allowed value of the pixels
     */
    public ThresholdDetector(int thresholdMin) {
        this(thresholdMin, DEFAULT_THRESHOLD_MAX);
    }

    /**
     * Creates a new thresholding algorithm.
     * @param thresholdMin minimum allowed value of the pixels
     * @param thresholdMax maximum allowed value of the pixels
     */
    public ThresholdDetector(int thresholdMin, int thresholdMax) {
        this.thresholdMin = thresholdMin;
        this.thresholdMax = thresholdMax;
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        Mat imageSrc = image.opencv();
        Mat imageDst = new Mat();
        Imgproc.threshold(imageSrc, imageDst, thresholdMin, thresholdMax, Imgproc.THRESH_BINARY);
        return segmentate(imageDst);
    }
}
