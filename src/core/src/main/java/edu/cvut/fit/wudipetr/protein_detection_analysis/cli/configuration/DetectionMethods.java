package edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration;

enum DetectionMethods {
    HMAXIMA("hmaxima"),
    KMEANS("kmeans"),
    LOCAL_MAXIMUMS("locmax"),
    REGION_GROWING("rgrow"),
    THRESHOLD("threshold"),
    WATER_FILLING("waterfill"),
    WATERSHED("watershed");

    private String name;

    DetectionMethods(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
