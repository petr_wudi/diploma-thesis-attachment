package edu.cvut.fit.wudipetr.protein_detection_analysis.statistics;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;

/**
 * Statistics that returns number of particles in some radius from the particle.
 */
public class NumOfParticlesInDistance implements Statistics<List<Integer>> {
    private static final double RADIUS_DEFAULT = 15;

    private final double radius;
    private final int numOfParticles;
    private final int imageSize;

    public NumOfParticlesInDistance() {
        this(RADIUS_DEFAULT);
    }

    public NumOfParticlesInDistance(int numOfParticles, int imageSize) {
        this.radius = -1.0;
        this.numOfParticles = numOfParticles;
        this.imageSize = imageSize;
    }

    public NumOfParticlesInDistance(double radius) {
        this.radius = radius;
        this.numOfParticles = -1;
        this.imageSize = -1;
    }

    @Override
    public List<Integer> get(Collection<Protein> proteins) {
        double radiusLocal = radius < 0 ? computeRadius(proteins) : radius;
        return proteins.stream()
                .map(proteinParticle -> numOfCloseParticles(proteinParticle, proteins, radiusLocal))
                .collect(Collectors.toList());
    }

    private double computeRadius(Collection<Protein> proteins) {
//        System.out.println("RADIUS = " + Math.sqrt((numOfParticles * imageSize) / ((double) proteins.size() * Math.PI)));
        return Math.sqrt((numOfParticles * imageSize) / ((double) proteins.size() * Math.PI));
    }

    private Integer numOfCloseParticles(Protein refProtein, Collection<Protein> proteins, double radiusLocal) {
        ImagePoint refPosition = refProtein.getPosition();
        return (int) proteins.stream()
                .map(Protein::getPosition)
                .map(buddy -> sqrDistance(refPosition, buddy))
                .filter(distance -> distance <= radiusLocal * radiusLocal)
                .count();
    }

    private int sqrDistance(ImagePoint referencePoint, ImagePoint buddyPoint) {
        int xDist = referencePoint.getX() - buddyPoint.getX();
        int yDist = referencePoint.getX() - buddyPoint.getY();
        return xDist * xDist + yDist * yDist;
    }
}
