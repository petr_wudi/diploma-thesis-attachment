package edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration;

enum PreprocessingMethods {
    GAUSS("gauss");

    private String name;

    PreprocessingMethods(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
