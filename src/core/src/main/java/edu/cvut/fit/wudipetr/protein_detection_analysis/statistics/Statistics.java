package edu.cvut.fit.wudipetr.protein_detection_analysis.statistics;

import java.util.Collection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;

/**
 * Some measure that can be applied to the protein particles
 * @param <T> type of the measure
 */
public interface Statistics<T> {

    /**
     * Gets the statistics from the proteins.
     * @param proteins list of proteins to be analyzed
     * @return analyzed proteins
     */
    T get(Collection<Protein> proteins);
}
