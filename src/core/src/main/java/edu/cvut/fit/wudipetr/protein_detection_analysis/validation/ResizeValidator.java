package edu.cvut.fit.wudipetr.protein_detection_analysis.validation;

import java.util.Collection;
import java.util.stream.Collectors;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

/**
 * Suppresses values lower than minimum or higher than maximum.
 */
public class ResizeValidator implements Validator {

    private final double multiplier;

    public ResizeValidator(double multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public Collection<Protein> filter(Collection<Protein> potentialProteins, Image image) {
        return potentialProteins.stream()
                .map(protein -> {
                    ImagePoint position = protein.getPosition();
                    int newX = (int) (position.getX() * multiplier);
                    int newY = (int) (position.getY() * multiplier);
                    ImagePoint newPosition = new ImagePoint(newX, newY);
                    ProteinMask mask = protein.getMask();
                    ProteinMask targetMask = resizeMask(mask);
                    return new Protein(newPosition, targetMask);
                })
                .collect(Collectors.toList());
    }

    private ProteinMask resizeMask(ProteinMask input) {
        Mat inputMat = input.getMat();
        Size inputSize = inputMat.size();
        int inputWidth = (int) inputSize.width;
        int inputHeight = (int) inputSize.height;
        int targetWidth = (int) (inputSize.width * multiplier);
        int targetHeight = (int) (inputSize.height * multiplier);
        boolean[][] arr = new boolean[targetHeight + 1][targetWidth + 1];
        for (int y = 0; y < inputHeight; ++y) {
            int newY = (int) (y * multiplier);
            for (int x = 0; x < inputWidth; ++x) {
                int newX = (int) (x * multiplier);
                double inputVal = inputMat.get(y, x)[0];
                if (inputVal > 0) {
                    arr[newY][newX] = true;
                }
            }
        }
        return new ArrayProteinMask(arr);
    }

    @Override
    public String getIdentifier() {
        return "resize";
    }

}
