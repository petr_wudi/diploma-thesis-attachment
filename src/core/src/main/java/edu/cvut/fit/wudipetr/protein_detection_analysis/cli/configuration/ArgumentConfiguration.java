package edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ArgumentConfiguration {
    private static final String DEFAULT_METHOD = DetectionMethods.WATERSHED.getName();

    private Mode mode;
    private boolean isDetectionMethodIntialized = false;
    private String detectionMethod = DEFAULT_METHOD;
    private Map<String, String> detectionParams = new HashMap<>();
    private List<String> preprocessingMethods = new ArrayList<>();
    private List<Map<String, String>> preprocessingParams = new LinkedList<>();
    private List<String> validationMethods = new ArrayList<>();
    private List<Map<String, String>> validationParams = new LinkedList<>();
    private List<String> errorMessages = new ArrayList<>();

    public ArgumentConfiguration(String[] args) {
        mode = Mode.RUN;
        Iterator<String> argsIterator = Arrays.asList(args).iterator();
        String problematicArgument = loadArgument(argsIterator);
        if (problematicArgument != null) {
            mode = Mode.BROKEN;
            errorMessages.add("Unknown command: " + problematicArgument);
        }
        if (argsIterator.hasNext()) {
            mode = Mode.BROKEN;
            errorMessages.add("Unknown command: " + argsIterator.next());
        }
        Collections.reverse(preprocessingParams);
        Collections.reverse(validationParams);
    }

    private String loadArgument(Iterator<String> args) {
        if (!args.hasNext()) {
            return null;
        }
        String arg = args.next();
        if (!tryNewArgument(arg, args)) {
            return arg;
        }
        return null;
    }

    private boolean tryNewArgument(String arg, Iterator<String> args) {
        if (arg.equalsIgnoreCase("-h")
                || arg.equalsIgnoreCase("--h")
                || arg.equalsIgnoreCase("-help")
                || arg.equalsIgnoreCase("--help")) {
            mode = Mode.HELP;
            return true;
        } else if (arg.equalsIgnoreCase("-d") || arg.equalsIgnoreCase("--detection")) {
            loadDetection(args);
            return true;
        } else if (arg.equalsIgnoreCase("-p") || arg.equalsIgnoreCase("--preprocessing")) {
            loadPreprocessing(args);
            return true;
        } else if (arg.equalsIgnoreCase("-v") || arg.equalsIgnoreCase("--validation")) {
            loadValidation(args);
            return true;
        }
        return false;
    }

    private void loadDetection(Iterator<String> args) {
        if (isDetectionMethodIntialized) {
            mode = Mode.BROKEN;
            errorMessages.add("Duplicate definition of detection methods");
            return;
        }
        isDetectionMethodIntialized = true;
        if (!args.hasNext()) {
            mode = Mode.BROKEN;
            errorMessages.add("Detection method unspecified.");
            return;
        }
        detectionMethod = args.next();
        detectionParams = loadMethodParams(args);
    }

    private void loadPreprocessing(Iterator<String> args) {
        if (!args.hasNext()) {
            mode = Mode.BROKEN;
            errorMessages.add("Preprocessing method unspecified.");
            return;
        }
        String methodName = args.next();
        preprocessingMethods.add(methodName);
        Map<String, String> params = loadMethodParams(args);
        preprocessingParams.add(params);
    }

    private void loadValidation(Iterator<String> args) {
        if (!args.hasNext()) {
            mode = Mode.BROKEN;
            errorMessages.add("Validation method unspecified.");
            return;
        }
        String methodName = args.next();
        validationMethods.add(methodName);
        Map<String, String> params = loadMethodParams(args);
        validationParams.add(params);
    }

    private Map<String, String> loadMethodParams(Iterator<String> args) {
        Map<String, String> params = new HashMap<>();
        while (args.hasNext()) {
            String paramKey = args.next();
            if (tryNewArgument(paramKey, args)) {  // what if there is -d or something
                return params;
            }
            if (!args.hasNext()) {
                mode = Mode.BROKEN;
                errorMessages.add("Value for parameter " + paramKey + " is not specified.");
                return params;
            }
            String paramValue = args.next();
            System.out.println("Put param " + paramKey + ", " + paramValue);
            params.put(paramKey, paramValue);
        }
        return params;
    }

    public String getDetectionMethod() {
        return detectionMethod;
    }

    public List<String> getPreprocessingMethods() {
        return preprocessingMethods;
    }

    public List<String> getValidationMethods() {
        return validationMethods;
    }

    public List<Map<String, String>> getPreprocessingParams() {
        return preprocessingParams;
    }

    public List<Map<String, String>> getValidationParams() {
        return validationParams;
    }

    public Map<String, String> getDetectionParams() {
        return detectionParams;
    }

    public Mode getMode() {
        return mode;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public enum Mode {
        BROKEN,
        HELP,
        RUN
    }
}
