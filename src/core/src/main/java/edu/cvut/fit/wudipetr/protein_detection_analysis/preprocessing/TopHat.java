package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class TopHat extends MorphologicalOperation {
    private static final double MULTIPLIER_DEFAULT = 1.0;

    public TopHat(int kernelDiameter) {
        this(kernelDiameter, MULTIPLIER_DEFAULT);
    }

    public TopHat(int kernelDiameter, double multiplier) {
        super(Imgproc.MORPH_TOPHAT, kernelDiameter, multiplier);
    }

    @Override
    public Image apply(Image image) {
        Mat imageMat = image.opencv();
        Mat topHat = applyOperation(imageMat, Imgproc.MORPH_TOPHAT);
        Mat negative = negative(imageMat);
        Mat negativeBlackHat = applyOperation(negative, Imgproc.MORPH_BLACKHAT);
        Mat blackHat = negative(negativeBlackHat);
//        Mat topHatNegative = negative(topHat);
        Mat sum = new Mat();
        Core.add(topHat, negativeBlackHat, sum);
//        Mat dst = applyOperation(imageMat, Imgproc.MORPH_TOPHAT);
        return new MatImage(sum);
    }

    @Override
    public String getIdentifier() {
        return "top hat";
    }
}
