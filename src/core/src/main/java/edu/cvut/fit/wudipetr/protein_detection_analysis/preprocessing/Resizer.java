package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;

public class Resizer implements Preprocessing {
    private final int multiplier;

    public Resizer(int multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public Image apply(Image image) {
        Mat input = image.opencv();
        Mat target = new Mat();
        Size targetSize = new Size(input.size().width * multiplier, input.size().height * multiplier);
        Imgproc.resize(input, target, targetSize);
        return new MatImage(target);
    }

    @Override
    public String getIdentifier() {
        return "resize";
    }

    @Override
    public String getFullName() {
        return "resize__" + multiplier;
    }
}
