package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.*;
import java.util.stream.Collectors;

public class WatershedDetector implements ProteinDetector {
    private static final int DEFAULT_VALUE_THRESHOLD = 50;

    LocalMaxDetector localMaxDetector;

    public WatershedDetector() {
        localMaxDetector = new LocalMaxDetector(DEFAULT_VALUE_THRESHOLD);
    }

    public WatershedDetector(int threshold) {
        localMaxDetector = new LocalMaxDetector(threshold);
    }

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        return detectMasks(image).stream().map(Protein::getPosition).collect(Collectors.toList());
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        Mat cvImage = image.opencv();
        Collection<ImagePoint> imagePoints = localMaxDetector.detectCenters(image);
        Mat markers = getMarkers(image, imagePoints);
        Imgproc.watershed(cvImage, markers);
        PrimitiveMask mask = new PrimitiveMask(image.width(), image.height());
        List<Protein> proteins = new ArrayList<>();
        for (ImagePoint point : imagePoints) {
            int x = point.getX();
            int y = point.getY();
            double value = markers.get(y, x)[0];
            if (value < 0.0001) {
                continue;
            }
            spread(x, y, mask, markers, value);
            ProteinMask arrayMask = new ArrayProteinMask(mask);
            mask.clear();
            proteins.add(new Protein(point, arrayMask));
        }
        return proteins;
    }

    private void spread(int x, int y, PrimitiveMask mask, Mat markers, double value) {
        if (value == 0) {
            return;
        }
        List<ImagePoint> todo = new LinkedList<>();
        addPoint(todo, x, y, mask, markers);
        todo.add(new ImagePoint(x, y));
        while (!todo.isEmpty()) {
            ImagePoint pt = todo.remove(0);
            int xx = pt.getX();
            int yy = pt.getY();
            tryAddPoint(xx-1, yy, mask, markers, value, todo);
            tryAddPoint(xx+1, yy, mask, markers, value, todo);
            tryAddPoint(xx, yy-1, mask, markers, value, todo);
            tryAddPoint(xx, yy+1, mask, markers, value, todo);
        }
    }

    private static void addPoint(List<ImagePoint> todo, int x, int y, PrimitiveMask mask, Mat markers) {
        markers.put(y, x, 0);
        mask.set(x, y, true);
        todo.add(new ImagePoint(x, y));
    }

    private static void tryAddPoint(int x, int y, PrimitiveMask mask, Mat markers, double value, List<ImagePoint> todo) {
        if (x >= 0 && y >= 0 && x < markers.width() && y < markers.height() && markers.get(y, x)[0] == value) { // todo maybe use epsilon
            addPoint(todo, x, y, mask, markers);
        }
    }

    private Mat getMarkers(Image image, Collection<ImagePoint> imagePoints) {
        Size size = new Size(image.width(), image.height());
        Mat markers = new Mat(size, CvType.CV_32SC1, new Scalar(0));
        int i = 1;
        for (ImagePoint point : imagePoints) {
            markers.put(point.getY(), point.getX(), i);
            if (++i >= 256) {
                i = 1;
            }
        }
        return markers;
    }
}
