package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

public class PrewittSubtract extends PrewittOperator {
    private double multiplier;

    public PrewittSubtract(int kernelSize) {
        this(kernelSize, 1.0);
    }

    public PrewittSubtract(int kernelSize, double multiplier) {
        super(kernelSize);
        this.multiplier = multiplier;
    }

    @Override
    public Image apply(Image image) {
        Mat source = image.opencv();
        Mat derivation1 = apply(source);
        Core.multiply(derivation1, new Scalar(multiplier, multiplier, multiplier), derivation1);
        Mat result = new Mat();
        Core.subtract(source, derivation1, result);
        return new MatImage(result);
    }

    @Override
    public String getIdentifier() {
        return "prewitt-subtractor";
    }

    @Override
    public String getFullName() {
        return "prewitt-subtractor__" + kernelSize;
    }
}
