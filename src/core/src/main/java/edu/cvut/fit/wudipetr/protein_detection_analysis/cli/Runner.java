package edu.cvut.fit.wudipetr.protein_detection_analysis.cli;

import edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration.ProgramConfiguration;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Runs a tasks.
 */
public class Runner {
    private static final File FILE_DIR = Paths.get("..", "img").toFile();
    protected static final String[] FILE_NAMES = {
            "input.png"
    };
    private static final File OUTPUT_DIR = new File(FILE_DIR, "output");

    public static void run(ProgramConfiguration programConfiguration) {
        List<File> inputFiles = Arrays.stream(FILE_NAMES)
                .map(fileName -> new File(FILE_DIR, fileName))
                .collect(Collectors.toList());
        Task task = new Task();
        task.setInputFiles(inputFiles);
        task.setOutputDir(OUTPUT_DIR);
        task.setProgramConfiguration(programConfiguration);
        task.run();
    }
}
