package edu.cvut.fit.wudipetr.protein_detection_analysis.detection.evolution;

public interface CrossoverAlgorithm {
    Gene[] crossover(Gene g1, Gene g2);
}
