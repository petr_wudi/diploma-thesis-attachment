package edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration;

enum ValidationMethods {
    HMAXIMA("hmaxima"),
    SIZE("size");

    private String name;

    ValidationMethods(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
