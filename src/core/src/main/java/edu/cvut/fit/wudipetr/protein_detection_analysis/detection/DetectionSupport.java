package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Support class with methods shared among the detection algorithms.
 */
public class DetectionSupport {
    private DetectionSupport() {
        // support class
    }

    /**
     * Finds all pixels that satisfy the specified function and are adjacent to the starting pixel or any another found
     * pixel.
     * @param labels all "found" pixels will have the "label" value in this array
     * @param label new value of the "found" pixels in the labels array
     * @param isIn mask to which all found pixels will be written
     * @param x X coordinate of the starting pixel
     * @param y Y coordinate fo the starting pixel
     * @param fct function which all "found" points must satisfy
     */
    public static void spread(int[][] labels, int label, PrimitiveMask isIn, int x, int y, BiFunction<Integer, Integer, Boolean> fct) {
        List<ImagePoint> todo = new LinkedList<>();
        addPoint(todo, x, y, labels, label, isIn);
        while (!todo.isEmpty()) {
            ImagePoint pt = todo.remove(0);
            int xx = pt.getX();
            int yy = pt.getY();
            tryAddPoint(xx-1, yy, labels, label, isIn, fct, todo);
            tryAddPoint(xx+1, yy, labels, label, isIn, fct, todo);
            tryAddPoint(xx, yy-1, labels, label, isIn, fct, todo);
            tryAddPoint(xx, yy+1, labels, label, isIn, fct, todo);
        }
    }

    /**
     * Finds all pixels that have true value in the specified array and are adjacent to the starting pixel or any
     * another found pixel.
     * @param labels all "found" pixels will have the "label" value in this array
     * @param label new value of the "found" pixels in the labels array
     * @param isIn mask to which all found pixels will be written
     * @param x X coordinate of the starting pixel
     * @param y Y coordinate fo the starting pixel
     * @param isZeroArr array in which all "found" pixels must be positive
     */
    public static void spread(int[][] labels, int label, PrimitiveMask isIn, int x, int y, boolean[][] isZeroArr) {
        List<ImagePoint> todo = new LinkedList<>();
        addPoint(todo, x, y, labels, label, isIn);
        while (!todo.isEmpty()) {
            ImagePoint pt = todo.remove(0);
            int xx = pt.getX();
            int yy = pt.getY();
            tryAddPoint(xx-1, yy, labels, label, isIn, isZeroArr, todo);
            tryAddPoint(xx+1, yy, labels, label, isIn, isZeroArr, todo);
            tryAddPoint(xx, yy-1, labels, label, isIn, isZeroArr, todo);
            tryAddPoint(xx, yy+1, labels, label, isIn, isZeroArr, todo);
        }
    }

    private static void addPoint(List<ImagePoint> todo, int x, int y, int[][] labels, int label, PrimitiveMask isIn) {
        labels[y][x] = label;
        isIn.set(x, y, true);
        todo.add(new ImagePoint(x, y));
    }

    private static void tryAddPoint(int x, int y, int[][] labels, int label, PrimitiveMask isIn, BiFunction<Integer, Integer, Boolean> fct, List<ImagePoint> todo) {
        if (checkPoint(x, y, labels, fct)) {
            addPoint(todo, x, y, labels, label, isIn);
        }
    }

    private static void tryAddPoint(int x, int y, int[][] labels, int label, PrimitiveMask isIn, boolean[][] isZeroArr, List<ImagePoint> todo) {
        if (checkPoint(x, y, labels, isZeroArr)) {
            addPoint(todo, x, y, labels, label, isIn);
        }
    }

    private static boolean checkPoint(int x, int y, int[][] labels, boolean[][] isZeroArr) {
        return x >= 0 && y >= 0 && y < labels.length && x < labels[0].length && labels[y][x] == 0 && isZeroArr[y][x];
    }

    private static boolean checkPoint(int x, int y, int[][] labels, BiFunction<Integer, Integer, Boolean> fct) {
        return x >= 0 && y >= 0 && y < labels.length && x < labels[0].length && labels[y][x] == 0 && fct.apply(x, y);
    }
}
