package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;

import java.util.Collection;

/**
 * Detects protein occurrence in an image
 */
public interface ProteinDetector {

    /**
     * @return list of points where proteins are
     */
    Collection<ImagePoint> detectCenters(Image image);

    /**
     * @return list of mask, each mask descibes shape of one protein in original image
     */
    Collection<Protein> detectMasks(Image image);

}
