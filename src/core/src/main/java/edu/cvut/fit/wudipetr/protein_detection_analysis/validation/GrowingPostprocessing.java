package edu.cvut.fit.wudipetr.protein_detection_analysis.validation;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;

import java.util.Collection;

public class GrowingPostprocessing implements Validator {

    @Override
    public Collection<Protein> filter(Collection<Protein> potentialProteins, Image image) {

        boolean[][] isTaken = new boolean[image.height()][image.width()];
        for (Protein protein : potentialProteins) {
            if (isTaken[protein.getPosition().getY()][protein.getPosition().getX()]) {
                continue;
            }
            protein.getMask().apply((x, y) -> isTaken[y][x] = true);

        }
        
        for (Protein protein : potentialProteins) {
            protein.getMask().apply((x, y) -> isTaken[y][x] = true);
        }
        return null;
    }

    @Override
    public String getIdentifier() {
        return "grow";
    }
}
