package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class OtsuDetector extends AbstractThresholdDetector {

    public OtsuDetector() {
    }

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        return detectMasks(image).stream().map(Protein::getPosition).collect(Collectors.toList());
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        Mat imageSrc = image.opencv();
        Mat imageDst = new Mat();
        List<Mat> channels = new ArrayList<>();
        Core.split(imageSrc, channels);
        List<Mat> channelsDst = new ArrayList<>();
        for (Mat channel : channels) {
            Mat channelDst = new Mat();
            Imgproc.threshold(channel, channelDst, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
            channelsDst.add(channelDst);
        }
        Core.merge(channelsDst, imageDst);
        return segmentate(imageDst);
    }
}
