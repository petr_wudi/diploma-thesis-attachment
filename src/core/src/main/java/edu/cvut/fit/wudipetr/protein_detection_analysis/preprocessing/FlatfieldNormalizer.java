package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class FlatfieldNormalizer extends AbstractFlatfieldOperator {
    private static final double REFERENCE_COEF_DEFAULT = 0.5;

    private FlatfieldFunction genericFunction = null;

    public FlatfieldNormalizer() {
        this(REFERENCE_COEF_DEFAULT);
    }

    public FlatfieldNormalizer(double referenceCoef) {
        super(referenceCoef);
        if (referenceCoef < 0 || referenceCoef > 1) {
            throw new IllegalArgumentException("Reference coefficient " + referenceCoef + " is not in interval 0-1");
        }
    }

    @Override
    public Image apply(Image image) {
        FlatfieldFunction function = genericFunction;
        if (function == null) {
            System.out.println("Compute the flatfield function");
            function = computeFlatfield(image);
        }
        Mat source = image.opencv();
        Mat target = new Mat(source.size(), source.type());
//        double minValue = MAX_SIGMA_MULTIPLIER_DEFAULT * function.getSigma();
        int centerX = image.width() / 2;
        int centerY = image.height() / 2;
        double centerPredicted = function.predict(centerX, centerY);
        for (int imageY = 0; imageY < image.height(); ++imageY) {
            int y = imageY - centerY;
            for (int imageX = 0; imageX < image.width(); ++imageX) {
                int x = imageX - centerX;
                double[] values = source.get(imageY, imageX);
                double predicted = function.predict(x, y);
                double toAdd = - predicted + centerPredicted;
                double b = Double.max(0, values[0] + toAdd);
                double g = Double.max(0, values[1] + toAdd);
                double r = Double.max(0, values[2] + toAdd);
//                double b = 20 + (predicted - centerPredicted) * 20;
//                double g = b;
//                double r = b;
                target.put(imageY, imageX, b, g, r);
//                target.put(imageY, imageX, predicted, predicted, predicted);
            }
        };
        return new MatImage(target);
    }

    @Override
    public String getIdentifier() {
        return "flatfield";
    }

    @Override
    public String getFullName() {
        return getIdentifier() + "__" + referenceCoef;
    }
}
