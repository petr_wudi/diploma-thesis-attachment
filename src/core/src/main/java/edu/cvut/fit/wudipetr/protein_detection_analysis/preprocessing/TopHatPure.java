package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class TopHatPure extends MorphologicalOperation {
    private static final double MULTIPLIER_DEFAULT = 1.0;

    public TopHatPure(int kernelDiameter) {
        this(kernelDiameter, MULTIPLIER_DEFAULT);
    }

    public TopHatPure(int kernelDiameter, double multiplier) {
        super(Imgproc.MORPH_TOPHAT, kernelDiameter, multiplier);
    }

    @Override
    public Image apply(Image image) {
        Mat dstMat = applyOperation(image.opencv(), Imgproc.MORPH_TOPHAT);
        return new MatImage(dstMat);
    }

    @Override
    public String getIdentifier() {
        return "top hat";
    }
}
