package edu.cvut.fit.wudipetr.protein_detection_analysis.cli;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Location of a point in an image, represented by X and Y coords.
 */
public class View {
    private static final int POINT_RADIUS = 5;
    private static final Random RANDOM = new Random();

    private static final View instance = new View();

    private View() {
    }

    public static View getInstance() {
        return instance;
    }

    public void drawPoints(Mat image, Collection<ImagePoint> positions) {
        for (ImagePoint point : positions) {
            Imgproc.circle(image, new Point(point.getX(), point.getY()), POINT_RADIUS, new Scalar(255, 255, 0));
        }
    }

    public Mat drawDensity(Size size, Collection<ImagePoint> positions) {
        Mat density = computeDensityNormalized(size, positions);
        Mat colorDensity = new Mat(density.size(), CvType.CV_8UC3);
        Imgproc.cvtColor(density, colorDensity, Imgproc.COLOR_GRAY2BGR);
        return colorDensity;
    }

    private Mat computeDensityNormalized(Size size, Collection<ImagePoint> positions) {
        Mat density = computeDensity(size, positions);
        Core.MinMaxLocResult minMax = Core.minMaxLoc(density);
        double min = minMax.minVal;
        double max = minMax.maxVal - min;
        Core.subtract(density, new Scalar(min), density);
        Core.multiply(density, new Scalar(1 / max), density);
        return density;
    }

    private Mat computeDensity(Size size, Collection<ImagePoint> positions) {
        Mat density = Mat.zeros(size, CvType.CV_32F);
        for (ImagePoint point : positions) {
            int maxBit = 1;
            Imgproc.circle(density, new Point(point.getX(), point.getY()), 1, new Scalar(maxBit, maxBit, maxBit), -1);
        }
        Mat densityBlur = new Mat();
        Imgproc.GaussianBlur(density, densityBlur, new Size(555, 555), 60.0);
        return densityBlur;
    }

    private Mat drawDensityCircle(Mat colorfulImg, Mat density) {
        Mat density8bit = new Mat();
        density.convertTo(density8bit, CvType.CV_8UC1);
        Mat thresholded = new Mat();
        Imgproc.threshold(density8bit, thresholded, 0, 255, Imgproc.THRESH_OTSU);
        List<MatOfPoint> contour = new ArrayList<>();
        Mat contoursMat = Mat.zeros(density.size(), CvType.CV_8U);
        Imgproc.findContours(thresholded, contour, contoursMat, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        Mat thresholded3 = new Mat();
        Imgproc.cvtColor(thresholded, thresholded3, Imgproc.COLOR_GRAY2BGR, 3);
        Core.multiply(thresholded3, new Scalar(0, 0.4, 0), thresholded3);
        Core.add(colorfulImg, thresholded3, colorfulImg);
        Core.multiply(colorfulImg, new Scalar(0.9, 0.9, 0.9), colorfulImg);
        Imgproc.drawContours(colorfulImg, contour, -1, new Scalar(30, 255, 30), 15);
        return colorfulImg;
    }

    public Mat drawDensityCircle(Mat image, Collection<ImagePoint> positions) {
        Mat density = computeDensityNormalized(image.size(), positions);
        Mat imgClone = image.clone();
        return drawDensityCircle(imgClone, density);
    }

    private double getMaxValue(Mat density) { // todo use opencv
        double valueMax = 1;
        for (int y = 0; y < density.height(); ++y) {
            for (int x = 0; x < density.width(); ++x) {
                double[] values = density.get(y, x);
                double value = values[0];
                if (value > valueMax) {
                    valueMax = value;
                }
            }
        }
        return valueMax;
    }

    private Mat convertDensityToRgb(Mat density) {
        Mat colorfulImg = Mat.zeros(density.size(), CvType.CV_8UC3);
        Imgproc.cvtColor(colorfulImg, colorfulImg, Imgproc.COLOR_BGR2HSV);
        double valueMax = getMaxValue(density);
        for (int y = 0; y < density.height(); ++y) {
            for (int x = 0; x < density.width(); ++x) {
                double[] values = density.get(y, x);
                double valueBig = values[0];
                double valueNorm = (int)((valueBig / valueMax) * 255);
                double hue = 2 * 255.0 / 3 + (valueNorm / 3);
                double saturation = 255;
                double value = valueNorm;
                colorfulImg.put(y, x, hue, saturation, value);
            }
        }
        Mat colorfulImg2 = Mat.zeros(density.size(), CvType.CV_8UC3);
        Imgproc.cvtColor(colorfulImg, colorfulImg2, Imgproc.COLOR_HSV2BGR_FULL, 3);
        return colorfulImg2;
    }

    /**
     * Draws masks using color areas. The colors of the images are random and might repeat. Similar to
     * {@link #drawFilling(Mat, Collection, boolean)}.
     * @param image input and also output image
     * @param masks areas covered by those masks are drawn. The masks should not cover each other.
     */
    public void drawFilling(Mat image, Collection<ProteinMask> masks) {
        drawFilling(image, masks, false);
    }

    /**
     * Draws masks using color areas.
     * @param image input and also output image
     * @param masks areas covered by those masks are drawn. The masks should not cover each other.
     * @param ascendingColors if true, colors are chosen deterministically. The mask colors might have very small
     *                        difference thus it's intended to use for computer processing. If false, colors are chosen
     *                        randomly and they are more colorful and differ more.
     */
    public void drawFilling(Mat image, Collection<ProteinMask> masks, boolean ascendingColors) {
        Mat colorMat = new Mat(image.size(), image.type());
        colorMat.setTo(new Scalar(255, 255, 0));
        int color = 255 * 255 * 255;
        for (ProteinMask mask : masks) {
            double[] colors = generateColors(ascendingColors, color--);
            mask.apply((x, y) -> image.put(y, x, colors));
        }
    }

    private double[] generateColors(boolean ascendingColors, int color) {
        if (ascendingColors) {
            int blue = (color >> 8) % 255;
            int green = (color >> 4) % 255;
            int red = color % 255;
            return new double[]{blue, green, red};
        } else {
            int blue = RANDOM.nextInt(156) + 50;
            int green = RANDOM.nextInt(156) + 50;
            return new double[]{blue, green, 0};
        }
    }
}
