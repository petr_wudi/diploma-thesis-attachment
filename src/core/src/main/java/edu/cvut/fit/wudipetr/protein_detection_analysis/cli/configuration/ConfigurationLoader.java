package edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.HMaximaDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.KMeansDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.LocalMaxDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.RegionGrowingDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ThresholdDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.WaterFillingDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.WatershedDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.GaussBlur;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.HMaximaValidator;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.SizeValidator;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;

/**
 * Converts configuration to a set of methods.
 */
public class ConfigurationLoader {
    private ConfigurationLoader() {}

    public static ProgramConfiguration load(ArgumentConfiguration argConfig) throws ConfigException {
        List<Preprocessing> preprocessingList = new ArrayList<>(argConfig.getPreprocessingMethods().size());
        Iterator<String> preprocessingMethods = argConfig.getPreprocessingMethods().iterator();
        Iterator<Map<String, String>> preprocessingParams = argConfig.getPreprocessingParams().iterator();
        while (preprocessingMethods.hasNext() && preprocessingParams.hasNext()) {
            String methodName = preprocessingMethods.next();
            Map<String, String> params = preprocessingParams.next();
            Preprocessing preprocessing = loadPreprocessing(methodName, params);
            preprocessingList.add(preprocessing);
        }
        ProteinDetector detector = loadDetector(argConfig);
        List<Validator> validators = new ArrayList<>(argConfig.getValidationMethods().size());
        Iterator<String> validationMethods = argConfig.getValidationMethods().iterator();
        Iterator<Map<String, String>> validationParams = argConfig.getValidationParams().iterator();
        while (validationMethods.hasNext() && validationParams.hasNext()) {
            String methodName = validationMethods.next();
            Map<String, String> params = validationParams.next();
            Validator validator = loadValidator(methodName, params);
            validators.add(validator);
        }
        String methodName = getMethodName(argConfig);
        return new ProgramConfiguration(preprocessingList, detector, validators, methodName);
    }

    private static Preprocessing loadPreprocessing(String methodName, Map<String, String> params) throws ConfigException {
        if (methodName.equalsIgnoreCase(PreprocessingMethods.GAUSS.getName())) {
            if (params.size() == 2) {
                int kernelSize = ConfigurationLoader.getIntegerParam(params, "kernelSize");
                double variance = ConfigurationLoader.getDoubleParam(params, "variance");
                return new GaussBlur(kernelSize, variance);
            }
        }
        throw new ConfigException("Can't find validation method " + methodName);
    }

    private static Validator loadValidator(String methodName, Map<String, String> params) throws ConfigException {
        if (methodName.equalsIgnoreCase(ValidationMethods.HMAXIMA.getName())) {
            if (params.size() == 1) {
                int h = ConfigurationLoader.getIntegerParam(params, "h");
                return new HMaximaValidator(h);
            }
        } else if (methodName.equalsIgnoreCase(ValidationMethods.SIZE.getName())) {
            if (params.size() == 1) {
                int minArea = ConfigurationLoader.getIntegerParam(params, "minArea");
                return new SizeValidator(minArea);
            }
        }
        throw new ConfigException("Can't find validation method " + methodName);
    }

    private static ProteinDetector loadDetector(ArgumentConfiguration argConfig) throws ConfigException {
        String methodName = argConfig.getDetectionMethod();
        if (methodName == null || methodName.trim().length() == 0) {
            throw new ConfigException("Detection method was't specified.");
        }
        // todo method number check
        if (methodName.equalsIgnoreCase(DetectionMethods.HMAXIMA.getName())) {
            if (argConfig.getDetectionParams().size() == 0) {
                return new HMaximaDetector();
            } else if (argConfig.getDetectionParams().size() == 3) {
                int h = ConfigurationLoader.getIntegerParam(argConfig.getDetectionParams(), "h");
                int radius = ConfigurationLoader.getIntegerParam(argConfig.getDetectionParams(), "radius");
                int duplicityThreshold = ConfigurationLoader.getIntegerParam(argConfig.getDetectionParams(), "duplicityThreshold");
                return new HMaximaDetector(h, radius, duplicityThreshold);
            }
            return new HMaximaDetector();
        } else if (methodName.equalsIgnoreCase(DetectionMethods.KMEANS.getName())) {
            return new KMeansDetector();
        } else if (methodName.equalsIgnoreCase(DetectionMethods.LOCAL_MAXIMUMS.getName())) {
            if (argConfig.getDetectionParams().size() == 0) {
                return new LocalMaxDetector();
            } else if (argConfig.getDetectionParams().size() == 1) {
                int threshold = ConfigurationLoader.getIntegerParam(argConfig.getDetectionParams(), "threshold");
                return new LocalMaxDetector(threshold);
            }
        } else if (methodName.equalsIgnoreCase(DetectionMethods.REGION_GROWING.getName())) {
            if (argConfig.getDetectionParams().size() == 1) {
                int threshold = ConfigurationLoader.getIntegerParam(argConfig.getDetectionParams(), "threshold");
                return new RegionGrowingDetector(threshold);
            } else if (argConfig.getDetectionParams().size() == 2) {
                int threshold = ConfigurationLoader.getIntegerParam(argConfig.getDetectionParams(), "threshold");
                int minThreshold = ConfigurationLoader.getIntegerParam(argConfig.getDetectionParams(), "minThreshold");
                return new RegionGrowingDetector(threshold, minThreshold);
            }
        } else if (methodName.equalsIgnoreCase(DetectionMethods.THRESHOLD.getName())) {
            if (argConfig.getDetectionParams().size() == 1) {
                int threshold = ConfigurationLoader.getIntegerParam(argConfig.getDetectionParams(), "threshold");
                return new ThresholdDetector(threshold);
            }
        } else if (methodName.equalsIgnoreCase(DetectionMethods.WATER_FILLING.getName())) {
            return new WaterFillingDetector();
        } else if (methodName.equalsIgnoreCase(DetectionMethods.WATERSHED.getName())) {
            if (argConfig.getDetectionParams().size() == 0) {
                return new WatershedDetector();
            } else if (argConfig.getDetectionParams().size() == 1) {
                int threshold = ConfigurationLoader.getIntegerParam(argConfig.getDetectionParams(), "threshold");
                return new WatershedDetector(threshold);
            }
        }
        throw new ConfigException("Can't find detection method " + methodName);
    }

    private static String getMethodName(ArgumentConfiguration argConfig) {
        return argConfig.getDetectionMethod() + paramsToString(argConfig.getDetectionParams());
        // todo preprocessing and postprocessing
    }

    private static String paramsToString(Map<String, String> params) {
        return params.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .collect(Collectors.joining());
    }

    /**
     * Reads parameter with the specified name from the map and converts it to an integer.
     * @param params map of parameters, should contain key with the specified param name, otherwise it returns an
     *               exception
     * @param paramName name of the parameter whose value is returned
     * @return parameter with the specified name converted to integer
     * @throws ConfigException the parameter doesn't exist or it's not possible to convert it to an integer
     */
    private static int getIntegerParam(Map<String, String> params, String paramName) throws ConfigException {
        String paramString = params.get(paramName);
        if (paramString == null) {
            throw new ConfigException("Parameter " + paramName + " was not specified");
        }
        try {
            return Integer.parseInt(paramString);
        } catch (NumberFormatException e) {
            throw new ConfigException("Cant' parse (whole number) value of parameter " + paramName, e);
        }
    }

    /**
     * Reads parameter with the specified name from the map and converts it to a double.
     * @param params map of parameters, should contain key with the specified param name, otherwise it returns an
     *               exception
     * @param paramName name of the parameter whose value is returned
     * @return parameter with the specified name converted to double
     * @throws ConfigException the parameter doesn't exist or it's not possible to convert it to a double
     */
    private static double getDoubleParam(Map<String, String> params, String paramName) throws ConfigException {
        String paramString = params.get(paramName);
        if (paramString == null) {
            throw new ConfigException("Parameter " + paramName + " was not specified");
        }
        try {
            return Double.parseDouble(paramString);
        } catch (NumberFormatException e) {
            throw new ConfigException("Cant' parse (decimal number) value of parameter " + paramName, e);
        }
    }
}
