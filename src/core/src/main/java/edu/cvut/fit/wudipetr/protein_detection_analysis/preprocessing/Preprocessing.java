package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;

public interface Preprocessing {
    Image apply(Image image);

    String getIdentifier();

    String getFullName();
}
