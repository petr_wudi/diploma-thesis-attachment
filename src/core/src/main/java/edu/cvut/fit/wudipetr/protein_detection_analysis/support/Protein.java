package edu.cvut.fit.wudipetr.protein_detection_analysis.support;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

public class Protein {
    ImagePoint position;
    ProteinMask mask;

    public Protein(ImagePoint position, ProteinMask mask) {
        this.position = position;
        this.mask = mask;
    }

    public ImagePoint getPosition() {
        return position;
    }

    public ProteinMask getMask() {
        return mask;
    }
}
