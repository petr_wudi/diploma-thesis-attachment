package edu.cvut.fit.wudipetr.protein_detection_analysis.statistics;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;

/**
 * Statistics that creates a map from distance to n-th closest particle to number of particles
 * having the distance.
 */
public class DistanceToNthClosest implements Statistics<Map<Integer, Integer>> {
    private final int numOfParticle;

    public DistanceToNthClosest() {
        this(1);
    }

    public DistanceToNthClosest(int numOfParticle) {
        this.numOfParticle = numOfParticle;
    }

    @Override
    public Map<Integer, Integer> get(Collection<Protein> proteins) {
        Map<Integer, Integer> distanceMap = new HashMap<>();
        proteins.stream()
                .map(protein -> computeDistance(protein, proteins))
                .forEach(distance -> {
                    int oldCount = distanceMap.getOrDefault(distance, 0);
                    distanceMap.put(distance, oldCount + 1);
                });
        return distanceMap;
    }

    private int computeDistance(Protein reference, Collection<Protein> proteins) {
        ImagePoint referencePoint = reference.getPosition();
        List<Double> lowestValuesSqr = new LinkedList<>();
        for (Protein buddy : proteins) {
            ImagePoint buddyPoint = buddy.getPosition();
            if (referencePoint == buddyPoint) {
                continue;
            }
            double distanceSqr = sqrDistance(referencePoint, buddyPoint);
            int index = 0;
            while (lowestValuesSqr.size() > index) {
                double value = lowestValuesSqr.get(index);
                if (distanceSqr <= value) {
                    break;
                }
                index++;
            }
            if (index < numOfParticle) {
                lowestValuesSqr.add(index, distanceSqr);
            }
            if (lowestValuesSqr.size() > numOfParticle) {
                lowestValuesSqr.remove(numOfParticle);
            }
        }
        double lowestValueSqr = lowestValuesSqr.size() == numOfParticle ? lowestValuesSqr.get(numOfParticle - 1) : -1;
        double distance = Math.sqrt(lowestValueSqr);
        return (int) distance;
    }

    private double sqrDistance(ImagePoint referencePoint, ImagePoint buddyPoint) {
        int xDist = referencePoint.getX() - buddyPoint.getX();
        int yDist = referencePoint.getX() - buddyPoint.getY();
        return xDist * xDist + yDist * yDist;
    }
}
