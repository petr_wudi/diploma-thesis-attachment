package edu.cvut.fit.wudipetr.protein_detection_analysis.validation;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

import java.util.*;

public class HMaximaValidator implements Validator {
    protected int height;
    protected int width;
    protected int[][] arr;

    /**
     * Local maxima must be higher than everything in its neighborhood higher
     * than or equal to local maxima's value minus this number
     */
    protected int h = 10;

    public HMaximaValidator(int h) {
        this.h = h;
    }

    @Override
    public Collection<Protein> filter(Collection<Protein> potentialProteins, Image image) {
        height = image.height();
        width = image.width();
        arr = image.getArray();


        List<Protein> hMaximums = new ArrayList<>();
        boolean[][] visited = new boolean[height][width];
        PrimitiveMask newVisited = new PrimitiveMask(width, height);
        PrimitiveMask tempVisited = new PrimitiveMask(width, height);
        for (Protein point : potentialProteins) {
            int x = point.getPosition().getX();
            int y = point.getPosition().getY();
            if (visited[y][x]) continue;
            newVisited.clear();
            tempVisited.clear();
            visited[y][x] = true;
            newVisited.set(x, y, true);
            tempVisited.set(x, y, true);
            if (isHmaxima(x, y, arr[y][x] - h, visited, newVisited, tempVisited)) {
                boolean isCoveringAnother = false;
                for (Protein currentMax : hMaximums) {
                    if (newVisited.get(currentMax.getPosition().getX(), currentMax.getPosition().getY())) {
                        isCoveringAnother = true;
                    }
                }
                if (!isCoveringAnother) {
                    ProteinMask mask = new ArrayProteinMask(newVisited);
                    Protein protein = new Protein(point.getPosition(), mask);
                    hMaximums.add(protein);
                }
            }
        }
        System.out.println("FILTER " + potentialProteins.size() + " to " + hMaximums.size());
        return hMaximums;
    }

    /**
     * Return whether all values in specified point's neighborhood that are
     * higher that lowerBound are also lower or equal than specified point
     * @param refX X coord of the point
     * @param refY Y coord of the point
     * @param lowerBound Algorithm will ignore values lower than this
     * @param visited Array of points that are checked and could be ignored
     * @param newVisited
     * @return Whether point is h-maxima or not
     */
    private boolean isHmaxima(int refX, int refY, int lowerBound, boolean[][] visited, PrimitiveMask newVisited, PrimitiveMask tempVisited) {
        List<HMaximaState> queue = new LinkedList<>();
        queue.add(new HMaximaState(refX, refY));
        while (!queue.isEmpty()) {
            HMaximaState s = queue.remove(0);
            int x = s.getX();
            int y = s.getY();
            // Higher value
            if(arr[y][x] > arr[refY][refX]) {
                return false;
            }
            // Already visited (but not by this run of the algorithm)
            if(arr[y][x] == arr[refY][refX] && visited[y][x] && !newVisited.get(x, y)) {
                return false;
            }
            // Set values
            visited[y][x] = true;
            newVisited.set(x, y, true);
            // Add new values
            tryAddHmaxima(queue, x-1, y, lowerBound, tempVisited);
            tryAddHmaxima(queue, x+1, y, lowerBound, tempVisited);
            tryAddHmaxima(queue, x, y-1, lowerBound, tempVisited);
            tryAddHmaxima(queue, x, y+1, lowerBound, tempVisited);
        }
        return true;
    }

    /**
     * Tries to call {@link #isHmaxima}
     * @param x X coord of point that will be used as argument
     * @param y Y coord of point that will be used as argument
     * @param lowerBound Point with value lower than this will be ignored
     * @param tempVisited
     */
    private void tryAddHmaxima(List<HMaximaState> queue, int x, int y, int lowerBound, PrimitiveMask tempVisited) {
        if (isInsideImage(x, y) && !tempVisited.get(x, y) && arr[y][x] >= lowerBound) {
            queue.add(new HMaximaState(x, y));
            tempVisited.set(x, y, true);
        }
    }

    private boolean isInsideImage(int x, int y) {
        return x >= 0 && y >= 0 && x < width && y < height;
    }

    @Override
    public String getIdentifier() {
        return "hmaxima";
    }

    private class HMaximaState {
        int x;
        int y;

        public HMaximaState(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
