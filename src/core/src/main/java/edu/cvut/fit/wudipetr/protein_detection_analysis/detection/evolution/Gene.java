package edu.cvut.fit.wudipetr.protein_detection_analysis.detection.evolution;

import java.util.Collection;
import java.util.List;

public class Gene {
    protected List<Particle> particles;

    public Gene() {

    }

    public void add(Particle particle) {
        particles.add(particle);
    }

    public Collection<Particle> getParticles() {
        return particles;
    }
}