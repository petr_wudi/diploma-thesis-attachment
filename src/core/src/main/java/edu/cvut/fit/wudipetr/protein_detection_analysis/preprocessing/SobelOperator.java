package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class SobelOperator implements Preprocessing {
    protected final int kernelSize;

    public SobelOperator(int kernelSize) {
        this.kernelSize = kernelSize;
    }

    @Override
    public Image apply(Image image) {
        Mat source = image.opencv();
        Mat target = apply(source);
        return new MatImage(target);
    }

    protected Mat apply(Mat source) {
        Mat target = new Mat();
        Imgproc.Sobel(source, target, CvType.CV_8U,1,1, kernelSize);
        return target;
    }

    @Override
    public String getIdentifier() {
        return "sobel";
    }

    @Override
    public String getFullName() {
        return "sobel__" + kernelSize;
    }
}
