package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;

import java.util.Collection;

public class WaterFillingDetector implements ProteinDetector {

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        return null;
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        return null;
    }
}
