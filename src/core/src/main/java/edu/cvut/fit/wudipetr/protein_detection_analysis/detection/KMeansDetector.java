package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

import java.util.*;
import java.util.stream.Collectors;

public class KMeansDetector implements ProteinDetector {

    /**
     * How many centroids (average) are contained in one pixel in each direction.
     * Should be number between 0 and 1 (but not 0).
     * Just count number of all centroids in X or Y direction and divide the number them by number of pixels in the
     * direction. Result should be this number.
     */
    protected static final double CENTROID_DENSITY = .3;

    protected static final int RADIUS = 5;

    List<Centroid> centroids;

    CentroidMap centroidMap;

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        detectCentroids(image);
        return centroids.stream().map(Centroid::getPosition).collect(Collectors.toList());
    }

    private void updateCentroidMap() {
        for (Centroid centroid : centroids) {
            centroidMap.get(centroid.getPosition()).add(centroid);
        }
    }

    private void assignPointsToCentroids(List<Centroid> centroids, Image image) {
        System.out.println("Assign");
        assert centroids.size() > 0;
        centroids.forEach(Centroid::clearAssigned);
        int diameter = 2 * RADIUS;
        for (int y = 0; y < image.height(); ++y) {
            if (y % 100 == 0) System.out.println("y=" + y);
            for (int x = 0; x < image.width(); ++x) {
                if (image.get(x, y) < 5) continue;
                Centroid closestCentroid = null;
                int closestDistance = Integer.MAX_VALUE;
                for (int yy = Integer.max(0, y - diameter); yy <= Integer.min(image.height() - 1, y + diameter); yy++) {
                    for (int xx = Integer.max(0, x - diameter); xx <= Integer.min(image.width() - 1, x + diameter); xx++) {
                        List<Centroid> cs = centroidMap.get(xx, yy);
                        for (Centroid centroid : cs) {
                            int distX = centroid.getPosition().getX() - x;
                            int distY = centroid.getPosition().getY() - y;
                            int distance = distX * distX + distY * distY;
                            if (distance < closestDistance) {
                                closestDistance = distance;
                                closestCentroid = centroid;
                            }
                        }
                    }
                }
                ImagePoint point = new ImagePoint(x, y);
                assert closestCentroid != null;
                closestCentroid.assign(point);
            }
        }
        System.out.println("Assigned");
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        detectCentroids(image);
        return centroids.stream().map(c -> c.getMask(image.width(), image.height())).collect(Collectors.toList());
    }

    private Collection<Centroid> detectCentroids(Image image) {
        List<Centroid> centroids = generateCentroids(image.width(), image.height());
        for (int i = 0; i < 1; ++i) {
            updateCentroidMap();
            assignPointsToCentroids(centroids, image);
            centroids.forEach(centroid -> centroid.update(image));
        }
        return centroids;
    }

    /**
     * Generates centroids placed on initial location
     * @param width
     * @param height
     * @return
     */
    private List<Centroid> generateCentroids(int width, int height) {
        /*
        int stepX = (int) (width * CENTROID_DENSITY);
        int stepY = (int) (height * CENTROID_DENSITY);
        */
        int stepX = RADIUS;
        int stepY = RADIUS;
        centroids = new ArrayList<>(width / stepX * height / stepY);
        centroidMap = new CentroidMap(RADIUS);
        for (int y = 0; y < height; y += stepY) {
            for (int x = 0; x < width; x += stepX) {
                centroids.add(new Centroid(x, y));
            }
        }
        return centroids;
    }

    protected class Centroid {
        protected ImagePoint position;
        protected List<ImagePoint> assignedPoints = new ArrayList<>();

        public Centroid(int x, int y) {
            position = new ImagePoint(x, y);
        }

        public void assign(ImagePoint point) {
            assignedPoints.add(point);
        }

        public void clearAssigned() {
            assignedPoints.clear();
        }

        /**
         * Updates the centroid's position to be in the center of assigned points
         * //todo write about weighted thing
         */
        public void update(Image image) {
            double x = 0;
            double y = 0;
            double valueSum = 0;
            for (ImagePoint point : assignedPoints) {
                double value = image.get(point);
                x += value * point.getX();
                y += value * point.getY();
                valueSum += value;
            }
            x /= valueSum;
            y /= valueSum;
            position = new ImagePoint((int) x, (int) y);
        }

        public ImagePoint getPosition() {
            return position;
        }

        public Protein getMask(int width, int height) {
            boolean[][] array = new boolean[height][width];
            for (ImagePoint pt : assignedPoints) {
                array[pt.getY()][pt.getX()] = true;
            }
            ProteinMask mask = new ArrayProteinMask(array);
            return new Protein(getPosition(), mask);
        }
    }

    protected class CentroidMap {

        Map<ImagePoint, List<Centroid>> map;
        int diameter;

        public CentroidMap(int radius) {
            map = new HashMap<>();
            diameter = 2 * radius;
        }

        public List<Centroid> get(ImagePoint point) {
            return get(point.getX(), point.getY());
        }

        public List<Centroid> get(int imageX, int imageY) {
            int x = imageX / diameter;
            int y = imageY / diameter;
            ImagePoint mapPoint = new ImagePoint(x, y);
            return map.computeIfAbsent(mapPoint, k -> new LinkedList<>());
        }
    }
}
