package edu.cvut.fit.wudipetr.protein_detection_analysis.detection.evolution;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;

public class Particle {
    protected ImagePoint position;
    protected int radius;
    protected int intensity;

    public Particle(int x, int y, int radius) {
        position = new ImagePoint(x, y);
        this.radius = radius;
        this.intensity = 200;
    }
}
