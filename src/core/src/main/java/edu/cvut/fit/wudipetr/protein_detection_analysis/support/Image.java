package edu.cvut.fit.wudipetr.protein_detection_analysis.support;

import org.opencv.core.Mat;

/**
 * I just wanted to be able to replace OpenCV under the hood and to avoid the complicated pixel value reading
 */
public interface Image {
    /**
     * Return value of the pixel on the image. The image might mave more than one color layer, in that case it returns
     * average value
     * There is no guarantee of boundary check so if you specify invalid coord it may just throw NPE
     * @param x horizontal position of the desired pixel (from left)
     * @param y vertical position of the desired pixel (from top)
     * @return the value
     */
    double get(int x, int y);

    /**
     * Return value of the pixel on the image. The image might mave more than one color layer, in that case it returns
     * average value
     * There is no guarantee of boundary check so if you specify invalid coord it may just throw NPE
     * @param point position of the desired pixel
     * @return the value
     */
    double get(ImagePoint point);

    /**
     * Sets value of an pixel
     * There is no guarantee of boundary check so if you specify invalid coord it may just throw NPE
     * @param x horizontal position of the pixel
     * @param y vertical position of the pixel
     * @param value the value to be set
     */
    void set(int x, int y, double value);

    /**
     * Sets value of an pixel
     * There is no guarantee of boundary check so if you specify invalid coord it may just throw NPE
     * @param point position of the pixel
     * @param value the value to be set
     */
    void set(ImagePoint point, double value);

    /**
     * @return width of the image (number of columns)
     */
    int width();

    /**
     * @return height of the image (number of rows)
     */
    int height();

    /**
     * Deprecated method at the time of class creation. I'm such an engineer
     * Replace by get and set methods
     */
    @Deprecated
    int[][] getArray();

    /**
     * @return the image as OpenCV Mat
     */
    Mat opencv();

    /**
     * @return deep copy of the image
     */
    Image copy();

    Image crop(ImagePoint start, ImagePoint end);
}
