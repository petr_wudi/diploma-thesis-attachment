package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class FlatfieldFilter extends AbstractFlatfieldOperator {
    private static final double MAX_SIGMA_MULTIPLIER_DEFAULT = 3;
    private static final double REFERENCE_COEF_DEFAULT = 0.5;

    private final double maxSigmaMultiplier;
    private FlatfieldFunction genericFunction = null;

    public FlatfieldFilter() {
        this(MAX_SIGMA_MULTIPLIER_DEFAULT);
    }

    public FlatfieldFilter(double maxSigmaMultiplier) {
        this(maxSigmaMultiplier, REFERENCE_COEF_DEFAULT);
    }

    public FlatfieldFilter(double maxSigmaMultiplier, double referenceCoef) {
        super(referenceCoef);
        this.maxSigmaMultiplier = maxSigmaMultiplier;
    }

    @Override
    public Image apply(Image image) {
        Mat binaryMask = getBinaryMask(image);
        Mat cvSource = image.opencv();
        Mat target = Mat.zeros(image.height(), image.width(), cvSource.type());
        cvSource.copyTo(target, binaryMask);
        return new MatImage(target);
    }

    public Mat getBinaryMask(Image image) {
        Mat binaryMask = new Mat(image.height(), image.width(), CvType.CV_8U);
        FlatfieldFunction function = genericFunction;
        if (function == null) {
            System.out.println("Compute the flatfield function");
            function = computeFlatfield(image);
        }
        double sigma = function.getSigma();
        for (int y = 0; y < image.height(); ++y) {
            for (int x = 0; x < image.width(); ++x) {
                double value = image.get(x, y);
                double ffValue = function.predict(x, y);
                boolean isInMask = value > ffValue + maxSigmaMultiplier * sigma;
                binaryMask.put(y, x, isInMask ? 1.0 : 0.0);
            }
        }
        return binaryMask;
    }

    @Override
    public String getIdentifier() {
        return "flatfield";
    }

    @Override
    public String getFullName() {
        return getIdentifier() + "__" + maxSigmaMultiplier + "_" + referenceCoef;
    }
}
