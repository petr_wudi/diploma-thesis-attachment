package edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration;

import java.util.List;

import edu.cvut.fit.wudipetr.protein_detection_analysis.detection.ProteinDetector;
import edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing.Preprocessing;
import edu.cvut.fit.wudipetr.protein_detection_analysis.validation.Validator;

public class ProgramConfiguration {
    private List<Preprocessing> preprocessing;
    private ProteinDetector detector;
    private List<Validator> validators;
    private String methodName;
    private boolean shouldLogPoints = true;
    private boolean shouldLogAreas = true;
    private boolean shouldLogAreasBlank = true;

    public ProgramConfiguration(List<Preprocessing> preprocessing, ProteinDetector detector,
            List<Validator> validators, String methodName) {
        this.preprocessing = preprocessing;
        this.detector = detector;
        this.validators = validators;
        this.methodName = methodName;
    }

    public List<Preprocessing> getPreprocessing() {
        return preprocessing;
    }

    public ProteinDetector getDetector() {
        return detector;
    }

    public List<Validator> getValidators() {
        return validators;
    }

    public String getMethodName() {
        if (methodName == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder(methodName);
        /*
        for (Preprocessing preprocessing : getPreprocessing()) {
            stringBuilder.append("__p-");
            stringBuilder.append(preprocessing.getIdentifier());
        }
        for (Validator validator : getValidators()) {
            stringBuilder.append("__v-");
            stringBuilder.append(validator.getIdentifier());
        }
         */
        return stringBuilder.toString();
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public boolean shouldLogPoints() {
        return shouldLogPoints;
    }

    public boolean shouldLogAreas() {
        return shouldLogAreas;
    }

    public boolean shouldLogAreasBlank() {
        return shouldLogAreasBlank;
    }

    public void setLogging(boolean shouldLogPoints, boolean shouldLogAreas, boolean shouldLogAreasBlank) {
        this.shouldLogPoints = shouldLogPoints;
        this.shouldLogAreas = shouldLogAreas;
        this.shouldLogAreasBlank = shouldLogAreasBlank;
    }
}
