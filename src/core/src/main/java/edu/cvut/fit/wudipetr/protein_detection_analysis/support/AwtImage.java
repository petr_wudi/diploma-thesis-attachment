package edu.cvut.fit.wudipetr.protein_detection_analysis.support;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.awt.*;
import java.awt.image.BufferedImage;

public class AwtImage implements Image {

    private final BufferedImage awtImage;

    public AwtImage(BufferedImage awtImage) {
        this.awtImage = awtImage;
    }

    @Override
    public double get(int x, int y) {
        Color color = new Color(awtImage.getRGB(x, y));
        return (color.getRed() + color.getGreen() + color.getBlue()) / 3;
    }

    @Override
    public double get(ImagePoint point) {
        return get(point.getX(), point.getY());
    }

    @Override
    public void set(int x, int y, double value) {
        awtImage.setRGB(x, y, (int) value); // todo
    }

    @Override
    public void set(ImagePoint point, double value) {
        set(point.getX(), point.getY(), value);
    }

    @Override
    public int width() {
        return awtImage.getWidth();
    }

    @Override
    public int height() {
        return awtImage.getHeight();
    }

    @Override
    public int[][] getArray() {
        int width = width();
        int height = height();
        int[][] arr = new int[height][width];
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                arr[y][x] = (int) get(x, y);
            }
        }
        return arr;
    }

    @Override
    public Mat opencv() {
        int type = CvType.CV_16UC3;
        Mat mat = new Mat(height(), width(), type);
        for (int y = 0; y < width(); ++y) {
            for (int x = 0; x < height(); ++x) {
                Color color = new Color(awtImage.getRGB(x, y));
                mat.put(y, x, color.getBlue(), color.getGreen(), color.getRed());
            }
        }
        return mat;
    }

    @Override
    public Image copy() {
        return new MatImage(opencv()); // todo fuj
    }

    @Override
    public Image crop(ImagePoint start, ImagePoint end) {
        return null; // todo
    }
}
