package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Mat;

public class DoubleSobelOperator extends SobelOperator {

    public DoubleSobelOperator(int kernelSize) {
        super(kernelSize);
    }

    @Override
    public Image apply(Image image) {
        Mat source = image.opencv();
        Mat derivation1 = apply(source);
        Mat derivation2 = apply(derivation1);
        return new MatImage(derivation2);
    }

    @Override
    public String getIdentifier() {
        return "sobel";
    }

    @Override
    public String getFullName() {
        return "sobel__" + kernelSize;
    }
}
