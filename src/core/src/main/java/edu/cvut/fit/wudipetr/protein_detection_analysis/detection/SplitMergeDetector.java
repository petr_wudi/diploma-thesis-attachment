package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Detector using split & merge segmentation algorithm.
 */
public class SplitMergeDetector implements ProteinDetector {
    private static final int SPLIT_LIMIT = 20;
    private static final double MAX_DIFFERENCE_DEFAULT = 50;
    private static final double MIN_THRESHOLD_DEFAULT = 100;

    private final double maxDifference;
    private final double minThreshold;

    public SplitMergeDetector() {
        this.maxDifference = MAX_DIFFERENCE_DEFAULT;
        this.minThreshold = MIN_THRESHOLD_DEFAULT;
    }

    public SplitMergeDetector(double maxDifference, double minThreshold) {
        this.maxDifference = maxDifference;
        this.minThreshold = minThreshold;
    }

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        return detectMasks(image).stream().map(Protein::getPosition).collect(Collectors.toList());
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        if (image.width() != image.height()) {
            System.out.println("Warning: image is not square, it will be trim.");
        }
        // todo split limit - make it impossible for areas not to be squares
        Function<Area, Boolean> condition = area -> {
            double minValue = Double.POSITIVE_INFINITY;
            double maxValue = Double.NEGATIVE_INFINITY;
            Iterator<Double> pixelIterator = area.iterator();
            while (pixelIterator.hasNext()) {
                double pixel = pixelIterator.next();
                if (pixel < minValue) {
                    minValue = pixel;
                }
                if (pixel > maxValue) {
                    maxValue = pixel;
                }
            }
            double difference = maxValue - minValue;
            return difference < maxDifference; // todo variance (or another moments) instead of max difference
        };
        Area rootArea = new Area(image);
        trySplit(rootArea, 0, condition);
        BiPredicate<AreaGroup, AreaGroup> mergeCondition = (a1, a2) -> {
            double minValue = Double.POSITIVE_INFINITY;
            double maxValue = Double.NEGATIVE_INFINITY;
            Iterator<Double> pixelIterator;
            for (Area a : a1.getAreas()) {
                pixelIterator = a.iterator();
                while (pixelIterator.hasNext()) {
                    double pixel = pixelIterator.next();
                    if (pixel < minValue) {
                        minValue = pixel;
                    }
                    if (pixel > maxValue) {
                        maxValue = pixel;
                    }
                }
            }
            for (Area a : a2.getAreas()) {
                pixelIterator = a.iterator();
                while (pixelIterator.hasNext()) {
                    double pixel = pixelIterator.next();
                    if (pixel < minValue) {
                        minValue = pixel;
                    }
                    if (pixel > maxValue) {
                        maxValue = pixel;
                    }
                }
            }
            double difference = maxValue - minValue;
            return difference < maxDifference;
        };
        Collection<AreaGroup> areasMerged = tryMerge(image, rootArea, mergeCondition);
        // todo merge
        Predicate<AreaGroup> collectCondition = areaGroup -> {
            double sum = 0;
            int numOfPixels = 0;
            for (Area area : areaGroup.getAreas()) {
                numOfPixels += area.getWidth() * area.getHeight();
                Iterator<Double> pixelIterator = area.iterator();
                while (pixelIterator.hasNext()) {
                    double pixel = pixelIterator.next();
                    sum += pixel;
                }
            }
            double avgValue = sum / numOfPixels;
            return avgValue > minThreshold;
        };
        return collectAreas(areasMerged, collectCondition, image);
    }

    private void trySplit(Area area, int i, Function<Area, Boolean> condition) {
        if (i >= SPLIT_LIMIT) {
            return;
        }
        if (condition.apply(area)) {
            // merge
        } else {
            area.split();
            for (Area subArea : area.getSubAreas()) {
                trySplit(subArea, i + 1, condition);
            }
        }
    }

    private Collection<AreaGroup> tryMerge(Image image, Area rootArea, BiPredicate<AreaGroup, AreaGroup> mergeCondition) {
        System.out.println("Image " + image.width() + " x " + image.height());
        AreaGroup[][] array = new AreaGroup[image.height()][image.width()];
        fillArray(array, rootArea);
        for (int y = 0; y < image.height(); ++y) {
            AreaGroup prevArea = array[y][0];
            for (int x = 1; x < image.width(); ++x) {
                AreaGroup nextArea = array[y][x];
                if (prevArea != nextArea
                        && y > 0 && prevArea != array[y-1][x-1] && nextArea != array[y-1][x]
                        && mergeCondition.test(prevArea, nextArea)) {
                    merge(prevArea, nextArea, array);
                }
            }
        }
        Set<AreaGroup> areaGroups = new HashSet<>();
        for (int y = 0; y < image.height(); ++y) {
            for (int x = 0; x < image.width(); ++x) {
                AreaGroup area = array[y][x];
                areaGroups.add(area);
            }
        }
        return areaGroups;
    }

    private void fillArray(AreaGroup[][] array, Area area) {
        List<Area> subAreas = area.getSubAreas();
        if (subAreas.isEmpty()) {
            ImagePoint startPoint = area.getStart();
            int startY = startPoint.getY();
            int endY = startY + area.getHeight();
            for (int y = startY; y < endY; ++y) {
                int startX = startPoint.getX();
                int endX = startX + area.getWidth();
                for (int x = startX; x < endX; ++x) {
                    array[y][x] = area;
                }
            }
        } else {
            for (Area subArea : subAreas) {
                fillArray(array, subArea);
            }
        }
    }

    private void fillArray(AreaGroup[][] array, Area area, AreaGroup areaValue) {
        List<Area> subAreas = area.getSubAreas();
        if (subAreas.isEmpty()) {
            ImagePoint startPoint = area.getStart();
            for (int y = startPoint.getY(); y < startPoint.getY() + area.getHeight(); ++y) {
                for (int x = startPoint.getX(); x < startPoint.getX() + area.getWidth(); ++x) {
                    array[y][x] = areaValue;
                }
            }
        } else {
            for (Area subArea : subAreas) {
                fillArray(array, subArea);
            }
        }
    }

    private void merge(AreaGroup a1, AreaGroup a2, AreaGroup[][] array) {
        AreaMerge merged = new AreaMerge();
        for (Area a : a1.getAreas()) {
            merged.add(a);
            fillArray(array, a, merged);
        }
        for (Area a : a2.getAreas()) {
            merged.add(a);
            fillArray(array, a, merged);
        }
    }

    private Collection<Protein> collectAreas(Collection<AreaGroup> areasMerged, Predicate<AreaGroup> collectCondition, Image image) {
        List<Protein> collection = new ArrayList<>(areasMerged.size());
        for (AreaGroup areaGroup : areasMerged) {
            if (!collectCondition.test(areaGroup)) {
                continue;
            }
            boolean[][] array = new boolean[image.height()][image.width()];
            long sumX = 0;
            long sumY = 0;
            long numOfPoints = 0;
            for (Area area : areaGroup.getAreas()) {
                ImagePoint startPoint = area.getStart();
                for (int y = startPoint.getY(); y < startPoint.getY() + area.getHeight(); ++y) {
                    for (int x = startPoint.getX(); x < startPoint.getX() + area.getWidth(); ++x) {
                        array[y][x] = true;
                        sumX += x;
                        sumY += y;
                        ++numOfPoints;
                    }
                }
            }
            int x = (int)(sumX / numOfPoints);
            int y = (int)(sumY / numOfPoints);
            ImagePoint position = new ImagePoint(x, y);
            ProteinMask mask = new ArrayProteinMask(array);
            Protein protein = new Protein(position, mask);
            collection.add(protein);
        }
        return collection;
    }

    private void collectAreas(Area area, Function<Area, Boolean> collectCondition, List<Protein> collection) {
        if (area.getSubAreas().isEmpty()) {
            if (collectCondition.apply(area)) {
                ImagePoint position = area.getPosition();
                ProteinMask mask = area.getMask();
                Protein protein = new Protein(position, mask);
                collection.add(protein);
            }
        } else {
            for (Area subArea : area.getSubAreas()) {
                collectAreas(subArea, collectCondition, collection);
            }
        }
    }

    private interface AreaGroup {
        List<Area> getAreas();
    }

    static private class Area implements AreaGroup {
        private final Image image;
        final ImagePoint start;
        final ImagePoint end; // exclude
        private List<Area> subAreas = Collections.emptyList();
        private List<Area> group = new ArrayList<>();

        Area(Image image) {
            this.image = image;
            int size = Integer.min(image.width(), image.height()); // just trim to make it square
            start = new ImagePoint(0, 0);
            end = new ImagePoint(size, size);
        }

        private Area(Image image, int startX, int startY, int endX, int endY) {
            this.image = image;
            start = new ImagePoint(startX, startY);
            end = new ImagePoint(endX, endY);
        }

        void split() {
            subAreas = new ArrayList<>(4);
            int sizeX = getWidth();
            int stepX = sizeX / 2;
            int sizeY = getHeight();
            int stepY = sizeY / 2;
            int[] xStarts = {start.getX(), start.getX() + stepX};
            int[] xEnds = {start.getX() + stepX, end.getX()};
            int[] yStarts = {start.getY(), start.getY() + stepY};
            int[] yEnds = {start.getY() + stepY, end.getY()};
            for (int y = 0; y < 2; ++y) {
                for (int x = 0; x < 2; ++x) {
                    int xStart = xStarts[x];
                    int yStart = yStarts[y];
                    int xEnd = xEnds[x];
                    int yEnd = yEnds[y];
                    Area area = new Area(image, xStart, yStart, xEnd, yEnd);
                    subAreas.add(area);
                }
            }
        }

        List<Area> getSubAreas() {
            return subAreas;
        }

        private Iterator<Double> iterator() {
            return new Iterator<Double>() {
                private int x = start.getX();
                private int y = start.getY();

                @Override
                public boolean hasNext() {
                    return y < end.getY();
                }

                @Override
                public Double next() {
                    double pixel = image.get(x, y);
                    if (++x >= end.getX()) {
                        x = 0;
                        ++y;
                    }
                    return pixel;
                }
            };
        }

        private int getWidth() {
            return end.getX() - start.getX();
        }

        private int getHeight() {
            return end.getY() - start.getY();
        }

        public ImagePoint getStart() {
            return start;
        }

        public ImagePoint getPosition() {
            int x = start.getX() + getWidth() / 2;
            int y = start.getY() + getHeight() / 2;
            return new ImagePoint(x, y);
        }

        public ProteinMask getMask() {
            return new ArrayProteinMask(start.getX(), start.getY(), end.getX(), end.getY());
        }

        private List<Area> getGroup() {
            return group;
        }

        private void setGroup(List<Area> group) {
            this.group = group;
        }

        @Override
        public List<Area> getAreas() {
            return Collections.singletonList(this);
        }
    }

    static private class AreaMerge implements AreaGroup {
        private List<Area> areas = new ArrayList<>();

        public void add(Area a) {
            areas.add(a);
        }

        @Override
        public List<Area> getAreas() {
            return areas;
        }
    }
}

