package edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask;

import org.opencv.core.*;

import java.util.Random;
import java.util.function.BiConsumer;

/**
 * Stores information where one protein lies using binary array.
 */
public class ArrayProteinMask implements ProteinMask {
    private static final Random RANDOM = new Random();

    private int xStart;
    private int yStart;
    private boolean[][] mask;

    /**
     * Creates new mask using binary array. The mask covers only those points, which are true in the array.
     * @param array binary array defining the mask
     */
    public ArrayProteinMask(boolean[][] array) {
        int minY = Integer.MAX_VALUE;
        int maxY = Integer.MIN_VALUE;
        int minX = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        for (int y = 0; y < array.length; ++y) {
            for (int x = 0; x < array[y].length; ++x) {
                if (array[y][x]) {
                    if (x > maxX) {
                        maxX = x;
                    }
                    if (y > maxY) {
                        maxY = y;
                    }
                    if (x < minX) {
                        minX = x;
                    }
                    if (y < minY) {
                        minY = y;
                    }
                }
            }
        }
        xStart = minX;
        yStart = minY;
        if (maxX < minX && maxY < minY) return;
        mask = new boolean[maxY - minY + 1][maxX - minX + 1];
        for (int y = minY; y <= maxY; ++y) {
            int length = maxX - minX + 1;
            System.arraycopy(array[y], minX, mask[y - minY], 0, length);
        }
    }

    /**
     * Crates protein mask covering rectangle defined by the two points
     * @param xStart horizontal coordinate of the left top (lower values) corner of the rectangle
     * @param yStart vertical coordinate of the left top (lower values) corner of the rectangle
     * @param xEnd horizontal coordinate of the right bottom (higher values) corner of the rectangle
     * @param yEnd vertical coordinate of the right bottom (higher values) corner of the rectangle
     */
    public ArrayProteinMask(int xStart, int yStart, int xEnd, int yEnd) {
        this.xStart = xStart;
        this.yStart = yStart;
        mask = new boolean[yEnd - yStart][xEnd - xStart];
        for (int y = 0; y < yEnd - yStart; ++y) {
            for (int x = 0; x < xEnd - xStart; ++x) {
                mask[y][x] = true;
            }
        }
    }

    /**
     * Creates the mask using PrimitiveMask object. This mask covers the same values as the input one.
     * @param primitiveMask source of information for this mask
     */
    public ArrayProteinMask(PrimitiveMask primitiveMask) {
        xStart = primitiveMask.getMinX();
        yStart = primitiveMask.getMinY();
        mask = primitiveMask.getMinArray();
    }

    @Override
    public void setValue(int x, int y, boolean isPresent) {
        // todo implement
    }

    @Override
    public Mat getMat() {
        int height = mask.length;
        assert mask.length > 0;
        int width = mask[0].length;
        Mat mat = Mat.zeros(height, width, CvType.CV_8U);
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (mask[y][x]) {
                    mat.put(y, x, 255);
                }
            }
        }
        return mat;
    }

    @Override
    public Mat getMat(int width, int height) {
        Mat mat = new Mat(height, width, CvType.CV_8U);
        assert mask.length > 0;
        int yEnd = Integer.min(height, yStart + mask.length);
        int xEnd = Integer.min(width, xStart + mask[0].length);
        for (int y = yStart; y < yEnd; ++y) {
            for (int x = xStart; x < xEnd; ++x) {
                if (mask[y - yStart][x - xStart]) {
                    mat.put(y, x, 255);
                }
            }
        }
        return mat;
    }

    @Override
    public int getSize() {
        int size = 0;
        for (boolean[] row : mask) {
            for (boolean value : row) {
                if (value) {
                    size++;
                }
            }
        }
        return size;
    }

    @Override
    public void drawFilling(Mat image) {
        int blue = RANDOM.nextInt(156) + 50;
        int green = RANDOM.nextInt(156) + 50;
        double[] colors = {blue, green, 0};
        for (int y = 0; y < image.height() - yStart && y < mask.length; ++y) {
            for (int x = 0; x < image.width() - xStart && x < mask[0].length; ++x) {
                if (mask[y][x]) {
                    image.put(y + yStart, x + xStart, colors);
                }
            }
        }
    }

    @Override
    public void apply(BiConsumer<Integer, Integer> consumer) {
        for (int y = 0; y < mask.length; ++y) {
            for (int x = 0; x < mask[y].length; ++x) {
                if (mask[y][x]) {
                    consumer.accept(x + xStart, y + yStart);
                }
            }
        }
    }
}
