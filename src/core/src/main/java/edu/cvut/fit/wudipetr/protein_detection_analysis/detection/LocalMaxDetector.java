package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.PrimitiveMask;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ProteinMask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class LocalMaxDetector implements ProteinDetector {
    private static final int DEFAULT_WIN_SIZE = 3;
    private static final int DEFAULT_THRESHOLD = 20;
    private static final double EPS = 0.0001;

    protected final int valueThreshold;
    protected final int winSize;
    protected final int maskCenter;
    private final boolean[][] mask;

    public LocalMaxDetector() {
        this(DEFAULT_THRESHOLD, DEFAULT_WIN_SIZE);
    }

    public LocalMaxDetector(int threshold) {
        this(threshold, DEFAULT_WIN_SIZE);
    }

    public LocalMaxDetector(int threshold, int winSize) {
        this.valueThreshold = threshold;
        this.winSize = winSize;
        if (winSize <= 0) {
            throw new RuntimeException("Window size must be higher than zero, not " + winSize);
        }
        if (winSize % 2 == 0) {
            throw new RuntimeException("Window size must be odd");
        }
        maskCenter = winSize / 2;
        mask = new boolean[winSize][winSize];
        for (int y = 0; y < winSize; ++y) {
            for (int x = 0; x < winSize; ++x) {
                int xDistFromCenter = maskCenter - x;
                int yDistFromCenter = maskCenter - y;
                int sqrDist = yDistFromCenter * yDistFromCenter + xDistFromCenter * xDistFromCenter;
                mask[y][x] = sqrDist <= maskCenter * maskCenter;
            }
        }
    }

    @Override
    public Collection<ImagePoint> detectCenters(Image originalImage) {
        Image image = originalImage.copy(); // todo delete
        // todo not duplicate
        int height = image.height();
        int width = image.width();
        int[][] labels = new int[height][width];
        int labelMax = 1;
        List<ImagePoint> proteins = new ArrayList<>();
        PrimitiveMask isIn = new PrimitiveMask(width, height);
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (labels[y][x] == 0 && isLocalMaxWin(image, labels, x, y)) {
                    stamp(x, y, labels, labelMax++, isIn, image);
                    ImagePoint center = isIn.getCenter();
                    isIn.clear();
                    proteins.add(center);
                }
            }
        }
        return proteins;
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        int height = image.height();
        int width = image.width();
        int[][] labels = new int[height][width];
        int labelMax = 1;
        List<Protein> proteins = new ArrayList<>();
        PrimitiveMask isIn = new PrimitiveMask(width, height);
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (labels[y][x] == 0 && isLocalMax(image, x, y)) {
                    isIn.clear();
                    stamp(x, y, labels, labelMax, isIn, image);
                    ProteinMask mask = new ArrayProteinMask(isIn);
                    ImagePoint center = isIn.getCenter();
                    Protein p = new Protein(center, mask);
                    proteins.add(p);
                }
            }
        }
        return proteins;
    }

    private void stamp(int x, int y, int[][] labels, int label, PrimitiveMask isIn, Image image) {
        int minX = Integer.max(0, x - maskCenter);
        int minY = Integer.max(0, y - maskCenter);
        int maxX = Integer.min(image.width() - 1, x + maskCenter);
        int maxY = Integer.min(image.height() - 1, y + maskCenter);
        int maskY = minY - y + maskCenter;
        for (int imageY = minY; imageY <= maxY; ++imageY) {
            int maskX = minX - x + maskCenter;
            for (int imageX = minX; imageX <= maxX; ++imageX) {
                if (mask[maskY][maskX] && labels[imageY][imageX] == 0 && image.get(imageX, imageY) > valueThreshold) {
                    labels[imageY][imageX] = label;
                    isIn.set(imageX, imageY, true);
                }
                ++maskX;
            }
            ++maskY;
        }
    }

    private boolean isLocalMaxWin(Image image, int[][] labels, int x, int y) {
        double refPoint = image.get(x, y);
        if (refPoint < valueThreshold) {
            return false;
        }
        int minX = Integer.max(0, x - maskCenter);
        int minY = Integer.max(0, y - maskCenter);
        int maxX = Integer.min(image.width() - 1, x + maskCenter);
        int maxY = Integer.min(image.height() - 1, y + maskCenter);
        int maskY = 0;
        for (int imageY = minY; imageY <= maxY; ++imageY) {
            int maskX = 0;
            for (int imageX = minX; imageX <= maxX; ++imageX) {
                double imageValue = image.get(imageX, imageY) * (mask[maskY][maskX] ? 1 : 0) * (1 - labels[imageY][imageX]);
                if (imageValue > refPoint) {
                    return false;
                }
                ++maskX;
            }
            ++maskY;
        }
        return true;
    }

    protected boolean isLocalMax(Image image, int x, int y) {
        double center = image.get(x, y);
        return center > valueThreshold
                && center + EPS >= getValue(image, x+1, y)
                && center + EPS >= getValue(image, x-1, y)
                && center + EPS >= getValue(image, x, y+1)
                && center + EPS >= getValue(image, x, y-1);
    }

    private double getValue(Image image, int x, int y) {
        if (x < 0 || y < 0 || x >= image.width() || y >= image.height()) return -1.0;
        return image.get(x, y);
    }

    protected boolean isSame(Image image, int x, int y, int xx, int yy) {
        double difference = image.get(x, y) - image.get(xx, yy);
        return difference <= EPS && difference >= -EPS;
    }
}
