package edu.cvut.fit.wudipetr.protein_detection_analysis.validation;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;

import java.util.Collection;
import java.util.stream.Collectors;

public class SizeValidator implements Validator {
    private final int minArea;

    public SizeValidator(int minArea) {
        this.minArea = minArea;
    }

    @Override
    public Collection<Protein> filter(Collection<Protein> potentialProteins, Image image) {
        return potentialProteins.stream()
                .filter(protein -> protein.getMask().getSize() > minArea)
                .collect(Collectors.toList());
    }

    @Override
    public String getIdentifier() {
        return "size";
    }
}
