package edu.cvut.fit.wudipetr.protein_detection_analysis.validation;

import java.util.Collection;
import java.util.stream.Collectors;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;

/**
 * Suppresses values lower than minimum or higher than maximum.
 */
public class MinMaxValidator implements Validator {

    private double minimum;
    private double maximum;

    public MinMaxValidator(double minimum, double maximum) {
        this.minimum = minimum;
        this.maximum = maximum;
    }

    @Override
    public Collection<Protein> filter(Collection<Protein> potentialProteins, Image image) {
        return potentialProteins.stream()
                .filter(protein -> {
                    ImagePoint position = protein.getPosition();
                    double value = image.get(position);
                    return value >= minimum && value <= maximum;
                })
                .collect(Collectors.toList());
    }

    @Override
    public String getIdentifier() {
        return "minmax";
    }

}
