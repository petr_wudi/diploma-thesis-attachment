package edu.cvut.fit.wudipetr.protein_detection_analysis.support;

import java.io.Serializable;

/**
 * Location of a point in an image, represented by X and Y coords
 */
public class ImagePoint implements Comparable<ImagePoint>, Serializable {
    protected int x;
    protected int y;

    public ImagePoint(int x, int y) {
        assert x >= 0;
        assert y >= 0;
        this.x = x;
        this.y = y;
    }

    /**
     * @return X coordinate of the point in the image (from left)
     */
    public int getX() {
        return x;
    }

    /**
     * @return Y coordinate of the point in the image (from top)
     */
    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + "]";
    }

    @Override
    public int compareTo(ImagePoint buddy) {
        int xComp = Integer.compare(buddy.x, x);
        return xComp != 0 ? xComp : Integer.compare(buddy.y, y);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ImagePoint) {
            ImagePoint buddy = (ImagePoint) obj;
            return buddy.x == x && buddy.y == y;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (x << (Integer.SIZE / 2)) + y;
    }
}
