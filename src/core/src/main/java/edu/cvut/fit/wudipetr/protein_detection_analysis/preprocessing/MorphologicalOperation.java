package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

public abstract class MorphologicalOperation implements Preprocessing {
    private final int operationType;
    int kernelDiameter;
    double multiplier;

    public MorphologicalOperation(int operationType, int kernelDiameter, double multiplier) {
        this.operationType = operationType;
        this.kernelDiameter = kernelDiameter;
        if (kernelDiameter % 2 == 0) {
            throw new IllegalArgumentException("Kernel diameter must be an odd number");
        }
        if (kernelDiameter <= 0) {
            throw new IllegalArgumentException("Kernel diameter must be a positive number");
        }
        this.multiplier = multiplier;
        if (multiplier > 1) {
            throw new IllegalArgumentException("Multiplier can't be higher than 1");
        }
        if (multiplier < -1) {
            throw new IllegalArgumentException("Multiplier can't be lower than -1");
        }
    }

    protected Mat negative(Mat imageMat) {
        Mat dst = Mat.zeros(imageMat.size(), imageMat.type());
        Core.add(dst, new Scalar(255, 255, 255), dst);
        Core.subtract(dst, imageMat, dst);
        return dst;
    }

    protected Mat applyOperation(Mat imageMat, int operationType) {
        Mat dst = new Mat();
        int kernelRadius = kernelDiameter / 2;
        Size size = new Size(kernelDiameter, kernelDiameter);
        Point anchor = new Point(kernelRadius, kernelRadius);
        Mat kernel = Imgproc.getStructuringElement(Imgproc.CV_SHAPE_ELLIPSE, size, anchor);
        Imgproc.morphologyEx(imageMat, dst, operationType, kernel);
        double max0 = Double.NEGATIVE_INFINITY;
        double max1 = Double.NEGATIVE_INFINITY;
        double max2 = Double.NEGATIVE_INFINITY;
        for (int y = 0; y < dst.height(); ++y) {
            for (int x = 0; x < dst.width(); ++x) {
                double[] valuesDst = dst.get(y, x);
                double[] valuesSrc = imageMat.get(y, x);
                valuesDst[0] = Double.min(Double.max(0,  (1 - multiplier) * valuesSrc[0] + multiplier * valuesDst[0]), 255);
                valuesDst[1] = Double.min(Double.max(0, (1 - multiplier) * valuesSrc[1] + multiplier * valuesDst[1]), 255);
                valuesDst[2] = Double.min(Double.max(0, (1 - multiplier) * valuesSrc[2] + multiplier * valuesDst[2]), 255);
                if (valuesDst[0] > max0) {
                    max0 = valuesDst[0];
                }
                if (valuesDst[1] > max1) {
                    max1 = valuesDst[1];
                }
                if (valuesDst[2] > max2) {
                    max2 = valuesDst[2];
                }

                dst.put(y, x, valuesDst);
            }
        }
        return dst;
    }

    @Override
    public String getFullName() {
        return getIdentifier() + "__" + kernelDiameter + "_" + multiplier;
    }
}
