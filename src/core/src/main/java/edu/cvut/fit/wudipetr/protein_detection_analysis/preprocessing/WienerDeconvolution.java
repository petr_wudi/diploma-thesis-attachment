package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;


import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class WienerDeconvolution implements Preprocessing {
    private static final double VARIANCE_DEFAULT = 10.0;

    private double variance;
    private double snr = 0.95;

    public WienerDeconvolution() {
        this(VARIANCE_DEFAULT);
    }

    public WienerDeconvolution(double variance) {
        this.variance = variance;
    }

    public WienerDeconvolution(double variance, double snr) {
        this.variance = variance;
        this.snr = snr;
    }

    @Override
    public Image apply(Image image) {
        Mat inputImageOriginal = image.opencv();
        Mat inputImage = new Mat();
        Imgproc.resize(inputImageOriginal, inputImage, new Size(inputImageOriginal.width() *  2, inputImageOriginal.height() *  2));
        Mat gaussKernelFreq = generateGaussFrequency(inputImage.width(), inputImage.height(), variance);
        List<Mat> imageFreq = dftC3(inputImage);
        List<Mat> imageProcessedFreq = wienerC3(imageFreq, gaussKernelFreq);
        Mat imageProcessed = idft(imageProcessedFreq);
        Imgproc.resize(imageProcessed, imageProcessed, inputImageOriginal.size());
        Mat outputNormalized = normalize(imageProcessed);
        Mat outputInt = new Mat();
        outputNormalized.convertTo(outputInt, CvType.CV_8UC3);
//        Core.rotate(outputInt, outputInt, Core.ROTATE_180);
        return new MatImage(outputInt);
    }

    private Mat generateGaussFrequency(int width, int height, double variance) {
//        double sideLength = (((double) width) + height) / 2;
//        variance = variance * sideLength;
        Mat gauss = generateGauss(width, height, variance);
        Mat zeros = Mat.zeros(gauss.size(), gauss.type());
        Mat complex = new Mat(gauss.size(), CvType.CV_64FC2);
        Core.merge(Arrays.asList(gauss, zeros), complex);

//        Mat complex = Mat.zeros(gauss.size(), CvType.CV_64FC2);


        System.out.println("COMPLEX " + CvType.typeToString(complex.type()));
        Mat complexOut = new Mat();

        File f1 = new File("./AAB.png");
        List<Mat> aaa = new ArrayList<>();
        Core.split(complex, aaa);
        Mat xxx = aaa.get(0);
        Imgcodecs.imwrite(f1.getAbsolutePath(), xxx);

        Core.dft(gauss, complexOut, Core.DFT_COMPLEX_OUTPUT);
        complexOut = rearrange(complexOut);
//        complex = dft(complex);
//        File f1 = new File("./AAA.png");
//        List<Mat> aaa = new ArrayList<>();
//        Core.split(complex, aaa);
//        Mat xxx = aaa.get(0);
//        Core.multiply(xxx, new Scalar(255), xxx);
//        Imgcodecs.imwrite(f1.getAbsolutePath(), xxx);

//        Core.merge(Arrays.asList(gauss, zeros), complex);
        System.out.println("TYPE=" + CvType.typeToString(complexOut.type()));
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                double[] values = complexOut.get(y, x);
                complexOut.put(y, x, Math.abs(values[0]), Math.abs(values[1]));
            }
        }

        Mat channel = new Mat();
        Core.extractChannel(complexOut, channel, 0);
        double maxVal = Core.minMaxLoc(channel).maxVal;
        Core.multiply(complexOut, new Scalar(1/maxVal, 1/maxVal), complexOut);

        return complexOut;
//        return rearrange(complexOut);
//        return complexOut;
    }

    private Mat generateGauss(int width, int height, double variance) {
        double varX = variance;
        double varY = variance;
        double multiplier = 1;
//        double multiplier = 2 * Math.PI * variance * 255;
        Mat gaussMat = new Mat(height, width, CvType.CV_64FC1);
        int centerX = width / 2;
        int centerY = height / 2;
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                int xDiff = x - centerX;
                int yDiff = y - centerY;
                double exponentX = xDiff * xDiff / (2 * varX);
                double exponentY = yDiff * yDiff / (2 * varY);
                double exponent = - exponentX - exponentY;
                double value = multiplier * Math.exp(exponent);
                gaussMat.put(y, x, value);
            }
        }
        return gaussMat;
    }

    private List<Mat> dftC3(Mat input) {
        List<Mat> colorMatsInput = new ArrayList<>(3);
        List<Mat> colorMatsOutput = new ArrayList<>(3);
        Core.split(input, colorMatsInput);
        for (int channel = 0; channel < colorMatsInput.size(); channel++) {
            Mat inputDouble = new Mat();
            colorMatsInput.get(channel).convertTo(inputDouble, CvType.CV_64FC1);
            Mat outputChannelMat = dft(inputDouble);
            colorMatsOutput.add(outputChannelMat);
        }
        return colorMatsOutput;
    }

    private Mat dft(Mat inputDouble) {
        Mat inputZeros = Mat.zeros(inputDouble.size(), inputDouble.type());
        Mat inputComplex = Mat.zeros(inputDouble.size(), CvType.CV_64FC2);
        Core.merge(Arrays.asList(inputDouble, inputZeros), inputComplex);
        Mat output = new Mat();
        Core.dft(inputComplex, output);
//        return output;
        return rearrange(output);
    }

    private Mat idft(List<Mat> input) {
        List<Mat> matsProcessed = input.stream()
                .map(this::idft)
                .collect(Collectors.toList());
        Mat output = new Mat();
        Core.merge(matsProcessed, output);
        return output;
    }

    private Mat idft(Mat input) {
        Mat outputComplex = new Mat();
        Core.idft(input, outputComplex);
        return magnitude(outputComplex);
//        return rearrange(magnitude(outputComplex));
//        return magnitude(outputComplex);
    }

    private List<Mat> wienerC3(List<Mat> inputChannels, Mat kernel) {
        return inputChannels.stream()
                .map(inputChannel -> wiener(inputChannel, kernel))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param input
     * @param kernel
     * @return
     */
    private Mat wiener(Mat input, Mat kernel) {
        Mat kernelAbsolute = magnitude(kernel);
        Mat kernelSquare = new Mat();

        Core.multiply(kernelAbsolute, kernelAbsolute, kernelSquare);
//        Core.pow(kernelAbsolute, 2.0, kernelSquare);


//        Core.multiply(kernelAbsolute, kernelAbsolute, kernelSquare);

//        Core.multiply(kernelSquare, kernelAbsolute, kernelSquare);
//        Core.multiply(kernelSquare, kernelAbsolute, kernelSquare);

        Core.add(kernelSquare, new Scalar(snr), kernelSquare);
//        Core.add(kernelSquare, new Scalar(maxVal * alpha), kernelSquare);
        Mat kernelSquareComplex = new Mat();
        Core.merge(Arrays.asList(kernelSquare, kernelSquare), kernelSquareComplex);
        Mat kernelConjugated = conjugate(kernel);
        Mat g = new Mat();
        Core.divide(kernelConjugated, kernelSquareComplex, g);
        Mat result = new Mat();
        Core.mulSpectrums(input, g, result, Core.FLAGS_NONE);

        File f1 = new File("./AAA.png");
        List<Mat> aaa = new ArrayList<>();
        Core.split(g, aaa);
        Mat xxx = aaa.get(0).clone();
        Core.log(xxx, xxx);
        double maxVal = Core.minMaxLoc(kernelAbsolute).maxVal;
        double maxVal2 = Core.minMaxLoc(xxx).maxVal;
        double minVal2 = Core.minMaxLoc(xxx).minVal;
        System.out.println("MAX VAL " + minVal2 + " | " + maxVal2);
        Core.add(xxx, new Scalar(-minVal2), xxx);
        Core.multiply(xxx, new Scalar(255.0 / (maxVal2 - minVal2)), xxx);
/*
        for (int y = 0; y < xxx.height(); ++y) {
            for (int x = 0; x < xxx.width(); ++x) {
                if (xxx.get(y, x)[0] <= 0.000001) {
                    xxx.put(y, x, 255);
                }
            }
        }*/

        Imgcodecs.imwrite(f1.getAbsolutePath(), xxx);

        return result;
    }

    private Mat multiply(Mat a, Mat b) {
        Mat output = new Mat(a.size(), a.type());
        for (int y = 0; y < a.height(); ++y) {
            for (int x = 0; x < a.width(); ++x) {
                double[] aValues = a.get(y, x);
                double[] bValues = b.get(y, x);
//                output.put(y, x, aValues[0], aValues[1]);
                output.put(y, x, aValues[0] * bValues[0], aValues[1] * bValues[1]);
            }
        }
        return output;
    }

    private Mat matrixMultiply(Mat a, Mat b) {
        Mat bTrans = new Mat();
        Core.transpose(b, bTrans);
        Mat result = new Mat();
        Mat zeroMat = new Mat();
//        Mat zeroMat = Mat.zeros(a.width(), b.width(), a.type());
        System.out.println("A=" + a.size() + ", c=" + a.channels());
        System.out.println("B=" + b.size());
        System.out.println("Btrans=" + bTrans.size());
        Core.gemm(a, b, 1, zeroMat, 0, result, Core.GEMM_2_T);
        return result;
    }

    private Mat conjugate(Mat kernel) {
        List<Mat> kernelChannels = new ArrayList<>(2);
        Core.split(kernel, kernelChannels);
        Mat conjugateMat = new Mat();
        Mat complexInverseMat = new Mat();
        Mat realMat = kernelChannels.get(0);
        Mat complexMat = kernelChannels.get(1);
        Core.multiply(complexMat, new Scalar(-1), complexInverseMat);
        Core.merge(Arrays.asList(realMat, complexInverseMat), conjugateMat);
        return conjugateMat;
    }

    private Mat magnitude(Mat complexMat) {
        List<Mat> channels = new ArrayList<>(2);
        Core.split(complexMat, channels);
        Mat magnitude = new Mat();
        Core.magnitude(channels.get(0), channels.get(1), magnitude);
        return magnitude;
    }

    private Mat rearrange(Mat input) {
        int width = input.width();
        int height = input.height();
        int width1 = width / 2;
        int height1 = height / 2;
        int width2 = width - width1;
        int height2 = height - height1;

        Mat in00 = new Mat(input, new Rect(0, 0, width1, height1));
        Mat in01 = new Mat(input, new Rect(width1, 0, width2, height1));
        Mat in10 = new Mat(input, new Rect(0, height1, width1, height2));
        Mat in11 = new Mat(input, new Rect(width1, height1, width2, height2));

        Mat result = new Mat(input.size(), input.type());

        Mat res00 = new Mat(result, new Rect(0, 0, width2, height2)); // there will be 11
        Mat res01 = new Mat(result, new Rect(width2, 0, width1, height2)); // there will be 10
        Mat res10 = new Mat(result, new Rect(0, height2, width2, height1)); // there will be 01
        Mat res11 = new Mat(result, new Rect(width2, height2, width1, height1)); // there will be 00

        in00.copyTo(res11);
        in01.copyTo(res10);
        in10.copyTo(res01);
        in11.copyTo(res00);
        return result;
    }

    private Mat normalize(Mat input) {
        Mat imageNormalized = new Mat(input.size(), input.type());
        double maxValue = 0;
        for (int y = 0; y < input.height(); y++) {
            for (int x = 0; x < input.width(); x++) {
                double value = input.get(y, x)[0];
                if (value > maxValue) {
                    maxValue = value;
                }
            }
        }
        for (int y = 0; y < input.height(); y++) {
            for (int x = 0; x < input.width(); x++) {
                double value = input.get(y, x)[0];
                value = (int)(value / maxValue * 255);
                imageNormalized.put(y, x, value, value, value);
            }
        }
        return imageNormalized;
    }

    @Override
    public String getIdentifier() {
        return "wiener";
    }

    @Override
    public String getFullName() {
        return getIdentifier() + "__" + variance + "_" + snr;
    }
}
