package edu.cvut.fit.wudipetr.protein_detection_analysis.detection;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.ImagePoint;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask.ArrayProteinMask;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PrewittDetector implements ProteinDetector {
    private final ThresholdDetector thresholdDetector;
    private final int threshold;
    private final double zero = 0.5;

    public PrewittDetector(int threshold) {
        thresholdDetector = new ThresholdDetector(threshold);
        this.threshold = threshold;
    }

    @Override
    public Collection<ImagePoint> detectCenters(Image image) {
        Image derivation = derivation(image);
        Image derivation2 = derivation(derivation);
        return thresholdDetector.detectCenters(derivation2);
    }

    @Override
    public Collection<Protein> detectMasks(Image image) {
        Image derivation = derivation(image);
//        Image derivation2 = derivation(derivation);
        File f1 = new File("./derivation1.png");
        Imgcodecs.imwrite(f1.getAbsolutePath(), derivation.opencv());
//        File f2 = new File("./derivation2.png");
        /*
//        Imgcodecs.imwrite(f2.getAbsolutePath(), derivation2.opencv());
        System.out.println(f1.getAbsolutePath());
//        System.out.println(f2.getAbsolutePath());
         */
        return detectZeros(image, derivation);
//        System.out.println(image);

//        return thresholdDetector.detectMasks(derivation2); // todo jak to vypada
    }

    private List<Protein> detectZeros(Image image, Image derivation) {
        // log row
        File outputFile = new File("ASDF.txt");
        int rowId = derivation.height() / 2;
        try {
            FileWriter fileWriter = new FileWriter(outputFile);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            for (int x = 0; x < derivation.width(); ++x) {
                double val = derivation.get(x, rowId);
                printWriter.print(val);
                printWriter.print(", ");
            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert image.width() == derivation.width();
        assert image.height() == derivation.height();
        List<Protein> proteins = new ArrayList<>();
        double min = Double.MAX_VALUE;
        double minPos = Double.MAX_VALUE;
        for (int y = 0; y < image.height(); ++y) {
            for (int x = 0; x < image.width(); ++x) {
                double derivationValue = derivation.get(x, y);
                if (image.get(x, y) > threshold && derivationValue < min) min = derivationValue;
                if (derivationValue > 0 && derivationValue < minPos) minPos = derivationValue;
                if (derivationValue >= -zero && derivationValue <= zero && image.get(x, y) > threshold) {
                    proteins.add(new Protein(new ImagePoint(x, y), new ArrayProteinMask(x, y, x+1, y+1)));
                }
            }
        }
        System.out.println("FOUND " + proteins.size());
        System.out.println(min + "   x    " + minPos);
        return proteins;
    }

    private Image derivation(Image input) {
        Mat inputMat = input.opencv();
        Mat kernelHorizontal = Mat.zeros(3, 3, CvType.CV_8S);
        kernelHorizontal.put(0, 0, 1);
        kernelHorizontal.put(1, 0, 1);
        kernelHorizontal.put(2, 0, 1);
        kernelHorizontal.put(0, 2, -1);
        kernelHorizontal.put(1, 2, -1);
        kernelHorizontal.put(2, 2, -1);
        Mat outputHorizontal = derivation(inputMat, kernelHorizontal);

        Mat kernelVertical = Mat.zeros(3, 3, CvType.CV_8S);
        kernelVertical.put(0, 0, 1);
        kernelVertical.put(0, 1, 1);
        kernelVertical.put(0, 2, 1);
        kernelVertical.put(2, 0, -1);
        kernelVertical.put(2, 1, -1);
        kernelVertical.put(2, 2, -1);
        Mat outputVertical = derivation(inputMat, kernelVertical);

//        return new MatImage(outputHorizontal);
        return new MatImage(joinDerivations(outputHorizontal, outputVertical));
    }

    private Mat joinDerivations(Mat horizontal, Mat vertical) {
        Mat joined = new Mat(horizontal.size(), horizontal.type());
        int bothPlus = 0;
        int bothMinus = 0;
        int minus = 0;
        int zeros = 0;
        for (int y = 0; y < horizontal.height(); ++y) {
            for (int x = 0; x < horizontal.width(); ++x) {
                double[] valuesH = horizontal.get(y, x);
                double[] valuesV = vertical.get(y, x);
                double b = valuesH[0] * valuesV[0];
                double g = valuesH[1] * valuesV[1];
                double r = valuesH[2] * valuesV[2];
                if (valuesH[0] > 0 && valuesV[0] > 0) ++bothPlus;
                if (valuesH[0] < 0 && valuesV[0] < 0) ++bothMinus;
                if (valuesH[0] == 0 || valuesV[0] == 0) ++ zeros;
                if (b < 0) ++minus;
                joined.put(y, x, b, g, r);
            }
        }
        System.out.println("Both plus: " + bothPlus + ", both minus: " + bothMinus + " of " + (horizontal.width() * horizontal.height()) + " px");
        System.out.println("Total plus " + (bothPlus + bothMinus) + ", total minus " + minus + ", total zeros " + zeros);
        return joined;
    }

    private Mat derivation(Mat input, Mat kernel) {
        Mat output = Mat.zeros(input.height(), input.width(), CvType.CV_16SC3);
        int winSizeX = kernel.width() / 2;
        int winSizeY = kernel.height() / 2;
        for (int winY = 0; winY < kernel.height(); ++winY) {
            for (int winX = 0; winX < kernel.width(); ++winX) {
                double multiplier = kernel.get(winY, winX)[0];
                if (multiplier > -0.0001 && multiplier < 0.0001) {
                    continue;
                }
                int minY = Integer.max(0, winSizeY - winY);
                int minX = Integer.max(0, winSizeX - winX);
                for (int y = minY; y < input.height() - winY; ++y) {
                    for (int x = minX; x < input.width() - winX; ++x) { // todo boundaries
                        int inputX = x - winSizeX + winX;
                        int inputY = y - winSizeY + winY;
                        double value = input.get(inputY, inputX)[0];
                        double outValueOrig = input.get(y, x)[0];
                        double outValueNew = outValueOrig + value * multiplier;
                        output.put(y, x, outValueNew, 0, 0);
                    }
                }
            }
        }
        return output;
    }
}
