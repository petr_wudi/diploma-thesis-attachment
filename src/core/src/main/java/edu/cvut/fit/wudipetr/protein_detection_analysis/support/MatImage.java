package edu.cvut.fit.wudipetr.protein_detection_analysis.support;

//import com.sun.istack.internal.NotNull;
import org.opencv.core.Mat;
import org.opencv.core.Range;
import org.opencv.imgproc.Imgproc;

/**
 * Image implemented using OpenCV Mat
 */
public class MatImage implements Image {
    protected Mat image;

    //public MatImage(@NotNull Mat image) {
    public MatImage(Mat image) {
        this.image = image;
    }

    @Override
    public double get(int x, int y) {
        double[] values = image.get(y, x);
        return (values[0] + values[1] + values[2]) / 3;
    }

    @Override
    public double get(ImagePoint point) {
        return get(point.getX(), point.getY());
    }

    @Override
    public void set(int x, int y, double value) {
        double[] values = {value, value, value};
        image.put(y, x, values);
    }

    @Override
    public void set(ImagePoint point, double value) {
        set(point.getX(), point.getY(), value);
    }

    @Override
    public int width() {
        return image.width();
    }

    @Override
    public int height() {
        return image.height();
    }

    @Override
    public int[][] getArray() {
        int width = width();
        int height = height();
        int[][] arr = new int[height][width];
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                arr[y][x] = (int) get(x, y);
            }
        }
        return arr;
    }

    @Override
    public Mat opencv() {
        return image;
    }

    @Override
    public Image copy() {
        Mat mat2 = image.clone();
        Image img2 = new MatImage(mat2);
        return img2;
    }

    @Override
    public Image crop(ImagePoint start, ImagePoint end) {
        Range xRange = new Range(start.getX(), end.getX());
        Range yRange = new Range(start.getY(), end.getY());
        Mat mat = new Mat(image, yRange, xRange);
        return new MatImage(mat);
    }
}
