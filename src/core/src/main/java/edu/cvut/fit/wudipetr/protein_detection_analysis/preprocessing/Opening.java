package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class Opening extends MorphologicalOperation {

    public Opening(int kernelDiameter) {
        this(kernelDiameter, 1.0);
    }

    public Opening(int kernelDiameter, double multiplier) {
        super(Imgproc.MORPH_OPEN, kernelDiameter, multiplier);
    }

    @Override
    public Image apply(Image image) {
        Mat imageMat = image.opencv();
        Mat opening = applyOperation(imageMat, Imgproc.MORPH_OPEN);
        return new MatImage(opening);
    }

    @Override
    public String getIdentifier() {
        return "opening";
    }
}
