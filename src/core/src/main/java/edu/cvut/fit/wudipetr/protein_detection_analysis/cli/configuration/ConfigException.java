package edu.cvut.fit.wudipetr.protein_detection_analysis.cli.configuration;

public class ConfigException extends Exception {

    public ConfigException(String message) {
        super(message);
    }

    public ConfigException(String message, Exception e) {
        super(message, e);
    }
}
