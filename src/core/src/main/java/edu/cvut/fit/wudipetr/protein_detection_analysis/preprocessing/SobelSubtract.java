package edu.cvut.fit.wudipetr.protein_detection_analysis.preprocessing;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.MatImage;
import org.opencv.core.Core;
import org.opencv.core.Mat;

public class SobelSubtract extends SobelOperator {
    public SobelSubtract(int kernelSize) {
        super(kernelSize);
    }

    @Override
    public Image apply(Image image) {
        Mat source = image.opencv();
        Mat derivation1 = apply(source);
        Mat result = new Mat();
        Core.subtract(source, derivation1, result);
        return new MatImage(result);
    }

    @Override
    public String getIdentifier() {
        return "sobel-subtractor";
    }

    @Override
    public String getFullName() {
        return "sobel-subtractor__" + kernelSize;
    }
}
