package edu.cvut.fit.wudipetr.protein_detection_analysis.support.protein_mask;

import org.opencv.core.Mat;

import java.util.function.BiConsumer;

/**
 * Stores information where one protein lies. It contains a mask with value of 1 on the place where protein lies and
 * 0 value on the part of the image where the protein does not lie.
 */
public interface ProteinMask {

    /**
     * Sets value of one pixel in the protein mask.
     * @param x horizontal coordinate of the pixel
     * @param y vertical coordinate of the pixel
     * @param isPresent whether the protein is present there or not
     */
    void setValue(int x, int y, boolean isPresent);

    /**
     * @return mat having non-zero values on the places where the protein lies and zeros elsewhere
     */
    Mat getMat();

    /**
     * Returns mat having non-zero values on the places where the protein lies and zeros elsewhere. The output mat will
     * have the specified dimensions.
     * @param width width of the output mat
     * @param height height of the output mat
     * @return mask mat
     */
    Mat getMat(int width, int height);

    int getSize();

    @Deprecated
    void drawFilling(Mat image);

    /**
     * Applies the consumer function to all pixels that are part of the mask.
     * @param consumer consumer accepting two integers - x and y coordinates of the pixel
     */
    void apply(BiConsumer<Integer, Integer> consumer);
}
