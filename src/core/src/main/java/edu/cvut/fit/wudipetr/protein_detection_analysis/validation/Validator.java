package edu.cvut.fit.wudipetr.protein_detection_analysis.validation;

import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Image;
import edu.cvut.fit.wudipetr.protein_detection_analysis.support.Protein;

import java.util.Collection;

public interface Validator {
    /**
     * Selects proteins that are relly proteins and not false positives. It could also merge more proteins into one.
     * @param potentialProteins list of particles that might be proteins
     * @param image the image the proteins were found in
     * @return list of particles the algorithms believes to be real proteins
     */
    Collection<Protein> filter(Collection<Protein> potentialProteins, Image image);

    public String getIdentifier();
}
