const express = require('express');
const session = require('express-session');
const fs = require('fs');

const app = express();
const parameterLimit = 1000;
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.static('dist'));
app.use(express.json());
app.use(express.urlencoded({
    extended: true,
    parameterLimit: parameterLimit
}));
app.use(session({secret: "Very nice hash I have..."}));

const password = 'NfAv^i&99Bej'; // todo temporary password


app.get('/', function (req, res) {
    let requestToken = req.session.token;
    if (requestToken === undefined || requestToken == null) {
        requestToken = req.query.token;
    }
    if (requestToken === password) {
        req.session.token = requestToken;
        res.render('index', {
            images: [
                '1t001.png',
                '1t001_SRRF.png',
                '1t009.png',
                '1t001.tif-SRRF.png'
            ]
            // todo all images
        });
    } else {
        res.render('forbidden');
    }
});

app.get('/img/:filename.png', function(req, res) {
    const requestToken = req.session.token;
    if (requestToken !== password) {
        res.render('forbidden');
        return;
    }
    const fileName = req.params.filename;
    const filePath = '/img/' + fileName + ".png";
    res.sendFile(__dirname + filePath);
});

app.use(express.static('output'));

function saveCircles(fileName, circles) {
    const dirName = './output';
    if (!fs.existsSync(dirName)){
        fs.mkdirSync(dirName);
    }
    // todo protection from XSS
    let data = JSON.stringify(circles, null, 2);
    fs.writeFile(dirName + '/' + fileName, data, (err) => {
        if (err) throw err;
        console.log('Data were written to file ' + fileName);
    });
}

function mkFilePath(dirName, fileNamePrefix, pageId) {
    return dirName + '/' + fileNamePrefix + '-' + pageId + '.json';
}

function loadCircles(fileNamePrefix) {
    const dirName = './output';
    let circles = [];
    let pageId = 0;
    let filePath = mkFilePath(dirName, fileNamePrefix, pageId);
    while (fs.existsSync(filePath)) {
        const contents = fs.readFileSync(filePath, 'utf8');
        const circlesLocal = JSON.parse(contents);
        circles = circles.concat(circles, circlesLocal);
        pageId++;
        filePath = mkFilePath(dirName, fileNamePrefix, pageId);
    }
    return circles;
}

app.get('/image', function (req, res) {
    const requestToken = req.session.token;
    if (requestToken !== password) {
        res.render('forbidden');
        return;
    }
    const imgSrcRaw = req.query.imgSrc;
    if (imgSrcRaw === undefined) {
        return;
    }
    const imgSrc = imgSrcRaw.replace("'", "").replace("/", "");
    const circles = loadCircles(imgSrc); // todo useless
    res.render('image', {
        imgSrc: imgSrc,
        circles: circles
    });
});

app.post('/getCircles', function(req, res){
    const requestToken = req.session.token;
    if (requestToken !== password) {
        res.render('forbidden');
        return;
    }
    const imgNameRaw = req.body.sourceImage;
    const imgName = imgNameRaw.replace("'", "").replace("/", "");
    const circles = loadCircles(imgName);
    res.json(circles);
});

app.post('/saveCircles', function(req, res){
    const requestToken = req.session.token;
    if (requestToken !== password) {
        res.render('forbidden');
        return;
    }
    const circles = req.body.circles;
    const fileName = req.body.fileName;
    const page = req.body.page;
    console.log("BODY", req.body);
    saveCircles(fileName + "-" + page + '.json', circles);
    circles.forEach(function(circle) {
        console.log('circle', circle.x, circle.y); // todo remove
    });
    res.json({
        returnStatus: 200
    });
});

const port = process.env.PORT || 3000;
app.listen(port, function () {
    console.log(`Listening on port ${port}!`)
});
