const $ = require("jquery");
const Circle = require("./circle").Circle;

const MIN_ZOOM = -150;
const MAX_ZOOM = 150;

export function Image(element, imageCloseContainer, containerElem, imgWidth, imgHeight) {
    this.element = element;
    this.closerContainerElem = imageCloseContainer;
    this.containerElem = containerElem;
    this.scrollState = {
        zoom : 0,
        scale: 1,
        imgWidth: imgWidth,
        imgHeight: imgHeight
    };
    this.circles = [];
    this.isMouseDown = false;
    this.startMovePosition = {
        x: 0,
        y: 0
    };
    this.movedSum = {
        x: 0,
        y: 0
    };
    const self = this;
    $(imageCloseContainer).bind('wheel', function (jqueryEvent) {
        const event = jqueryEvent.originalEvent;
        const clickX = event.clientX;
        const clickY = event.clientY;
        self.changeScroll(event.deltaY);
        self.recompute(clickX, clickY);
        return false;
    });
    imageCloseContainer.style.width = element.width + 'px';
    imageCloseContainer.style.height = element.height + 'px';
    $(imageCloseContainer).bind('mousedown', function(event) { self.setMouseDown(event); });
    $(imageCloseContainer).bind('mousemove', function(event) { self.mouseMove(event); });
    $(imageCloseContainer).bind('mouseup', function(event) { self.setMouseUp(); });
    $(imageCloseContainer).focus();
}

Image.prototype.setMouseDown = function(event) {
    this.isMouseDown = true;
    this.startMovePosition.x = event.clientX;
    this.startMovePosition.y = event.clientY;
    this.movedSum.x = 0;
    this.movedSum.y = 0;
};

Image.prototype.setMouseUp = function() {
    this.isMouseDown = false;
    // return false; // todo click again
};

Image.prototype.mouseMove = function(event) {
    if (this.isMouseDown) {
        const xDiff = event.clientX - this.startMovePosition.x;
        const yDiff = event.clientY - this.startMovePosition.y;
        const x = this.containerElem.scrollLeft - xDiff;
        const y = this.containerElem.scrollTop - yDiff;
        this.containerElem.scrollTo(x, y);
        this.startMovePosition.x = event.clientX;
        this.startMovePosition.y = event.clientY;
        this.movedSum.x += Math.abs(xDiff);
        this.movedSum.y += Math.abs(yDiff);
        return false;
    }
};

/**
 * @return {{x: number, y: number}} of the left top corner of the image visible on the screen (in real coordinates)
 */
Image.prototype.getStartPosition = function() {
    const x = this.containerElem.scrollLeft / this.scrollState.scale;
    const y = this.containerElem.scrollTop / this.scrollState.scale;
    return {
        x: x,
        y: y
    };
};

Image.prototype.click = function(event) {
    if (this.movedSum.x > 3 || this.movedSum.y > 3) {
        return;
    }
    const clickX = event.clientX;
    const clickY = event.clientY;
    const imgRect = this.containerElem.getBoundingClientRect();
    const imgX = imgRect.left;
    const imgY = imgRect.top;
    const xWin = clickX - imgX;
    const yWin = clickY - imgY;
    // todo code above use in a method
    const startPosition = this.getStartPosition();
    const scale = this.scrollState.scale;
    const x = startPosition.x + xWin / scale;
    const y = startPosition.y + yWin / scale;
    console.log(x, y);
    this.addCircle(x, y);
};

Image.prototype.addCircle = function(x, y) {
    const circleElem = document.createElement("div");
    circleElem.className = "circle";
    this.closerContainerElem.appendChild(circleElem);
    const circle = new Circle(circleElem, x, y);
    this.circles.push(circle);
    circle.recompute(this.scrollState.scale);
    circle.log();
};

Image.prototype.recompute = function(clickX, clickY) {
    const imgRect = this.containerElem.getBoundingClientRect();
    const imgX = imgRect.left;
    const imgY = imgRect.top;
    const mouseX = clickX - imgX;
    const mouseY = clickY - imgY;

    const oldStartPosition = this.getStartPosition();
    const oldScale = this.scrollState.scale;
    const scale = Math.pow(2, (this.scrollState.zoom / 48));
    this.scrollState.scale = scale;

    const startX = oldStartPosition.x + mouseX / oldScale - mouseX / scale;
    const startY = oldStartPosition.y + mouseY / oldScale - mouseY / scale;

    const realStartX = startX * scale;
    const realStartY = startY * scale;
    let imageWidth = this.scrollState.imgWidth * scale;
    let imageHeight = this.scrollState.imgHeight * scale;
    this.element.width = Math.round(imageWidth);
    this.element.height = Math.round(imageHeight);
    this.closerContainerElem.style.width = Math.round(imageWidth) + 'px';
    this.closerContainerElem.style.height = Math.round(imageHeight) + 'px';
    this.containerElem.scrollTo(realStartX, realStartY);

    this.circles.forEach(function(circle) { circle.recompute(scale); } );
};

Image.prototype.changeScroll = function(delta) {
    this.scrollState.zoom -= delta;
    if (this.scrollState.zoom > MAX_ZOOM) {
        this.scrollState.zoom = MAX_ZOOM;
    }
    if (this.scrollState.zoom < MIN_ZOOM) {
        this.scrollState.zoom = MIN_ZOOM;
    }
};

Image.prototype.deleteAllCircles = function() {
    this.circles.forEach(function(circle) {
        circle.deleteMe();
    });
};
