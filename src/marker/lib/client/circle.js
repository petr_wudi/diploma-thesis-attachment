const CIRCLE_RADIUS = 4 + 4;

export function Circle(element, x, y) {
    this.element = element;
    this.position =  {
        x: x,
        y: y
    };
    this.isDeleted = false;
    this.loggerElem = undefined;
}

Circle.prototype.getX = function() {
    return this.position.x;
};

Circle.prototype.getY = function() {
    return this.position.y;
};

Circle.prototype.log = function() {
    const logger = document.getElementById("logger");  // todo global
    this.loggerElem = document.createElement("li");
    this.loggerElem.className = "circleLog";
    const deleteBtn = document.createElement("a");
    deleteBtn.className = "deleteButton";
    deleteBtn.innerText = "delete";
    const textSpan = document.createElement("span");
    textSpan.innerText = Math.round(this.position.x) + ", " + Math.round(this.position.y);
    this.loggerElem.appendChild(deleteBtn);
    this.loggerElem.appendChild(textSpan);
    const self = this;
    deleteBtn.onclick = function(elem) { self.deleteMe(); };
    logger.appendChild(this.loggerElem);
};

/**
 * Recomputes position of an circle
 * @param scrollState how zoomed the site is
 */
Circle.prototype.recompute = function(scale) {
    const realStartX = this.position.x * scale;
    const realStartY = this.position.y * scale;
    this.element.style.left = (realStartX - CIRCLE_RADIUS) + 'px';
    this.element.style.top = (realStartY - CIRCLE_RADIUS) + 'px';
};

Circle.prototype.deleteMe = function() {
    this.isDeleted = true;
    if (this.loggerElem !== undefined) {
        this.loggerElem.parentNode.removeChild(this.loggerElem);
    }
    this.element.parentNode.removeChild(this.element);
};
