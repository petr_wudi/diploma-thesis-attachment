readme.txt.........................the file with CD contents description
jar.....................................the directory with Java packages
src........................................the directory of source codes
    core....................detector and analysis library source sources
    imagej..................................ImageJ plugin source sources
    evaluation.........................evaluation program source sources
    marker................................particle marker source sources
    thesis..............the directory of LaTeX source code of the thesis
thesis.pdf.................................the thesis text in PDF format
dependencies..............................libraries used by the programs
    opencv-build....................................OpenCV library build
    opencv-4.0.0..............................OpenCV library source code
